#include <stdio.h>
#include <stdlib.h>

struct node{
    int data;
    struct node *link;
};

struct node* del_last(struct node *head){
    if(head == NULL)
        printf("List is already empty!");
    else if(head->link == NULL){// Tek düğüm(node) olma durumu
        free(head);
        head = NULL;
    }
    else{
        struct node *temp = head;
        struct node *temp2 = head;
        while(temp->link != NULL){
            temp2 = temp;
            temp = temp->link;
        }
        temp2->link = NULL;
        free(temp);
        temp = NULL;
    }
    return head;
}

int main(){
    struct node *head;
    head=(struct node *)malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *current;
    current=(struct node *)malloc(sizeof(struct node));
    current->data = 98;
    current->link = NULL;
    head->link = current;

    current=(struct node *)malloc(sizeof(struct node));
    current->data = 3;
    current->link = NULL;
    head->link->link = current;

    struct node *ptr;

    head = del_last(head); // head fonksiyondan 1000 adresini alır
    ptr = head;
    while(ptr != NULL){
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
//    printf("\n");
    return 0;
}

// Note : There is no need to return the head pointer as
// We are not updating it in the function.
