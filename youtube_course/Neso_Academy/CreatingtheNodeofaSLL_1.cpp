//  https://www.youtube.com/watch?v=DneLxrPmmsw&list=PLBlnK6fEyqRi3-lvwLGzcaquOs5OBTCww&index=3 
// Self Referential Structure is a structure which contains// pointer to a structure of the same type.

// we will use self referential structure for creating
// a node of the single linked list.

//  ____________
//  |data|link | (link, pointer sadece bir sonraki node)
//  ------------
//     Node (node structure only)


// struct abc{
// 	int a;
// 	char b;
// 	struct abc *self;
// };

// Create A Node In C


#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *link;
};

int main(){
	struct node *head = NULL;
	head = (struct node *)malloc(sizeof(struct node));

	head->data = 45;
	head->link = NULL;

	printf("%d", head->data);
	return 0;
}

// |-----|
// |NULL | struct node *head = NULL;
// |-----|
//  Head
