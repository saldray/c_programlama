// Traversing A Single Linked List (SSL)
// Traversing a single linked list means visiting each
// node of a single linked list until the end node is
// reached.
//
// Our job is to calculate the total number of nodes by
// traversing the linked list.
//
// Program to count the number of nodes
//

#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *link;
};

int main(){
	count_of_nodes(head);
}

// ana fonksiyonda yani main() de 
// count_of_nodes(head); fonksiyonunu tanımladım ancak
// daha "head" göstericisini oluşturmadım
//
// imagine that head is the pointer to the first node of
// the linked list shown above.
//
void count_of_nodes(struct node *head){
	int count = 0;
	if(head == NULL)
		printf("Linked List is empty");
	struct node *ptr = NULL;
	ptr = head;
	while(ptr != NULL){
		count++;
		ptr = ptr->link;//ptr = 2000 gibi düşün
	}
	printf("%d", count);
}
