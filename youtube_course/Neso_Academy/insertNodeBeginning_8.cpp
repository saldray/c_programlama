#include <stdio.h>
#include <stdlib.h>

struct node{
    int data;
    struct node *link;
};
// Adding The Node At The Beginning of The List
struct node * add_beg(struct node * head, int d){
    struct node *ptr = (struct node *)malloc(sizeof(struct node));
    ptr->data = d;
    ptr->link = NULL;

    ptr->link = head;
    head = ptr;
    return head;
}

int main(){
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *ptr = (struct node *)malloc(sizeof(struct node));
    ptr->data = 98;
    ptr->link = NULL;

    head->link = ptr;

    int data = 3;

    head = add_beg(head, data);
    ptr = head;
    while(ptr != NULL){
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}

// output: 3 45 98

// pass by value
// head = add_beg(head, data); ile yaptık daha önce de böyle yapıyorduk









//https://www.youtube.com/watch?v=YL8MnLAJOMg&list=PLBlnK6fEyqRi3-lvwLGzcaquOs5OBTCww&index=13

// #include <stdio.h>
// #include <stdlib.h>
//
// struct node{
//     int data;
//     struct node *link;
// };
//
//
// void print_data(struct node *head){
//     if(head == NULL)
//         printf("Linked List is empty");
//     struct node *ptr = NULL;
//     ptr =head;
//     while(ptr != NULL){
//         printf("%d ", ptr->data);
//         ptr = ptr->link;
//     }
//     printf("\n");
// }
//
// int main(){
//     struct node *head;
//     head=(struct node *)malloc(sizeof(struct node));
//     head->data = 45;
//     head->link = NULL;
//
//     struct node *current;
//     current=(struct node *)malloc(sizeof(struct node));
//     current->data = 98;
//     current->link = NULL;
//     head->link = current;
//
//
//     print_data(head);
//
//     return 0;
// }
