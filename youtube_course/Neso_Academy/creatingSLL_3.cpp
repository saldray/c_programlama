// Üçüncü bir node ekleme
// 1. metod

// #include <stdio.h>
// #include <stdlib.h>
// 
// struct node{
// 	int data;
// 	struct node *link;
// };
// 
// int main(){
// 	struct node *head =(struct node *)malloc(sizeof(struct node));
// 	head->data = 45;
// 	head->link = NULL;
// 
// 	struct node *current;
// 	current =(struct node *)malloc(sizeof(struct node);
// 	current->data = 98;
// 	current->link = NULL;
// 	head->link = current;
// 
// 	struct node *current2 = malloc(sizeof(struct node));
// 	current2->data = 3;
// 	current2->link = NULL;
// 	current->link = current2;
// 
// 	return 0;
// }
//
// 2. Metod


#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *link;
};

int main(){
	struct node *head;
	head=(struct node *)malloc(sizeof(struct node));
	head->data = 45;
	head->link = NULL;

	struct node *current;
	current=(struct node *)malloc(sizeof(struct node));
	current->data = 98;
	current->link = NULL;
	head->link = current;
	
	current=(struct node *)malloc(sizeof(struct node));
	current->data = 3;
	current->link = NULL;
	head->link->link = current;


	struct node *iter;
	iter = head;
	while (iter != NULL){
		printf("%d ",iter->data);
		iter = iter->link;
	
	}
	return 0;
}
