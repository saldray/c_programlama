#include <stdio.h>
#include <stdlib.h>

struct n{
	int data;
	struct n *next;
};
typedef n node;

void bastir(node * r){
	node * iter;
	iter = r;
	while(iter != NULL){
		printf("%d ", iter->data);
		iter = iter->next;
	}
	printf("\n");
}

node * sonaEkle(node * r, int sayi){
//	node * ilkNode = NULL;
	node * root;//root adında gösterici tanımlanıdı
	root = (node *)malloc(sizeof(node));
	root->data = sayi;
	root->next = NULL;
	if(r == NULL){ //if(ilkNode == NULL)
//		ilkNode = root;
		return root;
	}
	else{
		node *temp = (node *)malloc(sizeof(node));
		temp = r;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = root; //fonksiyondaki kutu ekleniyor
	}
	return r;
}

int main(){
	node * root;
	root = NULL;
	int adet = 0;
	while(adet != 3){
		int sayi;
		printf(" bir sayi giriniz : ");
		scanf("%d", &sayi);
		root = sonaEkle(root,sayi); // fonksiyonu çağırıyorum
		//sonaEkle(root,sayi);
		//  adet += 1;
		adet++;
	}
	bastir(root);
	return 0;
}
