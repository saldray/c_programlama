// https://www.youtube.com/watch?v=nxtDe6Gq4t4&list=PLBlnK6fEyqRi3-lvwLGzcaquOs5OBTCww&index=4
// #include <stdio.h>
// #include <stdlib.h>
// 
// struct node{
// 	int data;
// 	struct node *link;
// };
// 
// int main(){
// 	struct node *head = malloc(sizeof(node));
// 	head->data = 45;
// 	head->link = NULL;
// 
// 	head = malloc(sizeof(struct node));
// 	head->data = 98;
// 	head->link = NULL;
// 
// 	return 0;	
// }
// Tekrar head tanımladığımızda tek bağlı listelerde
// bir daha geriye gidemiyoruz. Bunun çözümü
//



#include <stdio.h>
#include <stdlib.h>

struct node{
	int data;
	struct node *link;
};

void bastir(node * r){
//	struct node	*iter;
//	iter=r;
//	printf("
	while(r != NULL){
		printf("%d ", r->data);
		r = r->link;
	}
}

int main(){
	struct node *head =(struct node *)malloc(sizeof(struct node));
	head->data = 45;
	head->link = NULL;

	struct node *current = (struct node *)malloc(sizeof(struct node));
	current->data = 98;
	current->link = NULL;
	head->link = current;
	
	bastir(head);
	return 0;	
}

