// Linked List
/*
void add_at_end(struct node *head, int data){
    struct node *ptr, *temp;
    ptr = head;
    temp = (struct node*)malloc(sizeof(struct node));

    temp->data = data;
    temp->link = NULL;

    while(ptr->link != NULL){
        ptr = ptr->link;
    }
    ptr->link = temp;
}

// Time Complexity: O(n)

while(ptr->link != NULL){
    ptr = ptr->link;
}*/

// n tane eleman varsa hepsini gezecek.

// Optimal Code amaç time Complexity yi azaltmak


#include <stdio.h>
#include <stdlib.h>

struct node{
    int data;
    struct node * link;
};


struct node* add_at_end(struct node *ptr, int data){
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->link = NULL;

    ptr->link = temp;
    return temp;
}

int main(){
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *ptr = head;
    ptr = add_at_end(ptr, 98);
    ptr = add_at_end(ptr, 3);
    ptr = add_at_end(ptr, 67);

    ptr = head;

    while(ptr != NULL){
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}


// ptr->link = temp;
// return temp;
// }
// bu kodun zaman karmaşıklığı Time Complexity: O(1)


















