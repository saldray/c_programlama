// Linked List
/*
 * void add_at_end(struct node *head, int data){
 *    struct node *ptr, *temp;
 *    ptr = head;
 *    temp = (struct node*)malloc(sizeof(struct node));
 *
 *    temp->data = data;
 *    temp->link = NULL;
 *
 *    while(ptr->link != NULL){
 *        ptr = ptr->link;
 *    }
 *    ptr->link = temp;
 * }
 *
 * // Time Complexity: O(n)
 *
 * while(ptr->link != NULL){
 *    ptr = ptr->link;
 * }*/

// n tane eleman varsa hepsini gezecek.

// Optimal Code amaç time Complexity yi azaltmak


#include <stdio.h>
#include <stdlib.h>

struct node{
    int data;
    struct node * link;
};


struct node* add_at_end(struct node *ptr, int data){
    struct node *temp = (struct node *)malloc(sizeof(struct node));
    temp->data = data;
    temp->link = NULL;

    ptr->link = temp;
    return temp;
}

int main(){
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *ptr = head;
    ptr = add_at_end(ptr, 98);
    ptr = add_at_end(ptr, 3);
    ptr = add_at_end(ptr, 67);

    ptr = head;

    while(ptr != NULL){
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}


// ptr->link = temp;
// return temp;
// }
// bu kodun zaman karmaşıklığı Time Complexity: O(1)
/*
                                    ARRAY
 Array is not full                                         Array is full

 have got two cases

Case 1: Array is not full

int main(){
    int a[10];
    int i, n, freePos;
    printf("Enter the number of elements: ");
    scanf("%d ", &a[i]);
    freePos = n;
    freePos = add_at_end(a, freePos, 65);

    for(i=0; i<freePos; i++)
        printf("%d ", a[i]);
    return 0;
}


freePos = n;  Maintaining the index of the empty slot in freePoss
              variable

Lets say n = 3
User will enter 3 values.
Let say are: 1, 2 and 3


  | 3 |        | 1 | 2 | 3 |   |   |   |   |   |   |   |
  freePos        0   1   2   3   4   5   6   7   8   9

int add_at_end(int a[], int freePos, int data){
    a[freePos] = data;
    freePos++;
    return freePos;
}

freePos++;   | 4 |
            freePos

             | 1 | 2 | 3 | 65 |   |   |   |   |   |   |
freePos        0   1   2   3    4   5   6   7   8   9


Time Complexity is this function

Time Complexity: O(1)

int add_at_end(int a[], int freePos, int data){
a[freePos] = data;
freePos++;
return freePos;
}


bu fonksiyonu şöyle düşünebiliriz

#include <stdio.h>

int add_at_end(int a[], int freePos, int data){
a[freePos] = data;
freePos++;
return freePos;
}

int main(){
int a[10];
int i, n, freePos;
printf("Enter the number of elements: ");
scanf("%d ", &a[i]);
freePos = n;
freePos = add_at_end(a, freePos, 65);

for(i=0; i<freePos; i++)
    printf("%d ", a[i]);
return 0;
}


Case 2: Array is Full

int main(){
    int a[3];
    int i, n, freePos;
    printf("Enter the number of elements: ");
    scanf("%d", &n);
    for(i=0; i<n; i++)
        scanf("%d ", &a[i]);

    int size = sizeof(a)/sizeof(a[0]);
    freePos = n;
    if(n == size){
        int b[size+2];
        freePos = add_at_end(a, b, size, freePos, 65);
        for (i=0; i<freePos; i++)
            printf("%d ", b[i]);
    }
    return 0;
}


a | 1 | 2 | 3 |
    0   1   2

| 3 |   | 3 |
size   freePos


if(n == size)  indicates array is full.

b |   |   |   |   |   |
    0   1   2   3   4


int add_at_end(int a[], int b[], int n, int freePos, int data){
    int i;
    for(i=0; i<n; i++)
        b[i] = a[i];
    b[freePos] = data;
    freePos++;
    return freePos;
}


for (i=0; i<n; i++)  a daki tüm elemanları for döngüsü ile b ye kopya
    b[i] = a[i];       lıyor

artırmadan sora freePos

| 3 |   | 4  |
size   freePos


if(n == size)  indicates array is full.

b | 1 | 2 | 3 | 65 |   |
    0   1   2   3   4


int add_at_end(int a[], int b[], int n, int freePos, int data){
    int i;
    for(i=0; i<n; i++)
        b[i] = a[i];
    b[freePos] = data;
    freePos++;
    return freePos;
}

Bu fonksiyonun zaman karmaşıklığı O(n)
Time Complexity: O(1)




                             SUMMARY
                                |
with traversal: O(n)            |       When array is full: O(n)
                                |
without traversal: O(1)         |       When array is not full: O(1)


*/
