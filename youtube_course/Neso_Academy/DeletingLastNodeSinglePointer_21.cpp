#include <stdio.h>
#include <stdlib.h>

struct node{
    int data;
    struct node *link;
};

void del_last(struct node *head){// void döndürülür.
    if(head == NULL)
        printf("List is already empty!");
    else if(head->link == NULL){// Tek düğüm(node) olma durumu
        free(head);
        head = NULL;
    }
    else{
        struct node *temp = head;
        while(temp->link->link != NULL){
            temp = temp->link;
        }
        free(temp->link);
        temp->link = NULL;
    }// head değişmiyor geri döndürmeye gerek yok
}    // fonksiyonunu başında da fonksiyonu head'e atamadık.
// ayrıca del_last fonksiyonunu void döndürüyoruz.
int main(){
    struct node *head;
    head=(struct node *)malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *current;
    current=(struct node *)malloc(sizeof(struct node));
    current->data = 98;
    current->link = NULL;
    head->link = current;

    current=(struct node *)malloc(sizeof(struct node));
    current->data = 3;
    current->link = NULL;
    head->link->link = current;

    struct node *ptr;

    del_last(head); // head fonksiyondan 1000 adresini alır
    ptr = head;
    while(ptr != NULL){
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    //    printf("\n");
    return 0;
}
