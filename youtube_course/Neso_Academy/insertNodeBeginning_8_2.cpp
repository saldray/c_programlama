#include <stdio.h>
#include <stdlib.h>

struct node{
    int data;
    struct node *link;
};
// Adding The Node At The Beginning of The List
void add_beg(struct node **head, int d){
    struct node *ptr = (struct node *)malloc(sizeof(struct node));
    ptr->data = d;
    ptr->link = NULL;

    ptr->link = *head;//dereferencing yapıyoruz kutudaki değer alınır
    *head = ptr;
    //return head; yok değer döndürmüyoruz tipi void oldu
}

int main(){
    struct node *head = (struct node *)malloc(sizeof(struct node));
    head->data = 45;
    head->link = NULL;

    struct node *ptr = (struct node *)malloc(sizeof(struct node));
    ptr->data = 98;
    ptr->link = NULL;

    head->link = ptr;

    int data = 3;

    add_beg(&head, data);
    ptr = head;
    while(ptr != NULL){
        printf("%d ", ptr->data);
        ptr = ptr->link;
    }
    return 0;
}

/*
// https://www.youtube.com/watch?v=90zyJ1eVeUw&list=PLBlnK6fEyqRi3-lvwLGzcaquOs5OBTCww&index=14

pass by reference da head = add_beg(head, data); buna döner add_beg(&head, data);

&head ile head in adresi fonksiyona gönderilir.

          _______________   ______________
          |  45  | 2000 |-->| 98  | NULL |
           1000              2000
             ^
             |
          | 1000 |
            head
            600 (head'in adresi)

void add_beg(struct node **head, int d){
    struct node *ptr = malloc(sizeof(struct node));
    ptr->data = d;
    ptr->link = NULL;

    ptr->link = *head;
    *head = ptr;
}

               |  45  | 2000 |-->| 98  | NULL |
                 1000              2000
                  ^
                  |
    |  600 |-->| 1000 |
      head        head
      700         600 (head'in adresi)

**head : pointer to a pointer (double pointer)
that is a pointer to a pointer which means we need
another pointer which is a pointer to some other pointer
because we want to update this head |1000| which is the head
of main function                     head
                                     600
function that is why another pointer is required to store
the address of this head with help of this pointer içinde
tuttuğu adres 600 adı yine head kendi adresi 700
which is local to this function we'll be able to make changes
in the head of the main function |1000| tutan head
because we have the address of this particular pointer (yani 1000'i tutan head in adresine sahibiz main() fonksiyonun
içinde onu da değiştirebiliriz)
this head (600 u tutan head) is local to this function
void add_beg fonksiyonunun yerel head i
1000 adresini tutan head main function
we will make changes in this head (1000 tutan yani main
fonksiyonunun head inde değişiklik yapacağız) okay

























*/
