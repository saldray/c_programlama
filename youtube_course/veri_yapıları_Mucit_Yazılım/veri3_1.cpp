// sona eleman ekleme
// en son düğüm NULL ise son düğüm orasıdır

#include <stdio.h>
#include <stdlib.h>

struct dugum{
  int veri;
  struct dugum * gosterici;
};

struct dugum * bas = NULL;
struct dugum * gecici = NULL;

// metod yazalım (fonksiyonumuz)
void sonaEkle (int sayi){
  struct dugum * eklenecek;
  eklenecek = (struct dugum *)malloc(sizeof(struct dugum));
  eklenecek->veri = sayi; // parametre olarak gelecek sayi verisini ekleyecek
  eklenecek->gosterici = NULL;

  if(bas == NULL){
    bas = eklenecek;
  }
  else{
    gecici=bas;
    while(gecici->gosterici != NULL){//NULL olmadığı sürece true(1)
      gecici=gecici->gosterici;
    }
    gecici->gosterici = eklenecek;
  }
}
void yazdir(){//bir yazdir fonksiyonu tanımlıyoruz.
  gecici=bas;
  while(gecici->gosterici !=NULL){
    printf(" %d ", gecici->veri);
    gecici = gecici->gosterici;
  }
  printf(" %d ", gecici->veri); //hafızada tutuyor while NULL u bulduğunda
  //yazmayacak döngüden çıkacak
}
int main(){
  int adet = 0;
  while(adet != 5){
  int sayi;
  printf(" bir sayi giriniz : ");
  scanf("%d", &sayi);
  sonaEkle(sayi); // fonksiyonu çağırıyorum
  //  adet += 1;
  adet++;
  }
  yazdir(); //yazdir metodunu çağırıyorum Döngünün en dışına aldım ki yazdırsın
  return 0;
}
