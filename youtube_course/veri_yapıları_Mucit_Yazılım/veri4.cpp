#include <stdio.h>
#include <stdlib.h>

struct dugum{
  int veri;
  struct dugum * gosterici;
};

struct dugum * bas = NULL; // iki pointer düğümü oluşturdum
struct dugum * gecici = NULL;

void basaEkle (int sayi){
  struct dugum * eklenecek;
  // struct yapısında bir kutu oluşturduk
  // eklenecek pointer'ı ile bunu gösteriyoruz.
  eklenecek = (struct dugum *)malloc(sizeof(struct dugum));
  eklenecek->veri = sayi;
  eklenecek->gosterici = bas;
  bas = eklenecek; //
}

void yazdir(){
  gecici = bas;
  while(gecici->gosterici != NULL){
    printf(" %d ", gecici->veri);
    gecici = gecici->gosterici;// bir sonraki kutuyu göstermesi sağlanıyor
  }
  printf(" %d ", gecici->veri);
}

int main(){

  int sayi, adet=0;
  while(adet != 6){
    printf("\nbir sayi giriniz : ");
    scanf("%d", &sayi);
    basaEkle(sayi);
    //    yazdir();
    adet++;
  }
  yazdir();


  return 0;
}


// eklenecek->gosterici = bas;
// eklenecek kutusunun next ine bas in şu an gösterdigi adresi
// atıyoruz eklenecek eklenecek kutusu başa geldiğinde
// daha önce bas ta olan kutuyu göstermiş olacak şimdi

// bas = eklenecek;
// bas pointer ina ekleneceğin adresi geliyor ve artık
// bas ilk kutu olan eklenecek kutusunu point etmiş oluyor 
