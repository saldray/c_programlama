#include <stdio.h>
#include <stdlib.h>

struct dugum{ // yapı oluşturma
  int veri;
  struct dugum * gosterici;
};

int main(){
  struct dugum * bir; // düğüm oluşturma
  bir = (struct dugum *)malloc(sizeof(struct dugum)); // Bellekte yer ayırma

  struct dugum * iki;
  iki = (struct dugum *)malloc(sizeof(struct dugum));

  struct dugum * uc;
  uc = (struct dugum *)malloc(sizeof(struct dugum));

  struct dugum * dort;
  dort = (struct dugum *)malloc(sizeof(struct dugum));

  bir->veri= 11; // dugum bir in verisi 11, veri girişi yapıldı
  bir->gosterici = iki; // bir in gosterici si iki dugum'u adres olarak gösterdik
  iki->veri = 22;
  iki->gosterici = uc; // bir sonraki düğümü gösteriyoruz.

  uc->veri = 33;
  uc->gosterici = dort;

  dort->veri = 44;
  dort->gosterici = NULL;

  printf("%d - %d - %d - %d\n",bir->veri, iki->veri, uc->veri, dort->veri);

  return 0;
}
