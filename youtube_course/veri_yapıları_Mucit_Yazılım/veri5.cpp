// Hem başa hem sona eleman ekleme işlemi
// case yapısıyla

#include <stdio.h>
#include <stdlib.h>

struct node{
  int data;
  struct node * next;
};

struct node * start = NULL;
struct node * temp = NULL;

void basaEkle(int veri){
  struct node * eleman;
  eleman = (struct node *)malloc(sizeof(struct node));
  eleman->data = veri;
  eleman->next = start;
  start = eleman;
}

void sonaEkle(int veri){
  struct node * eleman;
  eleman = (struct node *)malloc(sizeof(struct node));
  eleman->data = veri;
  eleman->next = NULL; // sona ekleyeceksek NULL i bull
  if(start==NULL){
    start = eleman;
  }
  else{
    temp = start;
    while(temp->next != NULL){
        temp = temp->next;
      }
      temp->next = eleman;
  }
}

void yazdir(){
  temp = start;
  while(temp->next != NULL){
    printf(" %d ", temp->data);
    temp = temp->next;
  }
  printf(" %d ", temp->data);
}


int main(){

  int sayi, adet = 0, secim;
  while(adet != 6){
    printf("basa eleman eklemek için 1 \n");
    printf("sona eleman eklemek için 2 \n");
    printf("seciminizi yapınız \n");
    scanf("%d", &secim);

    switch(secim){
    case 1: printf("\nbasa eklenecek elemanın degeri : \n");
      scanf("%d", &sayi);
      basaEkle(sayi);
      yazdir();
      break;

    case 2: printf("\nsona eklenecek elemanın degeri : \n");
      scanf("%d", &sayi);
      sonaEkle(sayi);
      yazdir();
      break;
    }
    adet++;
  }
  return 0;
}
