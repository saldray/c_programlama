#include <stdio.h>
#include <stdlib.h>

struct yapi{
  int data;
  struct yapi *next;
};
typedef yapi node;

node * root = NULL;
node * temp = NULL;

void sonaEkle(int sayi);// fonksiyon prototipleri
void yazdir();

int main(){
  int adet = 0;
  while(adet != 5){
    int sayi;
    printf("bir sayi giriniz :");
    scanf("%d", &sayi);
    sonaEkle(sayi);
    adet++;
  }
  yazdir();
  return 0;
}

void sonaEkle(int sayi){
  node * eklenecek;
  eklenecek = (node *)malloc(sizeof(node));
  eklenecek->data = sayi;
  eklenecek->next = NULL;

  if(root == NULL){
    root = eklenecek;
  }
  else{
    temp = root;// en baştaki düğüm yani root dolu olduğunda
    // geçiciye root'u atayalım yani işlemleri temp üzerinden
    // yapalım
    while(temp->next != NULL){
      temp = temp->next;
    }
    temp->next = eklenecek;
  }
}
void yazdir(){
  temp = root;
  while(temp->next != NULL){
    printf("%d ", temp->data);
    temp = temp->next;
  }
  printf(" %d ", temp->data);// hafızada tutuyor != NULL oldunğunda
  // döngüden yazamadan çıkıyor  döngünün dışında yazdırıyorum
}
