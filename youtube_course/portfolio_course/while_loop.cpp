//https://www.youtube.com/watch?v=Ceq7Cq1I0sc&list=PLA1FTfKBAEX4hblYoH6mnq0zsie2w6Wif&index=9
#include <stdio.h>

int main(){
	int i = 0;

	while (i < 5){
		printf("i: %d\n", i);
		i++;
	}

	printf("Loop is over!\n");

	return 0;
}
