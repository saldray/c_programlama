#include <stdio.h>
#include <stdlib.h>
#include <ctype.h> // Include ctype.h for isdigit function

int main() {
    int number, flag = 1;

    printf("Enter the number as you wish \n");
    printf("If you want to quit press 0 \n\n");

    while (flag) {
        int result = scanf("%d", &number); // Attempt to read an integer
        if (result != 1) { // If scanf failed to read an integer
            char c;
            while ((c = getchar()) != '\n' && c != EOF); // Clear the input buffer
            printf("Please enter a valid number.\n"); // Inform the user about the error
            continue; // Skip the rest of the loop iteration
        }

        flag = number;

        if (flag == 0) {
            printf("You are quitting now.\n");
            break;
        } else {
            // add_list(); // Uncomment this line if you have a function to add numbers to a list
        }
    }
    return 0;
}
