#include <stdio.h>
#include <stdlib.h>

int main() {
    int number, flag = 1;

    printf("Enter the number as you wish\n");
    printf("If you want to quit, press 0\n\n");

    while (flag) {
        printf("Enter a number: ");
        if (scanf("%d", &number) != 1) {
            // Invalid input (not an integer)
            printf("Invalid input! Please enter a valid integer.\n");
            while (getchar() != '\n'); // Clear input buffer
        } else {
            // Valid input
            if (number == 0) {
                printf("You have quit.\n");
                break;
            }
            // Process the input (e.g., add to a list)
        }
    }

    return 0;
}
