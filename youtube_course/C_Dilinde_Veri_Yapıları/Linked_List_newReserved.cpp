#include <stdio.h>
#include <stdlib.h>

//Linked List

struct node {
  int data;
  struct node *next;
};

// Head & Tail
struct node *head = NULL;
struct node *tail = NULL;

// Add Node
int addNode(int data){

  // Linked List is empty
  if(head==NULL){
    struct node *newNode = (struct node *)malloc(sizeof(struct node));
    newNode ->data = data;
    newNode->next = NULL;

    head = tail = newNode;
  }
  // Linked List's not empty
  else{
    struct node *newNode = (struct node *)malloc(sizeof(struct node));
    newNode->data = data;
    newNode->next = NULL;

    tail->next = newNode;

    tail= newNode;
  }
  return 0;
}


int main(){

  addNode(10);
  addNode(14);
  addNode(16);
  addNode(19);


  return 0;
}


// linked list yapısında her eleman sonraki elemanı işaret
// eder demiştik
// işte bunun için
// ilk node adında bir struct tanımlamıştık
// struct node *next
// node türünden next adında bir pointer tanımlıyorum.
// pointerlar (void *) tipinde dönerler artık türü (node *)
// oldu

// Linked list türündeki bir yapı içerisinde listenin
// başını (head) ve sonunu (tail) belirtmemiz gerekiyor
// demiştik
// ilk başta listenin başını ve sonunu NULL olarak tanımlıyorum
// struct node *head = NULL;
// struct node *tail = NULL;

// struct yapımızı oluşturduk
// listemizin başını ve sonunu belirtecek olan değişkenleri
// tanımlamış olduk

// Şimdi şunu istiyorum listeye eleman ekleyecek fonksiyonumuzu
// yazalım

// main() içinde
// addNode() buradan göndereceğim sayıyı içeren mesela 10 olsun
// ilk elemanımız
// struct yapılını linked listeye eklemiş olacak
// addNode(10)
// main() in dışında fonksiyonumuzu yazalım
// Öncelikle tanımlayalım
// int türündeki addNode() fonksiyonunu tanımlıyorum
// fonksiyonun içindeki değeri int data türünde tanımlıyorum
// int addNode(int data);
// main() den göndermiş olduğum değeri buradaki data değişkenine
// almış olacağım

// iki durum var linked listte boş olma durumu ve dolu olma
// durumu ikisini de ayrı ayrı kontrol etmemiz gerekiyor.

// linked list is empty
// Bunu nasıl kontrol edebiliriz?
// listenin ilk elemanı yani head, NULL dan farklı mı
// if(head==NULL){} mu diye bakacağız Boş ise ne yapmamız gerekiyor
// yeni bir node ekleyeceğim
// struct node *new = (hafızada yeni bir yer açacağım)
// Bunun da new değişkeniyle işaret edeceğim
// struct node *new = (struct node *)malloc(sizeof(struct node));
// hafızada node adındaki struct için yer açmış olduk
// bunu da *new değişkeni ile göstermiş olduk yani pointer
// kullanmış olduk.

// Node türünden yapı oluşturdum ancak içi boş ne yapacağım
// gelen data değerini *new structının data'sı olarak ayarlayacağım
// new->data = data
// new->data buradaki data ifadesi struct yapısının içerisindeki data
// new->data = data ikinci data ifadesi ise
// fonksiyondan gelen data değeri olmuş olacak.

// new->next = NULL

// head = tail = new;
// hem listenin başı(head) hem listenin sonu(tail) eklenen
// elemanı göstermiş olacak

// Linkeed liste's not empty
// tail->next  tail'in next'i yani tail'daki next in gösterdiği yer.

// önce node oluşturacağız yani yeni bir eleman
// struct node *new = (struct node *)malloc(sizeof(struct node));
// new->data = data;
// new adlı yeni elemanın, kutunun data değeri fonksiyona gelen
// data değeri olacak
// son olarak bu new adındaki yeni kutunun değerin göstereceği
// next değeri NULL olacak
// new->next = NULL;

// sona ekle
// tail->next = new; oluşturduğnum kutuyu listenin sonuna eklemiş oldum

// tail'ı güncelle
// tail = new 
