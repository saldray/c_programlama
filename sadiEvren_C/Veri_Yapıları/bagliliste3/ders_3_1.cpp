#include <stdio.h>
#include <stdlib.h>

struct n{
	int x;
	n * next;
};
typedef n node;

void bastir(node * r);

void ekle(node * r, int x);

int main(){
	node * root; // root adında bir gösterici oluşturduk
	root = (node *)malloc(sizeof(node));
	// bir kutu oluşturuldu, adresini root göstericisi
	// tutuyor
	root -> next = NULL; //oluşturulan kutunu next'ine NULL
	// root göstericisi ile atandı.
	root -> x = 500; // root'un gösterdiği kutunun x değerine
	// 500 değerini atadık
	//
	bastir(root);
}


void bastir(node * r){
	while(r != NULL){
		printf("%d ",r->x);
		r = r -> next;
	}
}

// root un gösterdiği kutunun x değerini bastırıyorum
// next değeri NULL, x değerini okuduktan sonra fonksiyona
// root gönderildiği için buradaki r herhangi bir parametre
// bastir(roo) fonksiyonunda root->next'i NULL 
// tek kutu olduğu için döngü bir kere dönüyor.
// r ye r->next i atanıyor onun değeride NULL
