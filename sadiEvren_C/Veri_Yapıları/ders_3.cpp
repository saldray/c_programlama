// #include <stdio.h>
// #include <stdlib.h>

// struct n{
//     int x;
//     n * next;
// };
// typedef n node;

// void bastir(node * r){
//     while(r != NULL){
//         printf("%d ",r->x);
//         r = r->next;
//     }
// }
// void ekle(node *r, int x){
//     while (r -> next != NULL){// NULL olunca dursun
//         r = r -> next; // bir sonraki elemana gitmesi için
//     }
//     r -> next = (node *) malloc(sizeof(node));
//     r -> next -> x = x;
//     r -> next -> next = NULL;
// }

// int main(){
//     node * root;
//     root = (node *) malloc(sizeof(node));
//     root -> next = NULL;
//     root -> x = 500;
//     int i = 0;
//     for(i=0;i<5;i++){
//         ekle(root, i*10);
//         // iter -> next = (node *) malloc(sizeof(node));
//         // iter = iter -> next;
//         // iter -> x = i*10;
//         // iter -> next = NULL;
//     }
//     bastir(root);
// }

// Sıralı olarak link liste ekleme yapan bir fonksiyon yazalım
// Öyle bir fonksiyon yazalım ki fonksiyon ekleme işlemi
// yapsın ama link liste eklerken hep sıralı eklesin
// Yani şunu yapmasını istiyoruz İlk başta boş bir link liste
// başlayacak başa kayacak sonra gelen elemen yerleştirdiğimizden
// büyükse şayet sağına küçükse soluna sonra tekrar devam
// edecek yani
// ilk başta biz şunu biliyoruz değil mi bir tane root umuz var
// ve NULL 'ı gösteriyor herhangi bir link list değerini
// göstermiyor
// node * root -----> NULL
// Şimdi dolayısıyla buna göre hesap yapacağız


// #include <stdio.h>
// #include <stdlib.h>

// struct n{
//   int x;
//   n * next;
// };
// typedef n node;

// void bastir(node * r){
//   while(r != NULL){
//     printf("%d ",r->x);
//     r = r->next;
//   }
// }
// void ekle(node *r, int x){
//   while (r -> next != NULL){// NULL olunca dursun
//     r = r -> next; // bir sonraki elemana gitmesi için
//   }
//   r -> next = (node *) malloc(sizeof(node));
//   r -> next -> x = x;
//   r -> next -> next = NULL;
// }

// void ekleSirali(node * r, int x){

// }

// int main(){
//   node * root;

//   // root a malloc yaparak bir değer koymuştuk artık bunu
//   // yapmıyorum sadece root = NULL
//   // root = (node *) malloc(sizeof(node));
//   // root -> next = NULL;
//   // root -> x = 500;
//   int i = 0;
//   for(i=0;i<5;i++){
//     ekle(root, i*10);
//     // iter -> next = (node *) malloc(sizeof(node));
//     // iter = iter -> next;
//     // iter -> x = i*10;
//     // iter -> next = NULL;
//   }
//   bastir(root);
// }






// Yorumlardan arındırılmış devam
// ******************************

// #include <stdio.h>
// #include <stdlib.h>

// struct n{
//   int x;
//   n * next;
// };
// typedef n node;

// void bastir(node * r){
//   while(r != NULL){
//     printf("%d ",r->x);
//     r = r->next;
//   }
// }
// void ekle(node *r, int x){
//   while (r -> next != NULL){// NULL olunca dursun
//     r = r -> next; // bir sonraki elemana gitmesi için
//   }
//   r -> next = (node *) malloc(sizeof(node));
//   r -> next -> x = x;
//   r -> next -> next = NULL;
// }

// void ekleSirali(node * r, int x){
//   if(r == NULL ){
//     r=(node *)malloc(sizeof(node));
//     r->next = NULL;
//     r->x = x;
//   }
//   // özel bir durum bu tek bir düğüm varsa.
//   else if(r->next == NULL){
//     if(r->x > x){
//       node * temp = (node *)malloc(sizeof(node));
//       // temp->next = NULL; aslında bunuu yapmamıza gerek
//       // yok zaten aşağıda temp'in next'ine root u atadık
//       temp->x = x; // yeni kutu oluşturuldu
//       // Bu kutunun root 'tan önce gelmesi lazım.
//       temp->next = root; // root 'un tuttuğu adresi atadık.
//       r = temp; // temp'in tuttuğu adresi r 'ye atadık.
//       // Dolayısıyla root artık temp'i gösteriyor
//     }
//   }

// }

// int main(){
//   node * root;
//   root = NULL;
//   // root NULL bir değer NULL dan başlayarak link list'e
//   // ekleme işlemi yapmaya başlayacağım.
//   // for döngüsünü kaldıralım kendimiz elle değerler
//   // ekleyelim
//   ekleSirali(root,400);
//   ekleSirali(root,40);
//   ekleSirali(root,4);
//   ekleSirali(root,450);
//   ekleSirali(root,50);
//   bastir(root);
// }



// *********************************************************
// * şimdi bir problemimiz var problem nedir problem şudur *
// *********************************************************
// Burada ben link listlerde fonksiyonu çağırırken
// root'un burada değerini geçiriyorum. Yani point ettiği
// değerini geçiriyorum dolayısıyla point ettiği değeri
// geçirdikten sonra hem r hem de root aynı yeri point ediyor
// r nin başka yerleri göstermesi için yerini değiştirirken
// root un yeri değişmiyor link listin hala root unu tutmaya
// çalışıyor Dolayısıyla yeni root u değiştirdiysem root un
// değeri değiştiyse bu bilgiyi benim ana fonksiyon (main())
// e döndürüp root un da değerini ana fonksiyondaki root un
// ana fonksiyondaki link listin ilk yerini değiştirmem lazım
// Dolayısıyla artık buradan void değil node * döndürmem lazım
// root un değişme ihtimalini düşünerek şöyle yapmam lazım
// root = NULL
// root = eklleSirali(root, 400); vb gibi

// #include <stdio.h>
// #include <stdlib.h>

// struct n{
//   int x;
//   n * next;
// };
// typedef n node;

// void bastir(node * r){
//   while(r != NULL){
//     printf("%d ",r->x);
//     r = r->next;
//   }
// }
// void ekle(node *r, int x){
//   while (r -> next != NULL){// NULL olunca dursun
//     r = r -> next; // bir sonraki elemana gitmesi için
//   }
//   r -> next = (node *) malloc(sizeof(node));
//   r -> next -> x = x;
//   r -> next -> next = NULL;
// }

// node * ekleSirali(node * r, int x){
//   if(r == NULL ){
//     r=(node *)malloc(sizeof(node));
//     r->next = NULL;
//     r->x = x;
//     return r; // fonsiyonun başına node * ekledikden sonra
//     // return r; yazabiliriz.
//   }
//   // özel bir durum bu tek bir düğüm varsa.
//   else if(r->next == NULL){
//     if(r->x > x){
//       node * temp = (node *)malloc(sizeof(node));
//       // temp->next = NULL; aslında bunuu yapmamıza gerek
//       // yok zaten aşağıda temp'in next'ine root u atadık
//       temp->x = x; // yeni kutu oluşturuldu
//       // Bu kutunun root 'tan önce gelmesi lazım.
//       temp->next = root; // root 'un tuttuğu adresi atadık.
//       return temp;

//       //r = temp; // temp'in tuttuğu adresi r 'ye atadık.
//       // Dolayısıyla root artık temp'i gösteriyor
//       //return r; // yapa biliriz yada return temp;
//       else{ // tam tersi durum iki değer eşitte olabilir ya
//         // da küçükte
//         node * temp = (node *)malloc(sizeof(node));
//         temp-> x = x;
//         temp -> next = NULL;
//         r -> next = temp;
//         return r;
//       }
//     }
//   }

// }

// int main(){
//   node * root;
//   root = NULL;
//   // root NULL bir değer NULL dan başlayarak link list'e
//   // ekleme işlemi yapmaya başlayacağım.
//   // for döngüsünü kaldıralım kendimiz elle değerler
//   // ekleyelim
//   // Buradaki fark ne artık
//   // eklemeSirali fonksiyonu ekledikten sonra root değişebilir
//   // değişmiş root u main() fonsiyonuna alacak
//   root = ekleSirali(root,400);
//   root = ekleSirali(root,40);
//   root = ekleSirali(root,4);
//   root = ekleSirali(root,450);
//   root = ekleSirali(root,50);
//   bastir(root);
// }



// ****************************************
// else if  bölümünü biraz geliştirelim
// ****************************************


// #include <stdio.h>
// #include <stdlib.h>

// struct n{
//   int x;
//   n * next;
// };
// typedef n node;

// void bastir(node * r){
//   while(r != NULL){
//     printf("%d ",r->x);
//     r = r->next;
//   }
// }
// void ekle(node *r, int x){
//   while (r -> next != NULL){// NULL olunca dursun
//     r = r -> next; // bir sonraki elemana gitmesi için
//   }
//   r -> next = (node *) malloc(sizeof(node));
//   r -> next -> x = x;
//   r -> next -> next = NULL;
// }

// node * ekleSirali(node * r, int x){
//   if(r == NULL ){
//     r=(node *)malloc(sizeof(node));
//     r->next = NULL;
//     r->x = x;
//     return r; // fonsiyonun başına node * ekledikden sonra
//     // return r; yazabiliriz.
//   }
//   // özel bir durum bu tek bir düğüm varsa.
//   // root'un sabit kalabilmesi için pointer'a ihtiyaç var.
//   node * iter = r; // r değişkenlerinin yerine iter yazdım
//   while(iter->next != NULL && iter->next->x < x){
//     iter = iter->next;
//   }
//   node * temp = (node *)malloc(sizeof(node));
//   temp->next = iter->next;
//   iter->next = temp;
//   temp -> x = x;
//   if(r->next == NULL){
//     if(r->x > x){
//       node * temp = (node *)malloc(sizeof(node));
//       // temp->next = NULL; aslında bunuu yapmamıza gerek
//       // yok zaten aşağıda temp'in next'ine root u atadık
//       temp->x = x; // yeni kutu oluşturuldu
//       // Bu kutunun root 'tan önce gelmesi lazım.
//       temp->next = root; // root 'un tuttuğu adresi atadık.
//       return temp;

//       //r = temp; // temp'in tuttuğu adresi r 'ye atadık.
//       // Dolayısıyla root artık temp'i gösteriyor
//       //return r; // yapa biliriz yada return temp;
//       else{ // tam tersi durum iki değer eşitte olabilir ya
//         // da küçükte
//         node * temp = (node *)malloc(sizeof(node));
//         temp-> x = x;
//         temp -> next = NULL;
//         r -> next = temp;
//         return r;
//       }
//     }
//   }

// }

// int main(){
//   node * root;
//   root = NULL;
//   // root NULL bir değer NULL dan başlayarak link list'e
//   // ekleme işlemi yapmaya başlayacağım.
//   // for döngüsünü kaldıralım kendimiz elle değerler
//   // ekleyelim
//   // Buradaki fark ne artık
//   // eklemeSirali fonksiyonu ekledikten sonra root değişebilir
//   // değişmiş root u main() fonsiyonuna alacak
//   root = ekleSirali(root,400);
//   root = ekleSirali(root,40);
//   root = ekleSirali(root,4);
//   root = ekleSirali(root,450);
//   root = ekleSirali(root,50);
//   bastir(root);
// }


//******************************************************
// while döngüsünü eklendikten sonra bazı kodları kaldırıyorum
//******************************************************


#include <stdio.h>
#include <stdlib.h>

struct n{
  int x;
  n * next;
};
typedef n node;

void bastir(node * r){
  while(r != NULL){
    printf("%d ",r->x);
    r = r->next;
  }
}
void ekle(node *r, int x){
  while (r -> next != NULL){// NULL olunca dursun
    r = r -> next; // bir sonraki elemana gitmesi için
  }
  r -> next = (node *) malloc(sizeof(node));
  r -> next -> x = x;
  r -> next -> next = NULL;
}

node * ekleSirali(node * r, int x){
  if(r == NULL ){ //linklist boşsa
    r=(node *)malloc(sizeof(node));
    r->next = NULL;
    r->x = x;
    return r; 
  }
  if(r->x > x){// tek elemandan küçük durum
    // root değişiyor
    node * temp = (node *)malloc(sizeof(node));
    temp->x = x;
    temp->next = r;
    return temp;
  }

  // özel bir durum bu tek bir düğüm varsa.
  // root'un sabit kalabilmesi için pointer'a ihtiyaç var.
  node * iter = r; // r değişkenlerinin yerine iter yazdım
  while(iter->next != NULL && iter->next->x < x){
    iter = iter->next;
  }

  node * temp = (node *)malloc(sizeof(node));
  temp->next = iter->next;
  iter->next = temp;
  temp -> x = x;
  return r; // r nin değişme durumu yok hep aynı

}

int main(){
  node * root;
  root = NULL;
  // root NULL bir değer NULL dan başlayarak link list'e
  // ekleme işlemi yapmaya başlayacağım.
  // for döngüsünü kaldıralım kendimiz elle değerler
  // ekleyelim
  // Buradaki fark ne artık
  // eklemeSirali fonksiyonu ekledikten sonra root değişebilir
  // değişmiş root u main() fonsiyonuna alacak
  root = ekleSirali(root,400);
  root = ekleSirali(root,40);
  root = ekleSirali(root,4);
  root = ekleSirali(root,450);
  root = ekleSirali(root,50);
  bastir(root);
}

// Eşit olma durumunu test etmedik
