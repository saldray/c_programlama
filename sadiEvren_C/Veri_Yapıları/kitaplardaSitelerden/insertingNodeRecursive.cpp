// #include <stdio.h>
// #include <stdlib.h>

// struct node{
//   int data;
//   struct node * link;
// };

// struct node *insert(struct node *p, int n){// struct node * tipinde
//   // dönüş yapan insert adında bir fonksiyon tanımlandı
//   // yani fonksiyon return ettiğinde struct tipinde değer döndürecek
//   // iki parametre alacak biri biri
//   // struct node * tipinde adı p olan bir pointer girdi (argüman)
//   // olarak p pointer ina herhangi adda bir pointer gelebilir.
//   // diğeri integer tipinde (type casting i) n adlı parametre tanımlandı
//   struct node * temp; // temp adında gösterici oluşturuldu
//   if(p == NULL){//p pointer ı NULL bir değerin adresini gösteriyorsa
//     p = (struct node *)malloc(sizeof(struct node));
//     if(p == NULL){
//       printf("Error\n");
//       exit(0);
//       // malloc, void *(pointer, yıldız) tipinde bir değer döndürür
//       // (struct node *) ile (type casting) tipi belirtildi
//       // malloc fonksiyonu ile (struct node) boyutunda hafızada
//       // boş bir kutu oluşturulup p pointer ına atandı.
//       // eğer bir hata oluşursa diye hafızada boş bir kutu oluşturulup
//       // p ye atanamazsa programın çökmeden sonlanması için if ile
//       // hata mesajı döndürüldü.
//     }
//     p->data = n;
//     p->link = NULL;
//     // değer ataması yapıldı oluşturulan kutunun adresini tutan p
//     // pointer ının gösterdiği data değerine n degeri
//     // link kutusuna da NULL değeri atandı.
//   }
//   else{
//     p->link=insert(p->link,n); // the while loop replaced by recursive call
//     return (p);
//     // p pointer ının gösterdiği kutunun link bölümüne insert fonksi
//     // yonundan gelen değerler gönderilecek
//   }
// }
//   //}// *** sonradan eklendi
//   void printlist(struct node *p){// void tipinde fonksiyon tanımlandı
//     printf("The data values in the list are\n");
//     while(p != NULL){
//       printf("%d\t",p->data);
//       p = p->link;
//     }
//   }
// int  main(){
//   int n; int x;
//   struct node *start = NULL;
//   printf("Enter the nodes to be created \n");
//   scanf("%d",&n);
//   while ( n > 0 ){
//     printf("Enter the data values to be placed in a node\n");
//     scanf("%d",&x);
//     start = insert(start,x);
//   }
//   printf("The created list is\n");
//   printlist(start);
// }



// # include <stdio.h>
// # include <stdlib.h>
// struct node
// {
//     int data;
//     struct node *link;
// };
//     struct node *insert(struct node *p, int n)
//     {
//         struct node *temp;
//         if(p==NULL)
//         {
//             p=(struct node *)malloc(sizeof(struct node));
//             if(p==NULL)
//             {
//                 printf("Error\n");
//                 exit(0);
//             }
//             p-> data = n;
//             p-> link = NULL;
//         }
//         else
//             p->link = insert(p->link,n);/* the while loop replaced by  recursive call */
//             return (p);
//     }
//     void printlist ( struct node *p )
//     {
//         printf("The data values in the list are\n");
//         while (p!= NULL)
//         {
//             printf("%d\t",p-> data);
//             p = p-> link;
//         }
//     }
//     int main()
//     {
//         int n; int x;
//         struct node *start = NULL ;
//         printf("Enter the nodes to be created \n");
//         scanf("%d",&n);
//         while ( n > 0 )
//         {
//             printf( "Enter the data values to be placed in a node\n");
//             scanf("%d",&x);
//             start = insert ( start, x );
//         }
//         printf("The created list is\n");
//         printlist ( start );
//     }




// n-- eklendi infinite loop a giriyor


    // # include <stdio.h>
    // # include <stdlib.h>
    // struct node
    // {
    //     int data;
    //     struct node *link;
    // };
    // struct node *insert(struct node *p, int n)
    // {
    //     struct node *temp;
    //     if(p==NULL)
    //     {
    //         p=(struct node *)malloc(sizeof(struct node));
    //         if(p==NULL)
    //         {
    //             printf("Error\n");
    //             exit(0);
    //         }
    //         p-> data = n;
    //         p-> link = NULL;
    //     }
    //     else
    //         p->link = insert(p->link,n);/* the while loop replaced by  recursive call */
    //         return (p);
    // }
    // void printlist ( struct node *p )
    // {
    //     printf("The data values in the list are\n");
    //     while (p!= NULL)
    //     {
    //         printf("%d\t",p-> data);
    //         p = p-> link;
    //     }
    // }
    // int main()
    // {
    //     int n; int x;
    //     struct node *start = NULL ;
    //     printf("Enter the nodes to be created \n");
    //     scanf("%d",&n);
    //     while ( n > 0 )
    //     {
    //         printf( "Enter the data values to be placed in a node\n");
    //         scanf("%d",&x);
    //         start = insert ( start, x );
    //         n--;
    //     }
    //     printf("The created list is\n");
    //     printlist ( start );
    //     return 0;
    // }













// # include <stdio.h>
// # include <stdlib.h>
// struct node
// {
//     int data;
//     struct node *link;
// };
// struct node *insert(struct node *p, int n)
// {
//     struct node *temp;
//     if(p==NULL)
//     {
//         p=(struct node *)malloc(sizeof(struct node));
//         if(p==NULL)
//         {
//             printf("Error\n");
//             exit(0);
//         }
//         p-> data = n;
//         p-> link = NULL;
//     }
//     else
//         p->link = insert(p->link,n);/* the while loop replaced by  recursive call */
//         return (p);
// }
// void printlist ( struct node *p )
// {
//     printf("The data values in the list are\n");
//     while (p!= NULL)
//     {
//         printf("%d\t",p-> data);
//         p = p-> link;
//     }
// }
// int main()
// {
//     int n; int x;
//     struct node *start = NULL ;
//     printf("Enter the nodes to be created \n");
//     scanf("%d",&n);
//     while ( n-- > 0 )
//     {
//         printf( "Enter the data values to be placed in a node\n");
//         scanf("%d",&x);
//         start = insert ( start, x );
//         //        n--;
//     }
//     printf("The created list is\n");
//     printlist ( start );
// }




// bing yapay zeka ile

#include <stdio.h>
#include <stdlib.h>

struct node {
    int data;
    struct node *link;
};

struct node *insert(struct node *p, int n) {
    if (p == NULL) {
        p = (struct node *)malloc(sizeof(struct node));
        if (p == NULL) {
            printf("Error\n");
            exit(0);
        }
        p->data = n;
        p->link = NULL;
    } else {
        p->link = insert(p->link, n); // the while loop replaced by recursive call
    }
    return p;
}

void printlist(struct node *p) {
    printf("The data values in the list are\n");
    while (p != NULL) {
        printf("%d\t", p->data);
        p = p->link;
    }
}

int main() {
    int n;
    int x;
    struct node *start = NULL;
    printf("Enter the nodes to be created \n");
    scanf("%d", &n);
    while (n > 0) {
        printf("Enter the data values to be placed in a node\n");
        scanf("%d", &x);
        start = insert(start, x);
        n--; // Decrement n to avoid infinite loop
    }
    printf("The created list is\n");
    printlist(start);
    return 0; // Return statement added here
}



