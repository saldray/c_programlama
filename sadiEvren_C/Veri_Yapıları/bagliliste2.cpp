/*
  Bağlı listeler
  sequential  doğrusal erişim ile erişilebilir
  random      array dizilerde random acces yani rastgele
              erişim ile erişilebilir.
 */

// Bir döngü ile linked listemizin sonuna gidelim ve bir
// eleman (öğe) ekleyelim.

// Döngünün sonuna geldiğimi yani bağlı listenin sonuna
// geldiğimi nasıl anlarım

// sondaki pointer hiçbir şeyi göstermiyor. Boşluğu gösteriyor
// Bu boşluğa null diyoruz. bu null değere eriştiği anda biz
// durmasını isteyeceğiz
// if (iter != NULL) iter NULL değilken

// #include <stdio.h>
// #include <stdlib.h>

// struct n{
//   int x;
//   n * next;
// };
// typedef n node;

// int main(){
//   node * root;
//   root = (node *) malloc (sizeof(node));
//   root -> x = 10;
//   root -> next = (node *) malloc (sizeof(node));
//   root -> next -> x = 20;
//   root -> next -> next = (node *) malloc(sizeof(node));
//   root -> next -> next -> x = 30;
//   node * iter;
//   iter = root;
//   printf("%d", iter->x);
//   iter = iter -> next;
//   printf("%d", iter->x);
//   iter = root;
//   int i = 0;
//   while (iter != NULL){
//     i++;
//     printf("%dinci eleman :  %d \n",i,iter->x);
//     iter = iter->next;
//   }
// }

// Bende Segmentation fault hatası vermedi ama veriyor

// root'un gösterdiği son elemandan sonraki elemanı
// root -> next -> next -> next = NULL;
// C'de bunu elle koymamız gerekiyor

// Biz malloc ile hafızada yer ayırttığımız zaman
// malloc hafızada bir kutunun koyulabileceği bir yer ayırıyor
// bize. Bu kutu hafızada bir yer kaplıyor içine bir integer
// koyabiliyoruz bir de içine pointer koyabiliyoruz.
// pointer da ram de başka bir yeri gösterecek bize ama
// bu pointer'ın ilk tanımlandığında gösterdiği yer neresi
// belli değil her hangi bir yeri gösteriyor bize o değer
// NULL olabilir. RAM de boş olmasını garanti etmek zorunda
// değil işletim sistemi veya C

// #include <stdio.h>
// #include <stdlib.h>

// struct n{
//   int x;
//   n * next;
// };
// typedef n node;

// int main(){
//   node * root;
//   root = (node *) malloc (sizeof(node));
//   root -> x = 10;
//   root -> next = (node *) malloc (sizeof(node));
//   root -> next -> x = 20;
//   root -> next -> next = (node *) malloc(sizeof(node));
//   root -> next -> next -> x = 30;
//   root -> next -> next -> next = NULL;
//   node * iter;
//   iter = root;
//   printf("%d", iter->x);
//   iter = iter -> next;
//   printf("%d", iter->x);
//   iter = root;
//   int i = 0;
//   while (iter != NULL){
//     i++;
//     printf("%dinci eleman :  %d \n",i,iter->x);
//     iter = iter->next;
//   }
// }

// sentinal  sonu gösteren bir değer, işin bittiğini gösteren
// bir değer ifadesi

// bağlı listenin sonuna bir eleman eklemek istiyorum ancak
// iter şimdi listenin sonunu yani NULL değerini gösteriyor
// eleman ekleye bilmem için son kutudaki değeri gösteriyor
// olması gerekiyor yoksa iş işten geçmiş oluyor
// link liste eleman ekleye bilmem için son kutuyu gösteren
// bir değere ihtiyacım var

// kontrolümü şöyle yapmam lazım
// iter, NULL a gidene kadar değil
// iter'in next'i NULL'a gidene kadar demem lazım.
// yani iter'in next'i NULL olduğunda bu ne demek
// iter 30 değerinin olduğu kutuyu gösterdiğinde
// bu kutunun next'i NULL olduğu için duracak
// iter'in next'i NULL olduğunda dur. O zamana kadar olan
// elemanları bas


#include <stdio.h>
#include <stdlib.h>

// struct n{
//   int x;
//   n * next;
// };
// typedef n node;

// int main(){
//   node * root;
//   root = (node *) malloc (sizeof(node));
//   root -> x = 10;
//   root -> next = (node *) malloc (sizeof(node));
//   root -> next -> x = 20;
//   root -> next -> next = (node *) malloc(sizeof(node));
//   root -> next -> next -> x = 30;
//   root -> next -> next -> next = NULL;
//   node * iter;
//   iter = root;
//   printf("%d", iter->x);
//   iter = iter -> next;
//   printf("%d", iter->x);
//   iter = root;
//   int i = 0;
//   while (iter -> next != NULL){
//     i++;
//     printf("%dinci eleman :  %d \n",i,iter->x);
//     iter = iter->next;
//   }
//   for(i=0;i<5;i++){
//     iter->next = (node *)malloc(sizeof(node));
//     iter = iter->next;
//     iter->x = i*10;
//     iter->next = NULL;
//   }
// }


// öncelikle iter'in next'ine bir kutu koyuyorum
// iter->next = (node *)malloc(sizeof(node));
// bir node'un hafızada kaplayacağı yer kadar yer açtım
// bunu iter'in next'ine koydum

// iter -> next = NULL
// iter'in next'ine NULL değerini atıyorum
// Linked listen sonunda her zaman NULL değeri olacak

// Bir linked list alıp içeriğini ekrana bastıran bir
// fonksiyon yazacağız


struct n{
  int x;
  n * next;
};
typedef n node;

void bastir(node * r){
  while(r != NULL){
    printf("%d ", r->x);
    r = r->next;
  }
}

int main(){
  node * root;
  root = (node *) malloc (sizeof(node));
  root -> x = 10;
  root -> next = (node *) malloc (sizeof(node));
  root -> next -> x = 20;
  root -> next -> next = (node *) malloc(sizeof(node));
  root -> next -> next -> x = 30;
  root -> next -> next -> next = NULL;
  node * iter;
  iter = root;
  printf("%d", iter->x);
  iter = iter -> next;
  printf("%d", iter->x);
  iter = root;
  int i = 0;
  while (iter -> next != NULL){
    i++;
    printf("%dinci eleman :  %d \n",i,iter->x);
    iter = iter->next;
  }
  for(i=0;i<5;i++){
    iter->next = (node *)malloc(sizeof(node));
    iter = iter->next;
    iter->x = i*10;
    iter->next = NULL;
  }
  bastir(root);
}


// void bastir bir tane link list alacak
// node yıldız(*) r (root'un değerini alsın   )
// bir node değerini alıyor bu node dan NULL görene kadar
// ekrana bastırıyor

// bastir(node *r)
// *r yeni bir pointer tanımlıyoruz
// Bu *r'a ne verilirse onu basacak
// root dan başla dersem Önce root daki node a eşit olacak
// sonra bunun değerini ekrana basıp next'ine (değerine gidecek) gidecek
