#include <stdio.h>
#include <stdlib.h>

struct n{
    int x;
    n * next;
};
typedef n node;

void bastir(node * r){
    node *iter = r;
    printf("%d ", iter->x);
    iter = iter->next;
    // başlangıç durumunda iter le root birbirine eşit olduğundan
    // iter != root iter farlı mı root koşuluna girmeyecek
    // bizim bir tane elemanı bastırarak öyle devam etmemiz lazım
    while(iter != r){
        printf("%d ",iter->x);
        iter = iter->next;
    }
    printf("\n");
}
void ekle(node *r, int x){
    node * iter = r;// node yıldız(işaretci) iter eşit root
    while (iter -> next != r){// iter in next i root a eşit dur
        r = r -> next; // bir sonraki elemana gitmesi için
    }
    iter -> next = (node *) malloc(sizeof(node));
    iter -> next -> x = x;
    iter -> next -> next = r;
}

node * ekleSirali(node * r, int x){
    if(r == NULL ){ //linklist boşsa
        r=(node *)malloc(sizeof(node));
        r->next = r;
        r->x = x;
        return r;
    }
    if(r->x > x){// ilk tek elemandan küçük durum
        // root değişiyor
        node * temp = (node *)malloc(sizeof(node));
        temp->x = x;
        temp->next = r;
        node *iter = r;
        while (iter->next != r )
            iter = iter->next;
        iter->next = temp;
        return temp;
    }

    // özel bir durum bu tek bir düğüm varsa.
    // root'un sabit kalabilmesi için pointer'a ihtiyaç var.
    node * iter = r; // r değişkenlerinin yerine iter yazdım
    while(iter->next != r && iter->next->x < x){
        iter = iter->next;
    }

    node * temp = (node *)malloc(sizeof(node));
    temp->next = iter->next;
    iter->next = temp;
    temp -> x = x;
    return r; // r nin değişme durumu yok hep aynı

}
node * sil(node *r,int x){
    node *temp;
    node *iter = r;
    if(r->x == x){
        while(iter->next != r){
            iter = iter->next;
        }
        iter->next = r->next;
        free(r);
        return iter->next;
    }
    while (iter->next != r && iter->next->x != x){
        iter = iter->next;
    }
    if(iter->next == r){
        printf("sayi bulunamadi ");
        return r;

    }
    temp = iter->next;
    iter->next = iter->next->next;
    free(temp);
    return r;

}


int main(){
    node * root;
    root = NULL;
    root = ekleSirali(root,400);
    root = ekleSirali(root,40);
    root = ekleSirali(root,4);
    root = ekleSirali(root,450);
    root = ekleSirali(root,50);
    bastir(root);
    root = sil(root,50);
    bastir(root);
    root = sil(root,999);
    bastir(root);
    root = sil(root,4);
    bastir(root);
    root = sil(root,450);
    bastir(root);

}
