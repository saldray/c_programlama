// #include <stdio.h>
// #include <stdlib.h>

// struct n{
//     int x;
//     n * next;
// };
// typedef n node;

// void bastir(node * r){
//   node *iter = r;
//   printf("%d ", iter->x);
//   iter = iter->next;
//   // başlangıç durumunda iter le root birbirine eşit olduğundan
//   // iter != root iter farlı mı root koşuluna girmeyecek
//   // bizim bir tane elemanı bastırarak öyle devam etmemiz lazım
//   while(iter != r){
//         printf("%d ",iter->x);
//         iter = iter->next;
//     }
// }
// void ekle(node *r, int x){
//   node * iter = r;// node yıldız(işaretci) iter eşit root
//   while (iter -> next != r){// iter in next i root a eşit dur
//         r = r -> next; // bir sonraki elemana gitmesi için
//     }
//     iter -> next = (node *) malloc(sizeof(node));
//     iter -> next -> x = x;
//     iter -> next -> next = r;
// }

// node * ekleSirali(node * r, int x){
//     if(r == NULL ){ //linklist boşsa
//         r=(node *)malloc(sizeof(node));
//         r->next = r;
//         r->x = x;
//         return r;
//     }
//     if(r->x > x){// ilk tek elemandan küçük durum
//         // root değişiyor
//         node * temp = (node *)malloc(sizeof(node));
//         temp->x = x;
//         temp->next = r;
//         node *iter = r;
//         while (iter->next != r )
//           iter = iter->next;
//         iter->next = temp;
//         return temp;
//     }

//     // özel bir durum bu tek bir düğüm varsa.
//     // root'un sabit kalabilmesi için pointer'a ihtiyaç var.
//     node * iter = r; // r değişkenlerinin yerine iter yazdım
//     while(iter->next != r && iter->next->x < x){
//         iter = iter->next;
//     }

//     node * temp = (node *)malloc(sizeof(node));
//     temp->next = iter->next;
//     iter->next = temp;
//     temp -> x = x;
//     return r; // r nin değişme durumu yok hep aynı

// }

// int main(){
//     node * root;
//     root = NULL;
//     // root NULL bir değer NULL dan başlayarak link list'e
//     // ekleme işlemi yapmaya başlayacağım.
//     // for döngüsünü kaldıralım kendimiz elle değerler
//     // ekleyelim
//     // Buradaki fark ne artık
//     // eklemeSirali fonksiyonu ekledikten sonra root değişebilir
//     // değişmiş root u main() fonsiyonuna alacak
//     root = ekleSirali(root,400);
//     root = ekleSirali(root,40);
//     root = ekleSirali(root,4);
//     root = ekleSirali(root,450);
//     root = ekleSirali(root,50);
//     bastir(root);
// }





//   printf("%d ", iter->);
//   iter = iter->next;
//   iter i bir kere bastırdım ve iter i bir kere ilerletiyorum
// ondan sonra artık root görene kadar devam et diyebilirim



// *****************************************************
// Test durumu eklenmiş
// *****************************************************

#include <stdio.h>
#include <stdlib.h>

struct n{
    int x;
    n * next;
};
typedef n node;

void bastir(node * r){
  node *iter = r;
  printf("%d ", iter->x);
  iter = iter->next;
  // başlangıç durumunda iter le root birbirine eşit olduğundan
  // iter != root iter farlı mı root koşuluna girmeyecek
  // bizim bir tane elemanı bastırarak öyle devam etmemiz lazım
  while(iter != r){
        printf("%d ",iter->x);
        iter = iter->next;
    }
}
void ekle(node *r, int x){
  node * iter = r;// node yıldız(işaretci) iter eşit root
  while (iter -> next != r){// iter in next i root a eşit dur
        r = r -> next; // bir sonraki elemana gitmesi için
    }
    iter -> next = (node *) malloc(sizeof(node));
    iter -> next -> x = x;
    iter -> next -> next = r;
}

node * ekleSirali(node * r, int x){
    if(r == NULL ){ //linklist boşsa
        r=(node *)malloc(sizeof(node));
        r->next = r;
        r->x = x;
        return r;
    }
    if(r->x > x){// ilk tek elemandan küçük durum
        // root değişiyor
        node * temp = (node *)malloc(sizeof(node));
        temp->x = x;
        temp->next = r;
        node *iter = r;
        while (iter->next != r )
          iter = iter->next;
        iter->next = temp;
        return temp;
    }

    // özel bir durum bu tek bir düğüm varsa.
    // root'un sabit kalabilmesi için pointer'a ihtiyaç var.
    node * iter = r; // r değişkenlerinin yerine iter yazdım
    while(iter->next != r && iter->next->x < x){
        iter = iter->next;
    }

    node * temp = (node *)malloc(sizeof(node));
    temp->next = iter->next;
    iter->next = temp;
    temp -> x = x;
    return r; // r nin değişme durumu yok hep aynı

}

int main(){
    node * root;
    root = NULL;
    root = ekleSirali(root,400);
    root = ekleSirali(root,40);
    root = ekleSirali(root,4);
    root = ekleSirali(root,450);
    root = ekleSirali(root,50);
    bastir(root);
    root = sil(root,50);
    bastir(root);
    root = sil(root,999);
    bastir(root);
    root = sil(root,4);
    bastir(root);
    root = sil(root,450);
    bastir(root);
}
