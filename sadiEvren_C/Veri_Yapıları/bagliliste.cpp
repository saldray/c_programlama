/*
 *struct n{
   int x;
   n * next;
 }

n isminde bir struct tanımlanmış
struct'ın içinde bir tane pointer var.

n * next
n tipinde ve bir sonraki elemanı gösteriyor

data, veri burada x, buraya istedimiz kadar integer ya da
veri anlamında ne varsa yazılabilir.

struct n {
  int x;
  char * isim;
  char * soyisim;
  n * next;
}

biz genelde linked listlerde bir tane integer tutabiliyorsak
gerisini de tutubiliriz diye basıtleştirerek diğer verilerden
çok bahsetmiyoruz. Bu video serisinde de sadece bir tane
integer veri tutacağız. node'ında düğümünde

struct n {
  int x;
  n * next;
}
n * root;

bu da bize ilk elemanı gösterecek
sonraki elemanlara erişmek için ise root'un bir sonraki elemanı
root -> next

struct n {
int x;
n * next;
}
n * root;
root -> next
root -> next -> next

root'un gösterdiğinin sonrakisinin sonrakisi


 */


#include <stdio.h>
#include <stdlib.h> // malloc için dahil ettik
// main() den önce struct kullanmakta fayda var

struct n{
  int x;
  n * next;
};
typedef n node;

int main(){
  node * root;
  root = (node *)malloc(sizeof(node));
  root -> x = 10;
  root -> next = (node *) malloc (sizeof(node));
  root -> next -> x = 20;
  root -> next -> next = (node *) malloc(sizeof(node));
  root -> next ->next -> x = 30;
  node * iter;
  iter = root;
  printf("%d", iter->x);
  iter = iter -> next;
  printf("\n%d", iter -> x);

}

// yukarıda hafızada bir kutu oluşturacağım bunu nasıl oluşturacağım
// dahil ettiğimiz stdlib.h kütüphanesinin içindeki malloc
// komutu ile
// malloc (void *) döndürür
// void pointer'ı döndürür. Tipsiz döndürür.
// dolayısıyla (node *)'a type cast ediyorum (node yıldız'a)
// root = (node *)malloc(sizeof(node))
// bir tane node'un hafızada kapladığı alan kadar bana yer
// ayır yani memory allocation yap Ben bu alanı bir node
// olarak kullanacağım
// ve bunuda şu anda root gösterecek
// bir tane kutu oluşturduk ve burayı root gösteriyor

// root -> x = 10;
// oluşturmuş olduğumuz kutunun
// root (node* root) tarafından gösterilen kutunun data kısmına
// 10 koy

// ve hatta şunu da yapabilirsiniz
// root -> next
// root'un next'i nereden geldi
// root, struct yapısında bir yapıyı gösteriyor dolayısıyla
// dolayısıyla root bir pointer olduğu için root'un point
// ettiği kutunun next'i yani

// boş bir kutu oluşturduk ve bu kutu root'un next i
// root -> next = (node *) malloc (sizeof(node));
// malloc (sizeof node) hafızada yeni bir kutu oluşturacak
// bu kutuyu oluşturuyorum ve oluşturduğum kutuyu
// root'un next'ine koyuyorum

//                |      |       |       |       |       |
// node * root--- |data  |pointer|-------|       |       |
//                |      |next   |       |       |       |

// bu kutu, root -> next = (node *) malloc (sizeof(node));
// yani root'un next'i
// root un gösterdiği node(düğümün) next'i
// ilk oluşturduğum kutu tarafından gösterilecek. Onu bir nevi
// bağlamış oldum.
// root'un next'i

// root -> next -> x = 20;
// root'un, next'inin, next'inin x değerine 20 değerini atadım

//                |      |       |       |       |       |
// node * root--- |data  |pointer|-------|  20   |       |
//                |      |next   |       |       |       |


// root -> next -> next = (node *) malloc(sizeof(node));
// root -> next ->next -> x = 30;

//               _______________   ___________   _________
//               |     |       |   |    |    |   |   |   |
// node * root---|data |pointer|---| 20 |next|---| 30|   |
//               |     |next   |   |    |    |   |   |   |

// bir müddet sonra next'lerin yazılması işin içinden
// çıkılmayacak bir hal alıyor.
// bunun için bir tane daha root gibi bir pointer oluşturuyoruz
// node*iter

//               _______________   ___________   _________
//               |     |       |   |    |    |   |   |   |
// node * root---|data |pointer|---| 20 |next|---| 30|   |
// node * iter---|     |next   |   |    |    |   |   |   |

// iterater yine node yıldız tipinde node * iter
// istediğimiz bir node'u point edebiliriz.

// bir tane tanımlayalım
// node * iter
// iter = root;
// iter, root'un gösterdiği yeri göstersin.
