// #include <stdio.h>

// int main(){
//   for(int i = 0;i<10;i++){
//     printf("%d\n",i);
//     if(i==5)
//       break;
//   }
// }

// normalde gcc bunu hata olarak kabul eder gcc de
// saf pure c bunu ister
// int i;
// for(i=0;i<10;i++)

#include <stdio.h>

int main(){
  for(int i = 0;i<10;i++){
    if(i==5)
      continue;
    printf("%d\n",i);
  }
}
