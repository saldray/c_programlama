// Ornek

// Girilen sayının asal olup olmadığınğ bulan kod

// Burada yine yeni bir şey öğreneceğiz
// flag denen bir yapı var. Bu gene kullanılan taktiklerden birisidir.
// Bir sayıyı okuyoruz önce asal olup olmadığını kontrol edeceğimiz sayı b de duruyor
// a<b-1 e kadar kendinden bir önceki kendine tam bölündüğünü
// biliyoruz
// kendinden bir önceki sayıya kadar gelsin
// a++ birer birer arttırsın, a her adımda bir artsın
// tam bölmek ne demek bölüm işleminden kalan sıfır demek
//   if(b%a == 0)
// bölerse şayet flag = 1 olacak
// flag = 1; flag 1 olması demek bölen bir şey bulduk demek
// bu flag ilk başta 0 başlıyor. Döngünün sonuna kadar bunun
// 0 lığına zeval gelmezse girip 0 çıkarsa temiz çıkarsa if e
// girmez bunu bölen bir sayı yok demektir ama flag sıfır girip
// 1 çıkıyorsa bunu bir yapmıştır Yani bunu bölen tam bir sayı
// vardır. Sıfırın sıfır girip sıfır çıkması bize hiç bir sayının
// tam bölmediğini dolayısıyla sayının asal olduğunu gösterir ama
// bir tane sayının bile tam bölüyor olması sayının asal olmadığını
// gösterir.

// break  komutu döngüleri kırmak için kullanılıyor Yani ben
// bir tane bile bölen bir sayı bile bulduysam Mesala diyelim ki
// 18 sayısı asal mı?
// 2'ye böler mi 3'e böler mi ...
// Daha başlar başlamaz 2, 18'i böler gerisini test etmenin bir
// anlamı yok döngüyü kırayım daha fazla devam etmesin.

#include <stdio.h>

int main(){
  int a=1,b;
  scanf("%d",&b);
  int flag = 0;
  while(a<b-1){
    a++;
    if(b%a==0){
      flag = 1;
      break;
    }
  }
  if(flag == 0)
    printf("sayi asal");
  else
    printf("sayi asal değil");
}
