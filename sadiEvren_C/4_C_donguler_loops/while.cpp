// While Döngüsü

// int i=0;
// while(i<=10){
//   printf(" %d",i);
//   i++;
//  }

// while döngüsünde başlangıç değerini döngüden önce yapmamız
// gerekiyor.

// Koşulu while'ın içine yazıyoruz. Aynı if gibi kullanılıyor while
// yani diyor ki şu doğru olduğu sürece while(i<10) burada
// dönmeye devam et

// i++ ne yapıyordu? i'nin değerini bir arttırıyordu

// i, 0(sıfır)dan başlayacak, i'nin değerini sıfırı ekrana basacak
//   i++ i,yi bir attırdı i, 1 oldu
//   i, 10 oldu, yazdı. i++, 11 oldu
//   i<=10 kontrol edecek i 11, i, 10 dan küçük değil.
//   yazdırmayacak döngü kırılacak, altında hangi satırlar varsa
//   onları yapacak
