// Bölüm İçeriği
// * Döngüler
// - Basit Döngüler
// - İç içe döngüler (nested loops)
// - Örnekler

// Bir döngü üç tane parçadan oluşur
// 1- Başlangıç değeri
// 2- Bitiş değeri
// 3- Adım değeri

// 1- Initialization
// 2- Condition
// 3- Iteration

// #include <stdio.h>

// int main(){
//   for(int i=21;i<=50;i+=2){
//     printf(" %d",i);
//   }
// }

// başlangıç değeri i'nin 21'den başlaması
// 50'den küçük olduğu sürece, koşul aslında bitiş değeri diye
// veriyoruz ama koşul ve
// adım değeri

// Bu kod nasıl çalışır diye bakalı
//   i diye bir değişken tanımla içine 21 değerini ata
//   i=21 sonra
//   i<=50 (i elliden küçük eşit olduğu sürece) çalışmaya devam et
//   yani i=21 ken 50 den küçük eşit mi evet çalışmaya devam
//   edecek yani bu döngüyü çalıştıracak
//   i+=2 neydi
//   i=i+2 bu nedemek
//   i nin değerini al, iki arttır, i'nin içine koy
//   i'nin değeri 21, 21 al, i'nin içine koy, iki arttır.
//   i=21+2, i=23
//   her döngünün dönüşünde de i'yi basacak yani i'nin ilk değeri
//   21
//   Ekran:
//   21 ilk i'nin değerini basacak ekrana
//   21 23 25 27 29 31 ... 49

//   i=49+2, i=51
//   i<=50 döngüyü artık sağlamaz ve döngüden çıkılır.

#include <stdio.h>

int main(){
  for(int i=21;i<=50;i+=2){
    printf(" %d",i);
  }
}
