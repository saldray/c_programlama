// sağa dayalı yukarıdan aşağıya azalan dik üçgen

// #include <stdio.h>

// int main(){
//   for(int n = 4;n>=0;n--){
//     //4-n yildiz
//     for(int i = 0;i<4-n;i++){
//       printf(" ");
//     }
//     for(int i = 0;i<=n;i++){
//       printf("*");
//     }
//     printf("\n");
//   }
// }


//   0       5
//   1       4
//   2       3
//   3       2
//   4       1
// bosluk  yildiz

// aralarında nasıl bir ilişki var dikkatli bakarsak
// toplamlarının 5 olduğunu görürüz.
// bosluk + yildiz = 5

//       5             0
//       4             1
//  5-   3      =      2
//       2             3
//       1             4
//     yildiz       bosluk

// Şimdi burada yapacağımız şey şu
// Toplamları 5 yaptığı için 5 ten çıkartacağım
// ama yıldız koymadan önce bunu yapmam lazım
// Yani yıldız koymadan önce istediğim kadar boşluk
// koyduran bir döngüye daha ihtiyacam var.

// ***amaç i'ye baglı bir şeye dönüştürmek***


// #include <stdio.h>
//
// int main(){
//   int i,j;
//   for(i = 5;i>=1;i--){
//     for(int k = 0;k<5-i;k++){
//       printf(" ");
//     }
//     for(j=1;j<=i;j++){
//       printf("*");
//     }
//     printf("\n");
//   }
// }

#include <stdio.h>

int main(){
  for(int i=0;i<=5;i++){
    for(int j = 0;j<i;j++){
      printf(" ");
    }
    for(int j = 0;j<5-i;j++){
      printf("*");
    }
    printf("\n");
  }
}


