# Using Nested Loops: You can use nested loops to print the hollow square.
# The following Python code demonstrates how to create a hollow square based on user
# input for the side length


def hollow_square(side):
    for i in range(side):
        for j in range(side):
            if i == 0 or i == side - 1 or j == 0 or j == side - 1:
                print('*', end='')
            else:
                print(' ', end='')
        print()

# Example usage:
side_length = int(input("Please enter the side length of the square: "))
hollow_square(side_length)



# Using a Single Loop: Alternatively,
# you can achieve the same result using a single loop. Here’s another example

def hollow_square(side):
    for i in range(side):
        for j in range(side):
            if i == 0 or i == side - 1 or j == 0 or j == side - 1:
                print('*', end=' ')
            else:
                print(' ', end=' ')
        print()

# Example usage:
side_length = int(input("Please enter the side length of the square: "))
hollow_square(side_length)
