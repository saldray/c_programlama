// do..while Döngüsü

// int i=0;
// do{
//   printf(" %d",i);
//   i++;
//  }while(i<=10);


// C'de aslında 3 tane döngümüz var.
// for döngüsi
// while döngüsü
// do..while döngüsü

// Burada da initialization, ilkleme, ilklendirme değişkenin ilk
//   değerini ataması
//   do deyip döngü başlıyor, daha sonra while ile sonda döngüyü
//   kontrol ediyor.

// !! Burada dikkat edilmesi gereken önemli bir şey var.
//   for döngülerinin tamamı while döngüsü olarak yazılabilir.
//   while döngülerinin tamamı for döngüsü olarak yazılabilir.

// ama do..while döngüsü biraz enteresan bir döngüdür.
// şöyle enteresan bir döngüdür ki. Örnek yapmaya çalışalım.


// Bu kodda i, 21 den başlamış i, 50 den küçük eşit olduğu sürece
//   2 arttırılarak devam ediyor

// #include <stdio.h>

// int main(){
//   for(int i=21;i<=50;i+=2){
//     printf(" %d",i);
//   }
// }

// Ben soruyorum buraya 51 deseydim

// #include <stdio.h>

// int main(){
//   for(int i=51;i<=50;i+=2){
//     printf(" %d",i);
//   }

// Buraya 51 deseydim bu döngü çalışırmıydı.

// Çalışmazdı niye i'nin değeri 51 olacak i 50 den küçükken
// çalış demişiz i,51 ken i den küçük koşulu (i<=50) yerine
//   getirilmediği için hiç girmeyecek dolayısıyla bu kod hiç
//   çalışmayacak i, 51 olduğu için Burada kodda ne yazdıysak
//   c için hiç önemli değil. Bunları atlayıp bir sonraki
//   satırdan devam edecek Bununla birlikte
//   while döngüsünde de aynı şey olabilir


// while döngüsü

// int i=0;
// while(i<=10){
//   printf(" %d",i);
//   i++;
//  }

// int i=11;
// while(i<=10){
//   printf(" %d",i);
//   i++;
//  }

// i, 11 ken bu döngüye girecek mi? girmeyecek girmediği için
//   ne olacak içinde ne yazıldığı hiç önemli değil tak atlayacak
//   sonrasından devam edecek


// do while a geldiğimizde i'nin değeri 11 olsaydı ne olacaktı

// int i=0;
// do{
//   printf(" %d",i);
//   i++;
//  }while(i<=10);

// int i=11; // i'nin değeri 11 olsaydı
// do{
//   printf(" %d",i);
//   i++;
//  }while(i<=10);

// do while döngüsü önce yapıp sonra kontrol ettiği için
// bu 11 değeri bir kere ekrana basılacaktır
// do'nun içine girecekti bir kere basılacaktı değeri bir
//      arttıralacaktı (i++) ve i<=10 kontrol edecek sağlanmadığı
//      için döngü devam etmeyecek ve döngüden çıkacaktı dolayısıyla
//      while döngüleri ve for döngülerinde koşul başta kontrol
//   edildiği için koşul sağlanmadığı durumlarda döngüye hiç
//   girmiyor
//   do while döngülerinde ise koşul sonda kontrol edildiği için
//   koşul yanlış da olsa o döngünün dönmemeside gerekiyor olsa
//   bir kere girip kontrol ediyor

// Bu bir avantaj olarakta kullanılabilir. Bazen koşul kontrol
// etmeksizin git bir işler yap diyebilirsiniz Git kullanıcıdan
// bir sayı oku, başta oku sonra kontrol et falan gibi durumla
//   olabilir. böyle durumlarda kullanıla bilir ama aklımızda
//   bulunsun Bir kenara yazalım

//   do while döngüleri kontrolü sonda yaptığı için bir kere daha
//   dikkat edilmesi gereken döngülerdir

//   do while döngüsü while döngüsüne dönüştürülebilir dönüştürülmez diye değil


// int i=11;
//   printf(" %d",i);
//   i++; // Burası yazılmaya da bilir 12 olur çünkü
// while(i<=10){
//   printf(" %d",i);
//   i++;
//  }
