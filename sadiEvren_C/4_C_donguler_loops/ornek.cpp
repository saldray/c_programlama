// 20 ile 50 arasındaki tek sayıları ekrana bastıran kodu
// yazalım

// #include <stdio.h>

// int main(){
//   for(int i=20;i<=50;i++){
//     printf(" %d",i);
//   }
// }

// 20 den 50 kadar olan sayıları ekrana bastırır.

// #include <stdio.h>

// int main(){
//   for(int i=20;i<=50;i++){
//     if(i%2 == 1)
//       printf(" %d",i);
//   }
// }

// (i%2) sayının 2'ye bölümünden kalan demek
// Bir sayıyı 2'ye böldüğümüzde 1 kalıyorsa
// 1 ise tek sayıdır
// 0 ise çift sayıdır. Dolayısıyla burada
// 1 olması durumu tek olduğu anlamına gelir ekrana basacağımız
// sayı tek
// 20'nin 2'ye bölümünde kalan ne 0 tek mi? Hayır değil
// çifttir. Dolayısıyla bu koşul sağlanmadığı için
// if koşuluna girmeyecek. Sonra 21 oldu. if sağlanıyor mu?

// Şöyle de yapabilirdik

// int main(){
//   for(int i=21;i<=50;i+=2){
//     //if(i%2 == 1)
//     print1(" %d",i);
//   }
// }

// if e gerek olmadan da yazılabilirdi. Ancak burada seriyi
// tanımak gerekiyor.


// Örnek Kod

// 100'den 20'ye kadar, 3'e ve 7'ye tam Bölünen sayıları,
// büyükten küçüğe doğru yazdıralım.

// #include <stdio.h>

// int main(){
//   for(int i=100;i>=20;i--){
//     if(i%3==0&&i%7==0)
//       printf("%d ",i);
//   }
// }


// Örnek Kod

// Kullanıcıdan 3 adet sayı okuyup en Büyüğünü ekrana yazan
// kod
// girilen her sayının pozitif olduğu var sayılıyor
// eb(en büyük) 0'dan başlatılır

// #include <stdio.h>

// int main(){
//   int girilen;
//   int eb = 0;
//   for(int i=0;i<3;i++){
//     scanf("%d",&girilen);
//     if(girilen>eb)
//       eb = girilen;
//   }
//   printf("eb: %d",eb);
// }

// 5 sayı, 3 sayı ama kaç sayı olduğunu bilmuyoruz
// buna sentimet deniyor sentimental sentiment
// Matrix filminde de vardı bu sentiment'ler
// Bu bitiş durumunu belirten durumlar
// yok ediciler sonunun getiriciler
// -1 girilene kadar ilk sentiment değerimiz -1
// -1 girilene kadar bizim döngümüz devam edecek
// istenilen şey -1 girilene kadar girilen sayılar içinde
// en büyüğünü bulmak Yani kaç sayı olduğunu bilmiyoruz

// Örnek Kod

//   Kullanıcı, -1 girene kadar yazılan sayıların en büyüğünü
//   bulan kod


// #include <stdio.h>
// int main(){
//   int girilen = 0;
//   int eb = 0;
//   while(girilen != -1){
//     scanf("%d",&girilen);
//     if(girilen>eb)
//       eb = girilen;
//   }
//   printf("eb:%d",eb);
// }

// girilen sayı (!=-1) den farklıyke
// sayı -1'e eşit olduğunda döngü bitecek
// -1 bitişi belirten bir sayı
// sayıyı okuyoruz en büyüğü güncelliyoruz
// daha büyükse tabii ki, sonrada en büyüğü basıyoruz
// -1 dışındaki bütün sayılarda döngümüz devam ediyor
// çalışıyor


// Örnek Kod

// Kullanıcı -1 girene kadar girilen sayıların ortalamasını
// bulan kod


// iki tane değere ihtiyacımız vardır ortalamayı bulurken
// Bir sayıların toplamı
// İki kaç tane sayı olduğu

// Bunu yaparken şuna dikkat etmemiz lazım
// Döngü bir defa -1 girilene kadar devam edecek
// Bu while döngüsü ile rahatça kontrol edilebilecek
// Bizim de yapabileceğimiz bir şey while(girilen !=-1)
// Şimdi bizim
// Kaç sayı girildiğini ve şimdiye kadar girilen sayıların
// toplamını tutuna iki tane değişkene ihtiyacımız var

// Bu değişkenlerin özelliği şu
// Döngünün her dönüşünden bağımsız olarak bir yerde
// tutulması lazım
// Yani döngü döndüğünde ne kadar dönerse dönsün bu
// sayılar etkilenmeden artarak bir yerlerde durması lazım
// Bunu şu anlamda söylüyorum

// 23 33

// ilk kodu değiştirmeden yazalım sık yapılan hataları görelim

// ortalama.cpp

// #include <stdio.h>

// int main(){
//   int girilen=0;
//   int ort;
//   int n = 0;
//   int toplam = 0;
//   while(girilen !=-1){
//     scanf("%d",&girilen);
//     n++;
//     toplam = toplam + girilen;
//   }
//   printf(" ortalama : %d",toplam/n);
// }


// Burada bir bug var -1 de sayılıyor biz bunu istemiyoruz

// #include <stdio.h>
// int main(){
//   int girilen=0;
//   int ort;
//   int n = 0;
//   int toplam = 0;
//   while(girilen !=-1){
//     scanf("%d",&girilen);
//     n++;
//     toplam = toplam + girilen;
//   }
//   printf("kac siyi girildi : %d, toplam : %d ortalama : %d",n,toplam,toplam/n);
// }

// -1 ile de topluyor. Bunu istemiyoruz.

// #include <stdio.h>
// int main(){
//   int girilen=0;
//   int ort;
//   int n = 0;
//   int toplam = 0;
//   while(girilen !=-1){
//     scanf("%d",&girilen);
//     n++;
//     toplam = toplam + girilen;
//   }
//   n--;//n'nin değeri sonradan 1 artırılacağı için 1 azalt
//   toplam ++;// sonda -1 ile topladığı için 1 ekliyoruz.
//   printf("kac siyi girildi : %d, toplam : %d ortalama : %d",n,toplam,toplam/n);
// }


// #include <stdio.h>

// int main(){
//   int girilen=0;
//   int ort;
//   int n=0;
//   int toplam = 0;
//   while(girilen!=-1){
//     scanf("%d",&girilen);
//     n++;
//     toplam = toplam + girilen;
//   }
//   printf("ortalama:&d",toplam\n);
// }
