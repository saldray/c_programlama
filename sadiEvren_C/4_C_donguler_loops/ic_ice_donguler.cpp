// Örnek Tablo

//   4  6   8   10
//   6  9   12  15
//   8  12  16  20
//   10 15  20  25

// iç içe döngürler

// genelde bir tablo şeklinde çalıştığı
// bir x, bir de y boyutunun olduğu veya
// bir satır bir sütun boyutunun olduğunu söyleyebiliriz.

// Burada bir çarpım tablosu örneği verilmiş 5'e 5

// Burada ilk kez bir iç içe döngünün nasıl olduğunu anlamaya
// çalışacağız

#include <stdio.h>

// int main(){
//   int i,j; // foo, bar
//   for(i=1;i<=5;i++){
//     for(j=1;j<=5;j++){
//       printf("%d ",i*j);
//     }
//     printf("\n");
//   }
// }

// Her i değeri için j 1,2,3,4,5 çalışacak j, i'den çok daha
// hızlı bir döngü

#include <stdio.h>

int main(){
  for(int j=1;j<=5;j++){
    for(int i=1;i<=5;i++){
      printf("%d ",i*j); // sadece i, ("%d ",i);
    }
    printf("\n");
  }
}
