/* #include<stdio.h> */

/* int main(){ */
/*   printf("Merhaba Dünya"); */
/*   return 0; */
/* } */

/*
include :
bir kütüphaneyi programımıza dahil ediyoruz.
O kütüphanenin içindeki programları kullanabiliyoruz.
C normalde herhangi bir fonksiyon filan tanımaz.
C 'de printf diye bir şey yok. Bunlar <stdio.h> içinde
gelen şeyler.

main() :
İşletim sistemi tarafından çağırılan özel bir
fonksiyondur.
main 'i ana fonksiyon olarak ilk çalıştırılan fonksiyon
olarak kabul edeceğiz.
*/

# include <stdio.h>

int main(){
  printf("Merhaba C");
}
