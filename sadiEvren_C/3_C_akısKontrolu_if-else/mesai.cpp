// Örnek Uygulama
// * Bir şirket, çalışanlarına, fazla mesai ücreti ödemektedir.
//   Sizden ücreti hesaplayan bir program yazmanız isteniyor.
//   Programın özellikleri aşağıdaki şekildedir:
// * 10 saate kadar saat başına 5 lira
// * 10 ile 20 saat arasında, saat başına 3 lira
// * 20 saatten sonrası için, saat başına 2 lira


// #include <stdio.h>

// int main(){
//   int mesai;
//   printf("lütfen mesai saatini giriniz");
//   scanf("%d",&mesai);
//   if(mesai<=10){ // adam mesai'ye 10 saat kalıyorsa fiks olarak 50 lira alıyor
//     printf("ucret = %d",mesai*5);
//   }
//   else if(mesai >10 && mesai <=20)
//     printf("ucret = %d",10*5 + (mesai - 10) * 3);
//   else
//     printf("ucret = %d",10*5 + 10 * 3 + (mesai -20) * 2);
// }


// Şimdi burada  iyileştirme yapabilirmiyiz.


// #include <stdio.h>

// int main(){
//   int mesai;
//   printf("lütfen mesai saatini giriniz");
//   scanf("%d",&mesai);
//   if(mesai<=10){ // adam mesai'ye 10 saat kalıyorsa fiks olarak 50 lira alıyor
//     printf("ucret = %d",mesai*5);
//   }
//   else if(mesai >10 && mesai <=20)
//     printf("ucret = %d",50 + (mesai - 10) * 3);
//   else
//     printf("ucret = %d",80 + (mesai -20) * 2);
// }


// ufak bir trick(püf nokta, hile) daha var burada
// mesai 10'dan küçükse if'i çalıştıracak
// else if ne zaman çalışıyordu.
// üstündeki if olmadığı zaman yani if in false (0) olması
// yanlış olması durumunda
// Şimdi mesai 10'dan küçük değilse ve eşit değilse else if
// çalışacak o zaman benim burada mesai 10'dan büyük müdür
// diye bir daha kontrol etmemin bir anlamı var mı? yani
// mesai 10dan büyük mü diye bir daha kontrol etmemin bir
// anlamı var mı? zaten else if durumu doğru değilse if'de
// çalışacak else if'e hiç gelmeyecek o zaman else if içindeki
// mesai>10 u silelim mesai <= 20 ise else if'i çalıştır


#include <stdio.h>

int main(){
  int mesai;
  printf("lütfen mesai saatini giriniz");
  scanf("%d",&mesai);
  if(mesai<=10){ // adam mesai'ye 10 saat kalıyorsa fiks olarak 50 lira alıyor
    printf("ucret = %d",mesai*5);
  }
  else if(mesai <=20)
    printf("ucret = %d",50 + (mesai - 10) * 3);
  else
    printf("ucret = %d",80 + (mesai -20) * 2);
}
