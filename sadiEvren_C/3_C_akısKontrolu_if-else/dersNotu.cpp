// Örnek Uygulama
// * Bir ögrencinin notu, 100 üzerinden girildiğinde aşağıdaki
// değerlere göre harf notu karşılığını hesaplayan programı yazınız:
// * 90 ve üzeri AA
// * 80 ile arası BA
// * 70 ile 80 arası BB
// * 70 ve altı F


// Hatalı Kod

// #include <stdio.h>

// int main(){
//   print("notu giriniz");
//   int a;
//   scanf("%d",&a);
//   if(a>90){
//     printf("AA");
//   if(a>80 && a<90)
//     print("BA");
//   if(a>70 && a>80)
//     printf("BB");
//   else
//     printf("F");
//   }
// }

// a>90 yani 90'ın üzerinde ise AA demişiz
// sonra tekrar a>80 && a<90 bir daha kontrol etmeye gerek
// var mı yok neden 90'ın üzerinde değilse zaten 90'ın altın
// dadır ikinci bir kere && a<90 kontrolünün yapılmasına
// gerek yok.
// Bir hata daha var  eğer 90 ise ne yapacağız.
// Bu koda 500 versek ne olur AA alır.
// Bu kod ayrıca if'lerle ayrılmış.
// üstteki if'in alttaki if'in arasında hiçbir ilişkisi yok
// her if'de sıfırdan başlıyor
// ee bu else kimin hang if'in
// En son yazılan if' in önceki if'lerle hiçbir ilgisi yok
// Şimdi adam 95 aldı biz AA yı basacağız ve else'de girecek
// neden çünkü bir üstündeki if'i sağlamadığı için  bir
// else bir üstündeki if'i sağlamadı sürece else girecek
// adam hem bir AA almışsındır hem de bir F yazıyoruz.


// Doğru Kod

// #include <stdio.h>

// int main(){
//   printf("notu giriniz");
//   int a;
//   scanf("%d", &a);

//   if(a>90)
//     printf("AA");
//   else if(a>80)
//     printf("BA");
//   else if(a>70)
//     printf("BB");
//   else
//     printf("F");
//   return 0;
// }



#include <stdio.h>

int main(){
  printf("notu giriniz");
  int a;
  scanf("%d", &a);

  if(a>=90)
    printf("AA");
  else if(a>=80)
    printf("BA");
  else if(a>=70)
    printf("BB");
  else
    printf("F");
  return 0;
}
