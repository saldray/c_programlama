// #include <stdio.h>

// int main(){
//   int(a,b);
//   scanf("%d%d",&a,&b);
//   if(a>b)
//     printf("%d",a);
//   else
//     printf("%d",b);
// }

// #include <stdio.h>

// int main(){
//   int a,b;
//   scanf("%d%d",&a,&b);
//   if(a>b){
//     printf("%d",a);
//   }
//   else{
//     printf("%d",b);
//   }
// }

// if'ten sonra bir blok başlıyordur ve else'den sonra bir
// blok başlıyordur. Kural olarak bir küme parantezi(küme
// ayracı, curly bracket) ile bloğun başlangıcını belirtmekte
// fayda vardır.
// ama bir kolaylık olarak
// küme ayracı açılmamış olması burada bir blok olmadığı
// anlamına gelmiyor.
// if in ya da else in bir kural olarak altındaki ilk satır
// tek satır bloğun bir parçası olarak kabul edilir. Ben
// curly bracket açmazsam şayet if'in ya da else'in altında
// gördüğü ilk satırı o, if' ya da else bloğunun bir parçası
// olarak kabul eder.


// #include <stdio.h>

// int main(){
//   int a,b;
//   scanf("%d%d",&a,&b);
//   if(a>b)
//     printf("%d",a);
//     printf("deneme");
//   else // dangling else
//     printf("%d",b);
// }

// if in altındaki ikinci printf if'in dışında olacak
// normalde if in dışında ben bir printf yazabilir miyim?
// yazabilirim if bitmiş demektir Her durumda şu ikinci
// printf i çalıştır diyebilirim
// if i ya çalıştıracak ya da çalıştırmayaca ama
// printf("deneme") yi her durumda çalıştıracak
// peki buradaki sorun ne?
// else, else'in var olabilmesi için bir tane if in var
// olabilmesi gerekir.
// if olmadan else olabilir mi?
// olamaz
// Ben buraya printf("denenem"); koyduğumda if ile else'in
// bağlantısı kesildi. Bu ne demek Burada bir else var ama
// if'i yok Buna
// dangling else deniyor yani sallantıda, boşta, boşlukta else
// Herhang bir else olmama durumu
// dangling else
// Çalıştırırsak bir hata verecek bunu düzeltmek için

// #include <stdio.h>

// int main(){
//   int a,b;
//   scanf("%d%d",&a,&b);
//   if(a>b){
//     printf("%d",a);
//     printf("deneme");
//   }
//   else // tek satır olduğu için {} gerek yok
//     printf("%d",b);
// }
