#include <stdio.h>
#include <stdlib.h> // malloc un kullanılabilmesi için

int main(){
	int *p; // bir pointer tanımladım
	int a = 10;
	p = &a; // a değişkeninin adresini pointer'a atadım
	// artık a dediğimde de *p dediğimde de aynı değeri görüyorum
	printf("%d",*p);
	*p = 20;
	printf("\n %d",a);
}
