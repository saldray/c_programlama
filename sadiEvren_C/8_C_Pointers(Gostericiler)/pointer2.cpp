// fonksiyonlar üzerinde pointer tanımlama vardı

#include <stdio.h>
#include <stdlib.h>

void f(int *);

int main(){
	int *p;
	int a = 10;
	p = &a;
	printf("%d",*p);
	*p = 20;
	printf("\n%d",a);
	int *q = (int *)malloc(sizeof(int)*a);
	q[3] = 70;
	printf("\n%d",q[3]);
//	f(q[3]);
	f(&a); // fonksiyonda pointer olduğu için adresini verdim
	printf("\n%d",a);
	free(q);
	return 0;
}
// int f(int * a){
void f(int *a){
	*a = 80;//orijinal a'nın gösterdiği yer
	
}
