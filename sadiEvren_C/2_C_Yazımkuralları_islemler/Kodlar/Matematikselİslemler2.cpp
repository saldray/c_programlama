#include <stdio.h>

int main(){
    int a=10;
    int b=3,c=7;
    printf("%d",a+b);
    printf("\n%d",a<<2);
    printf("\n%d",a>>2);
    printf("\n%d",a+b*2);
    printf("\n%d",a%b);
    printf("\n%d",a==b);
    printf("\n%d",a>b);
    printf("\n%d",a++);
    printf("\n%d",++a);
    return 0;
}


// 2'nin ikilik tabandaki değeri nedir?
// 2¹ + 2°
// 2 = 0010  ikilik tabandaki değeri

// a<<2;  okların yönünde kaydırılır.
// 0000 0010
// 0000 0100
// 0000 1000

// a>>2; sağa kaydır dediğimizde

// 0010 iki kere sağa kaydırıldığında bu dünyadan çıkmış olacak
// sonuç sıfır olacak.

// 10'nun ikilik sistemdeki (tabandaki) yazılımı

// 10|2   5|2  2|2    (1010)ikilik taban
//   10 5   4 2  2 1
//   -      -    -
//   0      1    0
//
// 0.2°+ 1.2¹ + 0.2² + 1.2³ = 0 + 2 + 0 + 8 = 10
//
// 0000 1010
// 0001 0100
// 0010 1000  1.2³ + 1.2üssü5  8 + 32 = 40

// a++ post increment, Sonradan arttır
// ++a pre increment,  Önceden arttır

// pre Ön demek
// post son demek

// a++ burada a'nın değerini al işleme tabii tut işlemin sonucu
// ne çıkarsa çıksın onunla çok ilgilenme ama işlem bittikten
// sonrada bir arttır

// şimdi bu ne olacak a'nın değerini aldı a'nın değeri 10 du.
// a'nın değerini aldı ekrana bastırdı. 10 olarak ve değerini
// 1 arttırdı.
// a'nın hafızadaki değeri ne şu an. 11
// sonra buraya geliyor.

// ++a değerini önce arttır sonra işleme sok.
// a'nın orijinal değeri 10 ama biz burada a++ ile a'nın değerini
// bir arttırdık.
// dolayısıyla a'nın değeri 11 şu anda, 1 daha arttırıyoruz.
// 12 oluyor sonra işleme sokuyoruz dolayısıyla değeri 12
// oluyor.







































