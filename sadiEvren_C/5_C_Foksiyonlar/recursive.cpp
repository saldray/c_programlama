// Bir fonksiyonun kendisini çağırması durumudur.
// Örneğin faktöriyel için:
// n! = n*(n-1)!
// Formülünde ! İşleminin tanımında yine ! cinsinden
// bir fonksiyon bulunur:
// int f(int n){
// if(n==0)
// return 1;
// return n*f(n-1);
// }

// recursive (özyineli) fonksiyonlar
// recursive (özyineli) fonksiyonlar döngülerin alternatifidir.
// Yani siz döngüleri yazarken kullandığınız yapıyı
// recursive yazarken de kullanabilirsiniz
// Daha basitleştirecek konuşacak olursak
// recursive fonksiyonlar döngüdür.
// Bütün döngüler recursive fonksiyondur.
// Bütün recursive fonksiyonlar döngü olarak yazılabilir.


// #include <stdio.h>
// int fib(int);
// int main(){
//   int x;
//   scanf("%d",&x);
//   printf("%d",fib(x));
// }
// int fib(int n){
//   if(n==1||n==0)
//     return 1;
//   return fib(n-1)+fib(n-2);
// }
