// Her sayı kendinden önceki iki sayının toplamından ibaret
// Verilen sıradaki fibonacci sayısını bulan kod
// iterativ yaklaşım, yinelemeli yaklaşım

#include <stdio.h>

int fib(int);
int main(){
  int x;
  scanf("%d",&x);
  printf("%d",fib(x));
}
int fib(int n){
  int a = 1, b =1;
  int c;
  for (int i = 3;i<=n;i++){
    c = a + b;
    a = b;
    b = c;
  }
  return c;
}

// a    b    c
// 1    1    2  ilk iteration(ilk yineleme)
// 1(b) 2(c) 3
// 2(b) 3(c) 5
// 3(b) 5(c) 8
// 1, 1, 2, 3, 5, 8, 13, 21, 34, 55
