// Bölüm İçeriği

// * Fonksiyonlar
//  - Özyineli Fonksiyonlar (Recursive)
//  - C Dilinde Fonksiyonlar
//  - Örnekler
// * Sorular

// Verilen bir sayının faktöriyelini bulan kod

// #include <stdio.h>

// int fact(int); //prototipi

// int main(){
//   printf("bir sayı giriniz:");
//   int x;
//   scanf("%d",&x);
//   printf("faktoriyeli:%d",fact(x));
// }
// int fact(int x){
//   int sonuc = 1;
//   for(int i=1;i<=x;i++){
//     sonuc*=i;
//   }
//   return sonuc;
// }

// fonksiyonlar genelde include lerimizi yazdıktan sonra
// fonksiyonların prototipleri denilen tanım kısımları
// yer alıyor
// int fact(int);
// Bir tane fact isminde bir fonksiyon var.
// bunun (int) parametresi var.
// int integer değer döndürüyor. Yani bir tane parametre
// alıyor. Bir tane sayı yazılıyor integer tam sayı 8 gibi
// ve sonucunda da fonksiyon çalıştıktan sonra üretilen
// sonuçta bir tane sayı o da çağrıldığı yere geri döndürülecek
// ardından gelende adet main'i yazmaktır
// main fonksiyonu ana fonksiyon olduğu için onu mümkün
// olduğunca küçük gözle görülebilir. Göz önünde bir yerlere
// koymak isteriz.
// Sonrada fonksiyonun kendisi yazılır. Fonksiyonun içeriği yazılır.

// prototipte isim vermek zorunda değilim versem de olabilir

// fonksiyonun çalışa bilmesi için prototipin olması gerekir
// fact(x) fonksiyonun hata vermemesi için tanımlı olması gerekir
// Biz diyoruz ki C'ye ben fact diye bir fonksiyon tanımlayacağım
// ileride Sen bana güven "int fact(int);"
// Bu fact(x) fonksiyonunu gördüğünde hata verme
// içeriğini vermedim ama bu fonksiyonu ileride tanımlayacağımı
// vaad ediyorum. Bana güven
// main() fonksiyonu bitince bir yere gidiyorum.
// Sözümü yerine getiriyorum fact() fonksiyonunu yazıyorim
//

// #include <stdio.h>
// int fact(int);
// int main(){
//   printf("bir sayı giriniz:");
//   int x;
//   scanf("%d",&x);
//   printf("5! + girilen sayının faktoriali : %d", fact(5) + fact(x));
// }
// int fact(int x){
//   int sonuc = 1;
//   for(int i = 1;i<=x;i++){
//     sonuc *=i;
//   }
//   return sonuc;
// }


// #include <stdio.h>

// int fact(int);
// int main(){
//   printf("bir sayı giriniz: ");
//   int x;
//   scanf("%d",&x);
//   printf("girilen sayının faktoriyelinin faktoriyeli : %d", fact(fact(x)));
// }
// int fact(int x){
//   int sonuc = 1;
//   for(int i = 1;i<=x;i++){
//     sonuc *=i;
//   }
//   return sonuc;
// }

// 6 girildiğinde integer değer limitini aşar 0 döner.


// #include <stdio.h>

// int fact(int);
// int main(){
//   printf("bir sayı giriniz: ");
//   int x;
//   scanf("%d",&x);
//   printf("girilen sayının faktoriyelinin faktoriyeli : %d fact : %d", fact(fact(x)),fact(x));
// }
// int fact(int x){
//   int sonuc = 1;
//   for(int i = 1;i<=x;i++){
//     sonuc *=i;
//   }
//   return sonuc;
// }


// Verilen sayıların kombinasyonunu bulan kod

// C(n,r) = n!/(r!(n-r)!)

// 1,2,3,4,5 sayılarından oluşan bir grupta kaç farklı 2li kombinasyon bulunabilir.
// C (5 2)
// #include <stdio.h>

// int fact(int);
// int comb(int,int);

// int main(){
//   printf("iki sayı giriniz:");
//   int x,y;
//   scanf("%d%d",&x,&y);
//   printf("kombinasyonu : %d", comb(x,y));
// }
// int comb(int x, int y){
//   return fact(x)/(fact(y)*(fact(x-y)));
// }
// int fact(int x){
//   int sonuc = 1;
//   for(int i = 1;i<=x;i++){
//     sonuc *=i;
//   }
//   return sonuc;
// }


