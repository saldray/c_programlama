// https://www.sadievrenseker.com/datastr/final_cozum.htm
// Örnek Problem

// o---o---o
// |   |   |
// o---o---o
// |   |   |
// o---o---o

// Yukarıdaki grafikteki kare sayısı 5'tir
// Buna göre verilen bir kare grafik diziliminin
// boyutuna göre (nxn) kaç kare içereceğini
// hesaplayan fonksiyon yazınız.

// Çözüm

// Recursive bir problemdir. Buna göre örneğin
// 3x3lük bir ızgarada 4 adet 2x2lik
// kadar kare ve ilave olarak 1 kare daha bulunacaktır
// int kare_sayisi(int n){
//   if(n==1)
//     return 1;
//   return kare_sayisi(n-1)*4+1;
// }
