#include <stdio.h>

int main() {
    for (int i = 0; i <= 10; i++) {
        for (int j = 0; j <= i; j++) {
            int nf = 1;
            for (int p = 1; p <= i; p++) {
                nf = nf * p;
            }
            int kf = 1;
            for (int p = 1; p <= j; p++) {
                kf = kf * p;
            }
            int nkf = 1;
            for (int p = 1; p <= i - j; p++) {
                nkf = nkf * p;
            }
            printf(" %d ", nf / (kf * nkf));
        }
        printf("\n");
    }
}



// Here’s what the code does:

//     The program starts with the main() function, which is the entry
//     point for execution.  It uses nested loops to generate a Pascal’s
//     Triangle pattern. Let’s break down the loops: The outer loop (for
//     (int i = 0; i <= 10; i++)) iterates from 0 to 10, representing the
//     rows of the triangle.  The inner loop (for (int j = 0; j <= i;
//     j++)) iterates from 0 to the current row number (i), representing
//     the columns within each row.  Inside the innermost loop, three
//     factorials are calculated: nf (numerator factorial) calculates the
//     factorial of i.  kf (k-factorial) calculates the factorial of j.
//     nkf (n-k factorial) calculates the factorial of (i - j).  The
//     expression nf / (kf * nkf) computes the binomial coefficient (n
//     choose k) for each position in the triangle.  The result is
//     printed as a space-separated value using printf.

// The output of this program will be a triangular pattern of numbers,
// where each number represents a binomial coefficient. The first few
// rows of the Pascal’s Triangle will look like this:

// 1
// 1 1
// 1 2 1
// 1 3 3 1
// 1 4 6 4 1
// ...

// Each number in the triangle corresponds to the combination of
// selecting j items out of i items.


// Dış döngü (for (int i = 0; i <= 10; i++)) üçgenin satırlarını temsil
// ederek 0'dan 10'a kadar yinelenir.

// İç döngü (for (int j = 0; j <= i; j++)) 0'dan geçerli satır numarasına
// (i) kadar yinelenir ve her satırdaki sütunları temsil eder.

// En içteki döngünün içinde üç faktöriyel hesaplanır:

//      nf (pay faktöriyeli), i'nin faktöriyelini hesaplar.  kf
//      (k-faktöriyel), j'nin faktöriyelini hesaplar.  nkf (nk
//      faktöriyel), (i - j)'nin faktöriyelini hesaplar.

// nf / (kf * nkf) ifadesi, üçgendeki her konum için binom katsayısını (n
// seç k) hesaplar.

// Sonuç, printf kullanılarak boşlukla ayrılmış bir değer olarak
// yazdırılır.

// Bu programın çıktısı, her sayının bir binom katsayısını temsil ettiği
// üçgensel bir sayı modeli olacaktır.  Pascal Üçgeninin ilk birkaç
// satırı şöyle görünecek:

// 1 1 1 1 2 1 1 3 3 1 1 4 6 4 1 ...


// Üçgendeki her sayı, i öğeden j öğenin seçilmesinin kombinasyonuna
// karşılık gelir.


// stack              heap
// main

// i int
//    0
// j int
//    0
// nf int
//    1
// kf int
//    1
// nkf int
//    1
// p  int
//    1

// bir bastırır
