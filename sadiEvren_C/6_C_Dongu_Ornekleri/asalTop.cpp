#include <stdio.h>

int main(){
  int sum = 0;
  for(int d = 1;d<=100;d++){
    int p=0; //flag
    for(int i = 2;i<=d-1;i++){
      if(d%i==0){
        p=1;
      }
    }
    if(p==0){
      printf("%d\n",d);
      sum = sum +d;
    }
  }
  printf("sum : %d",sum);
}

// flag konulmuş flag döngüye giriyor
// tam bölen bir durum, asal olmasını engelleyen bir
// durum bulunduğu anda flag (bayrak) kalkıyor yani
// flag değeri değişiyor.
// flag girdiği durumdan farklı çıktıysa
// Bu asal değildir bununlu ilgil bir şey yapma
// ama girdiği gibi yani sıfır girip sıfır çıkıyorsa
// o zaman bu sayı asaldır deyip bunu toplamlara
// dahil edip ekrana basıyoruz
// en sonda da toplamı basıp çıkıyoruz.
// 1 den 100 e kadar olan asal sayıların toplamı

// 100’E KADAR ASAL SAYILAR:

// 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37,
// 41, 43, 47, 53, 59, 61, 67, 71, 73, 79,83, 89 ve 97
