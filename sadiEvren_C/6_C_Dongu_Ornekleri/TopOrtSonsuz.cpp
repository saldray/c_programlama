// #include <stdio.h>

// int main(){
//   while(){// içinde hiçbir koşul yok dolayısıyla doğru olacak
//     printf("sonsuz döngü");
//   }
// }

// Compiler(Derleyici) hata verdi while() döngüsünün içine
// bir şey istiyor 1, True ; 0, False dönüyordu
// True için 0 hariç herhangi bir sayı verile bilir

// #include <stdio.h>

// int main(){
//   while(1){
//     printf("Sonsuz döngü.");
//   }
// }

// C'de 0 False, 0 dışındaki bütün sayılar True'dur
