// iç içe döngüler
// #include <stdio.h>

// int main(){
//   for(int i = 1;i<=10;i++){
//     for(int j = 1;j<=10;j++){
//       printf("%d\t",i*j);
//     }
//     printf("\n");
// }
// }

// i,j| 1 2 3 4 5 6
// ----------------
// 1  | 2 3 4 5 6
// 2  | 3 4 5 6 7
// 3  | 4 5 6 7 8
// 4  | 5 6 7 8 9
// 5  | 6 7 8 9 10
// 6  |



// #include <stdio.h>

// int main(){
//   for(int i = 1;i<=5;i++){
//     for(int j = 1;j<=5;j++){
//       printf("%d\t",i+j);
//     }
//     printf("\n");
//   }
// }


// #include <stdio.h>

// int main(){
//   for(int i = 1;i<=5;i++){
//     for(int j = 1;j<=5;j++){
//       printf("%d,%d\t",i,j);
//     }
//     printf("\n");
//   }
// }


// * * * * *
//  * * * * *

// Bu şekli oluşturmak için daha önce toplamlarının
// bulduğumuz tabloda çift sayıların yerine * tek sayıların
// yerine boşluk koyacağız. Bunu C diline tercume edeceğiz.

// #include <stdio.h>

// int main(){
//   for(int i = 1;i<=10;i++){
//     for(int j = 1;j<=10;j++){
//       if ((i+j)%2 == 0)
//         printf("*");
//       else
//         printf(" ");
//     }
//     printf("\n");
//   }
// }


// 1,1	1,2	1,3	1,4	1,5
// 2,1	2,2	2,3	2,4	2,5
// 3,1	3,2	3,3	3,4	3,5
// 4,1	4,2	4,3	4,4	4,5
// 5,1	5,2	5,3	5,4	5,5

//   12345
// 1 *   *
// 2  * *
// 3   *
// 4  * *
// 5 *   *

// tabloda diagonalde çaprazlarda 1,1 2,2 3,3 4,4 5,5
// öteki çaprazların toplamıda 6  1,5 2,4 3,3 4,2 5,1

// #include <stdio.h>

// int main(){
//   for(int i = 1;i<=5;i++){
//     for(int j = 1;j<=5;j++){
//       if ((i+j)==6 || i==j)
//         printf("*");
//       else
//         printf(" ");
//     }
//     printf("\n");
//   }
// }



// #include <stdio.h>

// int main(){
//   for(int i = 0;i<=10;i++){
//     for(int j = 0;j<=i;j++){

//       int nf=1;
//       for(int p = 1;p<=i;p++){
//         nf = nf*p;
//       }
//       int kf=1;
//       for(int p = 1;p<=j;p++){
//         kf = kf*p;
//       }
//       int nkf=1;
//       for(int p = 1;p<=i-j;p++){
//         nkf = nkf*p;
//       }
//       printf(" %d ",nf/(kf*nkf));
//     }
//     printf("\n");
//   }
// }

// Binom üçgeni, pascal üçgeni kombinasyondan yapılıyordu
// 1  birin sıfırlı kombinasyonu
// 1 1  ikinin birli ikinin ikili kombinasyonu
// 1 2 1  üçün birli, üçün ikili, üçün üçlü kombinasyonu
// C(n,r), n!/r!(n-r)!  3!/1!(3-1)!

// (0/0)!
// (1 0)! (1 1)!  birin sıfırlı, birin birli
// (2 0)! (2 1)! (2 2)!


#include <stdio.h>

int main(){
  int sum = 0;
  for(int d = 1;d<=100;d++){
    int p=0; //flag
    for(int i = 2;i<=d-1;i++){
      if(d%i==0){
        p=1;
      }
    }
    if(p==0){
      printf("%d\n",d);
      sum = sum +d;
    }
  }
  printf("sum : %d",sum);
}

// flag konulmuş flag döngüye giriyor
// tam bölen bir durum, asal olmasını engelleyen bir
// durum bulunduğu anda flag (bayrak) kalkıyor yani
// flag değeri değişiyor.
// flag girdiği durumdan farklı çıktıysa
// Bu asal değildir bununlu ilgil bir şey yapma
// ama girdiği gibi yani sıfır girip sıfır çıkıyorsa
// o zaman bu sayı asaldır deyip bunu toplamlara
// dahil edip ekrana basıyoruz
// en sonda da toplamı basıp çıkıyoruz.
// 1 den 100 e kadar olan asal sayıların toplamı

// 100’E KADAR ASAL SAYILAR:

// 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37,
// 41, 43, 47, 53, 59, 61, 67, 71, 73, 79,83, 89 ve 97
