// #include <stdio.h>
// void yaz(); // fonksiyon ön bildirimi
// void main(){
// 	yaz();
// }
// 
// void yaz(){
// 	printf("C Programlama Notları\n");
// }
// 
// The error you’re encountering is due to the requirement
// that the main() function in C and C++ 
// must return an int value. Let me explain why:
// 
//     Exit Status:
// When a program finishes executing, 
// it returns an exit status to the operating system.
// This exit status indicates whether the program 
// terminated successfully or encountered an error.
// 
// By convention, a return value of 0 typically 
// signifies successful execution, 
// while non-zero values indicate errors.
// 
// Standard Requirement:
// The C and C++ standards specify that the main() function
// must have a return type of int.
// 
// Using void main() is considered non-standard 
// and incorrect.
// Instead, you should use int main().
// 
// Here’s the corrected version of your code:

// #include <stdio.h>
// 
// int main()
// {
//     printf("C Programlama Notları\n");
//     return 0; // Indicate successful execution
// }
// 
// Aşağıdaki örnek programda carp() fonksiyonuna ana
// fonksiyondan değer aktarılmaktadır.


// #include <stdio.h>
// void carp(int x, int y); // fonksiyon ön bildirimi
// int main(){
// 	carp(10,20);
// 	carp(5,6);
// 	carp(8,9);
// 	return 0;
// }
// 
// void carp(int x, int y){ // fonksiyon tanımlanması
// 	printf("%2d * %2d = %3d\n", x, y, x*y);
// }

// Aşağıdaki programda, klavyeden bir sayı girilmekte,
// kare() fonksiyonu bu sayının karesini alarak görüntülemektedir.

// #include <stdio.h>
// void kare(int x); // fonksiyon bildirimi
// 
// int main(){ // ana fonksiyon
// 	int sayi;
// 	printf("Bir sayı giriniz :");
// 	scanf("%d", &sayi);
// 	kare(sayi);
// }
// 
// void kare(int x){ // fonksiyon
// 	printf("Sayı: %d Karesi: %d\n", x, x*x);
// }

// ***************************************************
// Aşağıdaki programda x değişkeni hem ana
// programda hem de fonksiyon içinde tanımlanmış olmasına 
// rağmen iki ayrı değişken olarak kullanılmaktadır.
// ***************************************************

// #include <stdio.h>
// void fonk();
// 
// int main(){ // ana program
// 	int x;
// 	x=10;
// 	printf("ana programdaki x degeri: %d\n",x);
// 	fonk();
// 	printf("ana programdaki x degeri: %d\n",x);
// }
// 
// void fonk(){ // fonksiyon
// 	int x;
// 	x=100;
// 	printf("fonksiyondaki x degeri: %d\n", x);
// 	return;
// }
// 
// ***********************************************
// *	Değer Döndüren Fonksiyonlar              *
// ***********************************************

// Aşağıdaki programda ana programdan girilen iki sayı 
// fonksiyona gönderilmekte,
// fonksiyon bu iki sayıyı çarparak ürettiği sonucu 
// ana programdaki değişkene aktarmaktadır.

// #include <stdio.h>
// int carp(int x, int y); // fonksiyon ön bildirimi
// int main(){ // ana program
// 	int a, b, c;
// 	printf("Birinci sayiyi giriniz:");
// 	scanf("%d", &a);
// 	printf("İkinci sayiyi giriniz:");
// 	scanf("%d", &b);
// 	c=carp(a, b);
// 	printf("Carpim: %d\n", c);
// 	// veya printf("Carpim: %d\n", carp(a, b))
// }
// 
// int carp(int x, int y){ // fonksiyon
// 	int sonuc;
// 	sonuc= x * y;
// 	return sonuc; // veya return x*y;
// }

// **************************************
// Aşağıdaki programda ise, ana programda dairenin 
// yarıçapı girilmekte, bu değer
// fonksiyona gönderilmekte, fonksiyon da dairenin 
// alanını hesapladıktan sonra bu
// değeri ana programa aktarmaktadır.
// ****************************************

// #include <stdio.h>
// #define PI 3.141593
// 
// float daire(float y); // fonksiyon ön bildirimi
// int main(){ // ana program
// 	float r, s;
// 	printf("Dairenin yaricapini giriniz: ");
// 	scanf("%f", &r);
// 	s = daire(r);
// 	printf("Dairenin alani: %6.2f dir.\n",s);
// }
// // dairenin alanını hesaplayan fonksiyon
// float daire(float y){
// 	float a;
// 	a = PI * y * y;
// 	return(a);
// }
// 

// Fonksiyon ön bildiriminde eb fonksiyonunun
// üç adet integer değer alacağı bildirilmiş olup, 
// değişken isimleri bildirilmemiştir. Bu
// tür tanımlamalar sadece fonksiyon prototiplerinde 
// yapılabilir

// sayfa 100 deyim
