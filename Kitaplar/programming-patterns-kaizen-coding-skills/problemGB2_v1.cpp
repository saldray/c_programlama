#include <stdio.h>

int main(){
    int rows = 3;
    int cols = 3;
    int count = 1;

    for(int i=1; i<= rows*cols;i++){
      printf("Row = %d| Col = %d\n", i, count);
      if (i % cols == 0){
        printf("-------------------------------\n");
        count = 0;
      }
      count += 1;
    }
    return 0;
}
