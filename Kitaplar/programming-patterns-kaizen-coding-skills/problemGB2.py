rows = 9
cols = 3

for i in range(1, rows + 1):
    row = (i - 1) % 3 + 1
    col = (i - 1) // 3 + 1
    print(f"Row = {i}| Col = {row}")
    if i % cols == 0:
        print("--------------------------------")


rows = 3
cols = 3
count = 1

for i in range(1, rows * cols + 1):
    print(f"Row = {i} | Col = {count}")
    if i % cols == 0:
        print("--------------------------------")
        count = 0
    count += 1
