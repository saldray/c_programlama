// // public class ForLoopEveryRowAllColumnsHorizontally {
// #include <stdio.h>
// //  public static void main(String[] args) {
// int main(){
//     int TOTAL_ROWS = 3;
//     int TOTAL_COLUMNS = 3;

//     for(int row = 1 ; row <= TOTAL_ROWS ; row++ ){
//       printf( "Row = " + row + " |");
//       for( int col = 1 ; col <= TOTAL_COLUMNS ; col++) {
//         printf( " Col = " + col);
//       }
//       printf("\n");
//     }
//   }


#include <stdio.h>

int main() {
    int TOTAL_ROWS = 3;
    int TOTAL_COLUMNS = 3;

    for (int row = 1; row <= TOTAL_ROWS; row++) {
        printf("Row = %d |", row);
        for (int col = 1; col <= TOTAL_COLUMNS; col++) {
            printf(" Col = %d", col);
        }
        printf("\n");
    }

    return 0;
}
