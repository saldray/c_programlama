#include <stdio.h>

int main(){
  //1st way to print to console
  printf("Word:\n");

  //2nd way to print to console
  printf("Word:");printf("\n");

  //3rd way to print to console
  printf("\n");printf("Word: ");

  //4th way to print to console
  printf("\nWord: ");

  return 0;
}
