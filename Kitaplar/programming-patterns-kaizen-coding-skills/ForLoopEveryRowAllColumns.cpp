#include <stdio.h>

int main(){

  int TOTAL_ROWS = 3;
  int TOTAL_COLUMNS = 3;

  for(int row = 1; row <= TOTAL_ROWS ; row ++){
    for(int col = 1 ; col <= TOTAL_COLUMNS ; col ++){
      printf("Row = %d | col = %d\n", row, col);
    }
    printf("-----------------------------\n");
  }
  return 0;
}
