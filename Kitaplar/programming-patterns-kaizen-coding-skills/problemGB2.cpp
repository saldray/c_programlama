// #include <stdio.h>

// int main() {
//     int TOTAL_ROWS = 9;
//     int TOTAL_COLUMNS = 3;

//     for (int row = 1; row <= TOTAL_ROWS; row++) {
//         // Determine the current row and column
//         int currentRow = (row - 1) / TOTAL_COLUMNS + 1;
//         int currentCol = (row - 1) % TOTAL_COLUMNS + 1;

//         // Print the formatted output
//         printf("Row = %d | Col = %d\n", currentRow, currentCol);

//         // Add a separator after every 3 rows
//         if (row % TOTAL_COLUMNS == 0) {
//             printf("--------------------------------\n");
//         }
//     }

//     return 0;
// }


// #include <stdio.h>

// int main() {
//     int TOTAL_ROWS = 9;
//     int TOTAL_COLUMNS = 3;

//     for (int row = 1; row <= TOTAL_ROWS; row++) {
//         // Determine the current row and column
//         int currentRow = (row - 1) / TOTAL_COLUMNS + 1;
//         int currentCol = (row - 1) % TOTAL_COLUMNS + 1;

//         // Print the formatted output
//         printf("Row = %d | Col = %d\n", currentRow, currentCol);

//         // Add a separator after every 3 rows
//         if (row % TOTAL_COLUMNS == 0) {
//             printf("--------------------------------\n");
//         }
//     }

//     return 0;
// }



// # Define the number of rows and columns
// rows = 3
// cols = 3

// # Loop through the rows
// for row in range(1, 10):
//     # Print the row number
//     print(f"Row = {row}| Col =", end="")

//     # Loop through the columns
//     for col in range(1, cols + 1):
//         # Print the column number
//         print(col, end="")

//         # Check if this is the last column in the current row
//         if col < cols:
//             print("|", end="")

//     # Print a separator line after every 3 rows
//     if (row - 1) % 3 == 0:
//         print("\n--------------------------------")



// #include <stdio.h>

// int main() {
//     int rows = 3;
//     int cols = 3;

//     // Loop through the rows
//     for (int row = 1; row <= 9; row++) {
//         // Print the row number
//         printf("Row = %d| Col =", row);

//         // Loop through the columns
//         for (int col = 1; col <= cols; col++) {
//             // Print the column number
//             printf("%d", col);

//             // Check if this is the last column in the current row
//             if (col < cols) {
//                 printf("|", end);
//             }
//         }

//         // Print a separator line after every 3 rows
//         if ((row - 1) % 3 == 0) {
//             printf("\n--------------------------------");
//         } else {
//             printf("\n");
//         }
//     }

//     return 0;
// }



// #include <stdio.h>

// int main() {
//     int rows = 3;
//     int cols = 3;

//     // Loop through the rows
//     for (int row = 1; row <= 9; row++) {
//         // Print the row number
//         printf("Row = %d| Col =", row);

//         // Loop through the columns
//         for (int col = 1; col <= cols; col++) {
//             // Print the column number
//             printf("%d", col);

//             // Check if this is the last column in the current row
//             if (col < cols) {
//                 printf("|"); // Corrected to directly print '|'
//             }
//         }

//         // Print a separator line after every 3 rows
//         if ((row - 1) % 3 == 0) {
//             printf("\n--------------------------------");
//         } else {
//             printf("\n");
//         }
//     }

//     return 0;
// }


// #include <stdio.h>

// int main() {
//     int rows = 3;
//     int cols = 3;

//     // Loop through the rows
//     for (int row = 1; row <= 9; row++) {
//         // Print the row number
//         printf("Row = %d| Col =", row);

//         // Loop through the columns up to the current row's limit
//         for (int col = 1; col <= row; col++) {
//             // Print the column number
//             printf("%d", col);

//             // Check if this is the last column in the current row
//             if (col < row) {
//                 printf("|"); // Corrected to print '|' only between columns
//             }
//         }

//         // Print a separator line after every 3 rows
//         if ((row - 1) % 3 == 0) {
//             printf("\n--------------------------------");
//         } else {
//             printf("\n");
//         }
//     }

//     return 0;
// }




#include <stdio.h>

int main() {
    int rows = 9;
    int cols = 3;
    int count = 1;

    for (int i = 1; i <= rows; i++) {
        printf("Row = %d| Col = %d\n", i, count);
        count++;
        if (count > cols) {
            count = 1;
            printf("--------------------------------\n");
        }
    }

    return 0;
}
