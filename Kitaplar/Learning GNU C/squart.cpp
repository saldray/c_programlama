// file:///home/Saldray/Downloads/ind/learning_gnu_c.pdf
// Learning GNU C
// A C programming tutorial for users of the GNU operating system

// #include <stdio.h>
// int get_next_square(void);
// int
// main()
// {
//     int i;
//     for (i = 0; i < 10; i++)
//         printf ("%6d\n", get_next_square ());
//     printf ("and %6d\n", get_next_square ());
//     return 0;
// }
// int
// get_next_square ()
// {
//     static int count = 1;
//     count += 1;
//     return count * count;
// }

// for'da küme parantezi kullanmazsak for dan sonraki
// ilk satırı for döngüsünde sayar sonrası onun dışında



#include <stdio.h>
int get_next_square(void);
int
main()
{
    int i;
    for (i = 0; i < 10; i++){
        printf ("fonk çağrılmadan i = %d\n", i);
        printf ("%6d\n", get_next_square ());
        printf ("döngüdeki fonk call burada girmiyor 'i' = %d\n", i);
    }
    printf ("and %6d\n", get_next_square ());
    return 0;
}
int
get_next_square ()
{
    static int count = 1;
    printf("ilk count= %d\n", count);
    count += 1;
    printf("count = %d", count);
    return count * count;
}
