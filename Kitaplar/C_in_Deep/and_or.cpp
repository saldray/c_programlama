#include <stdio.h>

int main(void) {
    //  printf("Hello World\n");
    int i = 3, j = 2, k = 0, m;
    m = ++i || ++j && ++k;
    printf("%d %d %d %d", i, j, k, m);
    return 0;
}


// && operatörünün || or operatörüne önceliği vardır.
// ++i, 3 bir arttırılır = 4 olur.
// ++j && ++k işleminin
// i'yi 1 arttırıyor 4 oluyor. || bir tanesinin true dönmesi sonucu
// true çıkarır. Kısa devre yapar Sonrasını da kabul eder.
//

// #include <stdio.h>
//
// int main(void) {
//     //  printf("Hello World\n");
//     int i = 0, j = 2, k = 0, m;
//     m = i || ++j && ++k;
//     printf("%d %d %d %d", i, j, k, m);
//     return 0;
// }

// output: 0 3 1 1



// #include <stdio.h>
//
// int main(void) {
//     int i = 3, j = 2, k = 0, m;
//     m = ++i && ++j && ++k;
//     printf("%d %d %d %d", i, j, k, m);
//     return 0;
// }
//
// output: 4 3 1 1























