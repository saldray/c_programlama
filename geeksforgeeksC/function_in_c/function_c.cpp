// #include <stdio.h>

// int sum(int a,int b){
//   return a + b;
// }
// int main(){
//   int add=sum(10,30);
//   printf("Sum is:%d",add);
//   return 0;
// }

// #include <stdio.h>

// int sum(int a,int b){
//   return(a+b);
// }
// int main(){
//   int add=sum(10,30);
//   printf("Sum is:%d",add);
//   return 0;
// }



// // C program to show function
// // call and definition
// #include <stdio.h>

// // Function that takes two parameters
// // a and b as inputs and returns
// // their sum
// int sum(int a, int b){
//   return a + b;
// }

// // Driver code

// int main(){
//   int add = sum(10, 30);
// //  printf("Sum is: %d \n", add);
//   printf("Sum is: %d", add);
//   return 0;
// }


// // C program to implement
// // the above approach
// #include <math.h>
// #include <stdio.h>

// // Driver code
// int main(){
//   double Number;
//   Number = 49;
//   // Computing the square root with
//   // the help of predefined C
//   // library function
//   double squareRoot = sqrt(Number);

//   printf("The Square root of %.2lf = %.2lf",
//          Number, squareRoot);
//   return 0;
// }


// User Defined Function

// // C program to show
// // user-defined functions
// #include <stdio.h>

// int sum(int a, int b){
//   return a + b;
// }

// // Driver code
// int main(){
//   int a = 30, b = 40;

//   // function call
//   int res = sum(a, b);

//   printf("Sum is: %d", res);
//   return 0;
// }



// // C program to show use
// // of call by value
// #include <stdio.h>

// void swap(int var1, int var2)
// {
// int temp = var1;
// var1 = var2;
// var2 = temp;
// }

// // Driver code
// int main()
// {
// int var1 = 3, var2 = 2;
// printf("Before swap Value of var1 and var2 is: %d, %d\n",
//                 var1, var2);
// swap(var1, var2);
// printf("After swap Value of var1 and var2 is: %d, %d",
//                 var1, var2);
// return 0;
// }

// yukarıda
// Bu yöntemde parametre geçişi, gerçek parametrelerden değerleri resmi
// işlev parametrelerine kopyalar. Sonuç olarak, işlevlerin içinde
// yapılan herhangi bir değişiklik çağıranın parametrelerinde yansımaz.

// Parameter passing in this method copies values from actual parameters
// into formal function parameters. As a result, any changes made
// inside the functions do not reflect in the caller’s parameters.

// int sum(int a,int b)
// int a,int b   Formal Parameter

// int add = sum(10,30);
// 10,30         Actual Parameter




// // C program to show use of
// // call by Reference
// #include <stdio.h>

// void swap(int *var1, int *var2)
// {
// int temp = *var1;
// *var1 = *var2;
// *var2 = temp;
// }

// // Driver code
// int main()
// {
// int var1 = 3, var2 = 2;
// printf("Before swap Value of var1 and var2 is: %d, %d\n",
//                 var1, var2);
// swap(&var1, &var2);
// printf("After swap Value of var1 and var2 is: %d, %d",
//                 var1, var2);
// return 0;
// }

// https://www.geeksforgeeks.org/user-defined-function-in-c/?ref=next_article
