 // #include <stdio.h>
 // #include <stdlib.h>

 // int main(){
 // 	printf("Hello World!\n");
 // 	return EXIT_SUCCESS;
 // }

// ***************************************************
// +****Çalıstı Türkçe karakter vermemeye çalış ******
// ***************************************************
// #include <stdio.h>

// int main() {
//     const char* word = "istihza"; // Use const to avoid the warning
//     int sira = 1;
//     char harf;

//     for (int i = 0; word[i] != '\0'; ++i) {
//         harf = word[i];
//         printf("%d %c\n", sira, harf);
//         ++sira;
//     }

//     return 0;
// }




//     #include <stdio.h>: This line includes the standard
//     #input/output library, allowing us to use functions like
//     #printf.

//     int main(): The main function is the entry point of the
//     program. It returns an integer value (usually 0) to indicate
//     the program’s exit status.

//     const char* word = "istihza";: We declare a pointer to a
//     constant character (const char*) named word. It points to the
//     string literal "istihza".

//     int sıra = 1;: We initialize an integer variable called sıra
//     with the value 1. This variable will keep track of the position
//     (or order) of each character in the string.

//     char harf;: We declare a character variable named harf. This
//     variable will store each character from the string.

//     for (int i = 0; word[i] != '\0'; ++i): This for loop iterates
//     through the characters of the string word. The condition
//     word[i] != '\0' checks if we’ve reached the end of the string
//     (where '\0' represents the null terminator).

//     harf = word[i];: Inside the loop, we assign the current
//     character from word to the variable harf.

//     printf("%d %c\n", sıra, harf);: We use printf to print the
//     position (sıra) and the character (harf). The %d and %c format
//     specifiers indicate that we’ll print an integer and a
//     character, respectively.

//     ++sıra;: We increment the position (sıra) for the next
//     iteration.

//     Finally, the program returns 0 to indicate successful
//     execution.

// The output of this program will be:


// ++i (Pre-increment):

// int i = 5;
// int result = ++i; // i is incremented to 6, and result is assigned 6

// i++ (Post-increment):

// int i = 5;
// int result = i++; // result is assigned 5, and i is incremented to 6


// https://www.tutorialspoint.com/what-is-the-difference-between-plusplusi-and-iplusplus-in-c

// #include <stdio.h>
// int main() {
//    int i=5,j;
//    j=i++;
//    printf ("after postfix increment i=%d j=%\n", i,j);
//    i=5;
//    j=++i;
//    printf (" after prefix increment i=%d j=%d",i,j);
//    return 0;
// }


// after postfix increment i=6 j=5
// after prefix increment i=6 j=6

// https://stackoverflow.com/questions/24853/what-is-the-difference-between-i-and-i

// ++i will increment the value of i, and then return
// the incremented value.

// ++i, i'nin değerini artıracak ve ardından artan değeri döndürecektir.

 // i = 1;
 // j = ++i;
 // (i is 2, j is 2)

// i++ will increment the value of i, but return the original
// value that i held before being incremented.

// i++, i'nin değerini artıracaktır, ancak artırılmadan önce tuttuğum orijinal değeri döndürecektir.

 // i = 1;
 // j = i++;
 // (i is 2, j is 1)


// #include <stdio.h>

// int main(){
//   for(int i=0; i<5; i++){
//       printf("%d\n", i);
//   }

// }

// #include <stdio.h>

// int main(){
//   for(int i=0; i<5; ++i){
//     printf("%d\n", i);
//   }
// }

// ikiside aynı çıktıyı üretir önemli olan nerede kullanılacağıdır

// i++ ----> Assign ----> increment
// ++i ----> increment ---> Assign

// #include <stdio.h>

// int main(){
//   for(int i=0; i<5;){
//     printf("%d\n", ++i);
//   }
// }


// #include <stdio.h>

// int main(){
//   for(int i=0; i<5;){
//     printf("%d\n", i++);
//   }
// }



// #include <stdio.h>

// int main(){
//   for(int i=0; ++i<10;){
//     printf("ideger %d\n",i);
//   }
// }

// 0 geliyor 1 arttırılıyor. 1<10 sağlıyor
// printf ile 1 basılıyor

// #include <stdio.h>

// int main(){
//   for(int i=0; i++<10;){
//     printf("ideger %d\n",i);
//       }
// }

// 0 alıyor 0<10, koşuldan sonra arttırıyor
// i++, 1 oluyor ve bastırır.

// #include <stdio.h>

// int main(){
//   printf("Hello world!\n");

//   return 0;
// }


// #include <stdio.h>

// int main(){
//   int a = 10;
//   printf("%d", a);

//   return 0;
// }


// #include <stdio.h>

// int main(){
//   int i = 1;
//   printf("%d %d %d\n", i++, i++, i);
//   return 0;
// }


// Programming to print cube of given number

// #include <stdio.h>

// int main(){
//   int number;
//   printf("enter a number:");
//   scanf("%d",&number);
//   printf("cube of number is:%d",number*number*number);
//   return 0;
// }


// #include <stdio.h>

// int main(){
//   for(int i = 1;i<=10; i++){
//     if(i == 2){
//       continue;
//     }
//     if(i == 6){
//       break;
//     }
//     printf("%d ", i);
//   }
// }


// C program to demonstrate
// switch case statement
// #include <stdio.h>

// // Driver code
// int main() {
//   int i = 4;
//   switch (i) {
//     case 1:
//       printf("Case 1\n");break;
//     case 2:
//       printf("Case 2\n");break;
//     case 3:
//       printf("Case 3\n");break;
//     case 4:
//       printf("Case 4\n");break;
//     default:
//       printf("Default\n");break;
//   }
// }


//#include <stdio.h>
//
//int main(){
//	for (int i = 6; i>0;i--){
//		for(int j = 1;j<=i;j++){
//			printf("%d",j);
//		}
//		printf("\n");	
//	}
//}

// #include <stdio.h>
// 
// int main(){
// 	puts("This is a test.");
// 	return 0;
// }


#include <stdio.h>

int main(){
	}
