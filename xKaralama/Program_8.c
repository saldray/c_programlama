// #include <stdio.h>
//
// int main(){
//
//     int a = 10;
//     int b = 20;
//
//     swapr(&a, &b);
//     printf("\na = %d",a);
//     printf("\nb = %d",b);
// }
//
// swapr(int *x, int *y){
//     int t;
//
//     t = *x;
//     *x = *y;
//     *y = t;
// }


#include <stdio.h>

void swap(int *x, int *y) {
    int temp;

    temp = *x;
    *x = *y;
    *y = temp;
}

int main() {
    int a = 10;
    int b = 20;

    swap(&a, &b); // Corrected function call
    printf("\na = %d", a);
    printf("\nb = %d", b);

    return 0; // Added return statement for proper program termination
}
