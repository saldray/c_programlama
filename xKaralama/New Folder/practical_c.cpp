// // Figure 9-1 Local and global variables
// #include <stdio.h>
// 
// int global; // a global variable
// int main(){
// 	int local;	// a local variable
// 
// 	global = 1; // global can be used here
// 	local = 2; // so can local
// 	{
// 		int very_local;	// beginning a new block
// 						// this is local to the block
// 		very_local = global+local;
// 		printf("very_local: %d\n", very_local);
// 
// 	}
// 	    // We just closed the block
// 		// very_local can not be used
// 		printf("global: %d\nlocal: %d\n", global,local);
// }


// #include <stdio.h>
// 
// int total;
// int count;
// 
// int main() {
// 	total = 0;
// 	count = 0;
// 
// 	{
// 		int count;
// 
// 		count = 0;
// 
// 		while (1) {
// 			if (count > 10)
// 				break;
// 
// 			total += count;
// 			printf("count: %d\ntotal: %d\n", count, total);
// 			++count;
// 			printf("local count %d\n", count);
// 		}
// 	}
// 	++count;
// 	printf("global count: %d", count);
// 	return (0);
// 	
// }

// Buradaki sorun değişkenlere verilen adlar
//  You should give these variables different names, 
//  like total_count, current_count, and item_count.  


// Example 9-4: len-len.c (continued)

// #include <stdio.h>
//
// int length(char string[])

// beginning c from beginner to pro

// #include <stdio.h>
//
// int main(){
//     for(int count = 1; count <= 10; ++count){
//         printf(" %d\n", count);
//     }
// }


// #include <stdio.h>
//
// int main(){
//     for(int count =1 ; count <=10; ++ count){
//         printf(" %d", count);
//     }
// }







