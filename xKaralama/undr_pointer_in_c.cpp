/*Program 8*/
// 
// #include <stdio.h>
// 
// void swarp(int *x, int *y);
// 
// int main(){
// 	
// 	int a = 10;
// 	int b = 20;
// 
// 	swapr(&a, &b);
// 	printf("\na = %d", a);
// 	printf("\nb = %d", b);
// }
// 
// swapr(int *x, int *y){
// 
// 	int t;
// 
// 	t = *x;
// 	*x = *y;
// 	*y = t;
// }

// phind.com
//

// #include <stdio.h>
// 
// void swapr(int *x, int *y){
// 	int temp;
// 
// 	temp = *x;
// 	*x = *y;
// 	*y = temp;
// }
// 
// int main(){
// 	int a = 10;
// 	int b = 20;
// 
// 	swapr(&a, &b);
// 	printf("\na = %d", a);
// 	printf("\nb = %d", b);
// 
// 	return 0;
// }


/*Program7*/

// #include <stdio.h>
//
// int main(){
// 	int a = 10;
// 	int b = 20;
//
// 	swapv(a, b);
// 	printf("\na = %d", a);
// 	printf("\nb = %d", b);
// }
//
// swapv(int x, int y){
//
// 	int t;
// 	t = x;
// 	x = y;
// 	y = t;
//
// 	printf("\nx = %d", x);
// 	printf("\ny = %d", y);
// }
//
//
// error: ‘swapv’ was not declared in this scope
// 59 |         swapv(a, b);
// |         ^~~~~
// undr_pointer_in_c.cpp: At global scope:
// undr_pointer_in_c.cpp:64:1: error: ISO C++ forbids declaration of ‘swapv’ with no type [-fpermissive]
// 64 | swapv(int x, int y){
// 	| ^~~~~
// 	undr_pointer_in_c.cpp: In function ‘int swapv(int, int)’:
// 	undr_pointer_in_c.cpp:73:1: warning: no return statement in function returning non-void [-Wreturn-type]
//





	#include <stdio.h>

	// Declare the swapv function here before using it in main
int swapv(int x, int y);

int main() {
	int a = 10;
	int b = 20;

		// Call the swapv function
	swapv(a, b);
	printf("\na = %d", a); // Note: After swapping, a and b values will change
	printf("\nb = %d", b); // Note: After swapping, a and b values will change

	return 0; // Ensure main returns an integer
	}

	// Define the swapv function with a return type
	int swapv(int x, int y) {
	int t;
	t = x;
	x = y;
	y = t;

		// Print the swapped values
	printf("\nx = %d", x);
	printf("\ny = %d", y);

	return 0;

		// Return the sum of x and y (or any other value if needed)
		//return x + y;
}
