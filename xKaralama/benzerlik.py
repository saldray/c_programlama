# a = 1
# b = 2

# print(f"a = {a}, b = {b}")
# a = a+b
# b = a-b
# a = a-b
# print(f"a = {a}, b = {b}")


# variable1 = 1
# variable2 = 2

# print(f'variable1 = {variable1}, variable2 = {variable2}')
# temporary = variable1
# variable1 = variable2
# variable2 = temporary

# print(f'variable1 = {variable1}, variable2 = {variable2}')


# variable1 = 1
# variable2 = 2

# print(f'variable1 = {variable1}, variable2 = {variable2}')
# variable1, variable2 = variable2, variable1
# print(f'variable1 = {variable1}, variable2 = {variable2}')



# variable1 = 1
# variable2 = 2

# print(f'variable1 = {variable1}, variable2 = {variable2}')
# if type(variable1) is type(variable2):
#     temporary = variable1
#     variable1 = variable2
#     variable2 = temporary
#     print(f'variable1 = {variable1}, variable2 = {variable2}')
# else:
#     print("Swapping is not applied")

# Else'e düşüp düşmediğini görmek için

variable1 = 1 # int
variable2 = '2' # str

print(f'variable1 = {variable1}, variable2 = {variable2}')
if type(variable1) is type(variable2):
    temporary = variable1
    variable1 = variable2
    variable2 = temporary
    print(f'variable1 = {variable1}, variable2 = {variable2}')
else:
    print("Swapping is not applied")
    print(type(variable1))
    print(type(variable2))
