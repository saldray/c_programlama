// #include <stdio.h>
// int main()
// {
// char str[255], *ptr1, *ptr2, temp ;
// int n,m;
// printf("Enter a string: ");
// scanf("%s", str);
// ptr1=str;
// n=1;
// while(*ptr1 !='\0')
// {
// ptr1++;
// n++;
// }
// ptr1--;
// ptr2=str;
// m=1;
// while(m<=n/2)
// {
// temp=*ptr1;
// *ptr1=*ptr2;
// *ptr2=temp;
// ptr1--;
// ptr2++;;
// m++;
// }
// printf("Reverse string is %s", str);
// }


#include <stdio.h>
#include <string.h>

void reverseString(const char *original,char *reversed,int index){// original array değişmesini istemiyorum o nedenle const
    if(index<0){
        *reversed='\0';
        return; //void olduğu için böyle
    }
    *reversed=*(original+index);
    reverseString(original,reversed+1,index-1);
}

int main(){
    char str[100];
    char reversed[100];
    char finalResult[200];

    printf("Bir string giriniz:");
    scanf("%s",str); // bir adres belirteci kullanmama gerek yok
    // Çünkü bir string array ile uğraşıyoruz.
    int len=strlen(str);

    reverseString(str, reversed, len-1);

    printf("Original string: %s\n",str);
    printf("Ters çevrilmiş string: %s\n",reversed);

    strcat(finalResult,reversed);
    strcat(finalResult,"- Bu ters cevrilmis bir stringdir");
    printf("\nsonuç stringi: %s\n",finalResult);

    return 0;
}

