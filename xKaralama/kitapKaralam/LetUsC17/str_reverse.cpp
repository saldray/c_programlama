#include <stdio.h>
#include <string.h>

int main() {
    char l[100]; // Assuming the input string length is less than 100
    fgets(l, sizeof(l), stdin); // Read input string

    int i = strlen(l) - 1; // Get the length of the string and subtract 1 for 0-based index

    while (i >= 0) { // Loop until i is less than 0
        printf("%c", l[i]); // Print the character at index i
        i--; // Decrement i
    }

    printf("\n"); // Print a new line at the end

    return 0;
}
