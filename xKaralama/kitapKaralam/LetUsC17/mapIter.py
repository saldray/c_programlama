arr = map(int, input().split())  # Read space-separated integers from input and convert them to integers

for a in arr:  # Iterate through each integer 'a' in the 'arr'
    print('#' * a)  # Print the '#' character repeated 'a' number of times.

# How the code works:
#
#     Input and Conversion:
#         input(): This function reads a line of text from the user's input (e.g., "3 5 1").
#         split(): This method splits the input string into a list of strings, using spaces as the default delimiter (e.g., ["3", "5", "1"]).
#         map(int, ...): The map() function applies the int() function to each element of the list. int() converts each string element to an integer. The result of map() is a map object, which is an iterator of integers. In Python 3, you typically convert this map object to a list, tuple, or iterate over it directly. In this code, the for loop directly iterates over the map object.
#
#     Iteration:
#         for a in arr:: This loop iterates through each integer in the arr (the iterator of integers created from the map object). In each iteration, the current integer is assigned to the variable a.
#
#     Printing the Output:
#         print('#' * a): This line is the heart of the output. The * operator, when used with a string and an integer, performs string repetition. So '#' * 3 evaluates to "###", '#' * 5 evaluates to "#####", and so on. The print() function then displays this repeated string to the console.
#
# Example:
#
# If the user enters the following input:
#
# 3 5 1
#
# The code will produce the following output:
#
# ###
# #####
# #
#
# Explanation of the Example:
#
#     Input: The input() function reads "3 5 1".
#     Splitting: split() turns it into ["3", "5", "1"].
#     Mapping: map(int, ...) converts the list to an iterator of integers: (3, 5, 1).
#     Looping:
#         First iteration: a is 3, so print('#' * 3) prints "###".
#         Second iteration: a is 5, so print('#' * 5) prints "#####".
#         Third iteration: a is 1, so print('#' * 1) prints "#".


