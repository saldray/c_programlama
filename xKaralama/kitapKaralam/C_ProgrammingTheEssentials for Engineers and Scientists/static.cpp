// Static Variable

// #include <stdio.h>
// 
// void display();
// int main(){
//     display();
//     display();
// }
// void display(){
//     static int c = 0;
//     printf("%d\t ",c);
//     c += 5;
// }


// Normal Vasiable

// #include <stdio.h>
// 
// void display();
// int main(){
//     display();
//     display();
// }
// void display(){
//     int c = 0;
//     printf("%d\t \n",c);
//     c += 5;
// }


// oct.c

// #include <stdio.h>
// 
// int main(void){
//     int i;
//     
//     printf("Give integer: ");
//     scanf("%i", &i);
//     printf("%i %d\n", i, i);
//     printf("Give integer: ");
//     scanf("%d", &i);
//     printf("%i %d\n", i, i);
// 
//     return 0;
// }

// 11 değerini girdiğimde tam sayı olarak okur
// decimal 10 luk sistemde
// 011 girdiğinme octal yani sekizlik 8 lik sistem olarak kabul eder
// 011 (octal) = 9 (decimal)
// 11 (decimal) = 11 (decimal)




// C Programming: The Essentials for Engineers and Scientists
// program çalıştı

// circle.dat dosyasını programı çalıştırmadan önce oluştur
// içine bir değer yaz misal 3 gibi

/* Calculate the area and circumference of a circle
of specified radius, using an external data file. */
#include <stdio.h>
#define PI 3.14159
int main(void)
{
double radius, area, circumference;

FILE *inp, *outp;
/* input - radius of a circle */
/* output - area of a circle */
/* output - circumference of a circle */
/* pointers to input and output files */
/* Open the input and output files. */
inp = fopen("circle.dat","r");
outp = fopen("circle.out", "w");
/* Read the radius. */
fscanf(inp, "%lf",&radius);
fprintf(outp, "The radius is %.2f\n",radius);
printf("The radius is %.2f\n",radius);
fclose(inp); /* Close the input file. */
/* Calculate the area and circumference. */
area = PI*radius*radius;
circumference = 2*PI*radius;
/* Store the output. */
fprintf(outp, "The area is %.2f\n",area);
fprintf(outp, "The circumference is %.2f\n" ,circumference);

printf("The area is %.2f\n",area);
printf("The circumference is %.2f\n" ,circumference) ;
fclose(outp); /* Close the output file. */
return 0;
}


// /* Calculate the area and circumference of a circle
// of specified radius, using an external data file. */
// #include <stdio.h>
// #define PI 3.14159
// int main(void)
// {
// double radius, area, circumference;
//
// FILE *inp, *outp;
// /* input - radius of a circle */
// /* output - area of a circle */
// /* output - circumference of a circle */
// /* pointers to input and output files */
// /* Open the input and output files. */
// inp = fopen("circle.dat","r");
// outp = fopen("circle.out", "w");
// /* Read the radius. */
// fscanf(inp, "%lf",&radius);
// fprintf(outp, "The radius is %.2f\n",radius);
// printf("The radius is %.2f\n",radius);
// fclose(inp); /* Close the input file. */
// /* Calculate the area and circumference. */
// area = PI*radius*radius;
// circumference = 2*PI*radius;
// /* Store the output. */
// fprintf(outp, "The area is %.2f\n",area);
// fprintf(outp, "The circumference is %.2f\n" ,circumference);
//
// printf("The area is %.2f\n",area);
// printf("The circumference is %.2f\n" ,circumference) ;
// fclose(outp); /* Close the output file. */
// return 0;
// }









