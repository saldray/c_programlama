// C_Programming_Core_Concepts_and_Techniques_William_Smith

// #include <stdio.h>
// 
// int square(int number){
//     return number * number;
// }
// 
// int main(){
//     int num = 4;
//     printf("The square of %d is %d\n", num, square(num));
//     return 0;
// }


// #include <stdio.h>
// 
// int square(int number) {
//     return number * number;
// } 
// 
// int main(){
//     int num = 5;
//     printf("The square of %d is %d\n", num, square(num));
//     return 0;
// }


// with prototype 

// #include <stdio.h>
// 
// // Function prototype
// int square(int number);
// 
// int main(){
//     int num = 6;
//     printf("The square of %d is %d\n", num, square(num));
//     return 0;
// }
// 
// // Function definition
// int square(int number){
//     return number * number;
// }


