// Bu programda circle.dat dosyasının olduğu var sayılıyor.
// programı çalıştırmadan önce circle.dat diye bir dosya oluştur
// ve içine bir değer yaz misal 3 gibi


#include <stdio.h>
#define PI 3.14159

int main(void) {
    double radius, area, circumference;
    FILE *inp, *outp;

    /* Open the input and output files. */
    inp = fopen("circle.dat", "r");
    if (inp == NULL) {
        fprintf(stderr, "Error: Unable to open input file.\n");
        return 1;
    }

    outp = fopen("circle.out", "w");
    if (outp == NULL) {
        fprintf(stderr, "Error: Unable to open output file.\n");
        fclose(inp);
        return 1;
    }

    /* Read the radius. */
    if (fscanf(inp, "%lf", &radius) != 1) {
        fprintf(stderr, "Error: Invalid input format.\n");
        fclose(inp);
        fclose(outp);
        return 1;
    }

    fprintf(outp, "The radius is %.2f\n", radius);
    printf("The radius is %.2f\n", radius);
    fclose(inp); /* Close the input file. */

    /* Calculate the area and circumference. */
    area = PI * radius * radius;
    circumference = 2 * PI * radius;

    /* Store the output. */
    fprintf(outp, "The area is %.2f\n", area);
    fprintf(outp, "The circumference is %.2f\n", circumference);
    printf("The area is %.2f\n", area);
    printf("The circumference is %.2f\n", circumference);

    fclose(outp); /* Close the output file. */

    return 0;
}

