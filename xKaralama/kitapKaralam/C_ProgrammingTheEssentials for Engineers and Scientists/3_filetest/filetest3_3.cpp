#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILE_NAME "structr2.dat"

int main() {
    FILE *Infile;
    char month[10];
    int count = 0;
    int hr, min, sec;
    float x;
    int status;

    Infile = fopen(FILE_NAME, "r");
    if (Infile == NULL) {
        perror("Error opening file");
        return EXIT_FAILURE;
    }

    while ((status = fscanf(Infile, "%9s %d %d %d %f", month, &hr, &min, &sec, &x)) != EOF) {
        if (status == 5) {
            printf("%3s %2d %2d %2d %6.2f\n", month, hr, min, sec, x);
            count++;
        } else {
            fprintf(stderr, "Error reading line %d\n", count + 1);
            break;
        }
    }

    fclose(Infile);
    printf("Lines in file: %d\n", count);
    return EXIT_SUCCESS;
}
