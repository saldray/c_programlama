// # Improved Code for Reading Data from structr2.dat

#include <stdio.h>
#include <stdlib.h>

#define FILE_NAME "structr2.dat"
#define MAX_MONTH_LENGTH 10

int main() {
    FILE *Infile;
    char month[MAX_MONTH_LENGTH];
    int count = 0;
    int hr, min, sec;
    float x;
    int status;

    Infile = fopen(FILE_NAME, "r");
    if (Infile == NULL) {
        perror("Error opening file");
        return EXIT_FAILURE;
    }

    while ((status = fscanf(Infile, "%9s %d %d %d %f", month, &hr, &min, &sec, &x)) == 5) {
        printf("%3s %2d %2d %2d %6.2f\n", month, hr, min, sec, x);
        count++;
    }

    if (status != EOF) {
        fprintf(stderr, "Error reading file\n");
    }

    fclose(Infile);
    printf("Lines in file: %d\n", count);
    return EXIT_SUCCESS;
}
