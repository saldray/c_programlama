#include <stdio.h>
#include <stdlib.h>  // For exit()
#include <string.h>   // For string functions

#define FILE_NAME "structr2.dat"
#define MONTH_LENGTH 10 // Define maximum length of month string

int main() {
    FILE *infile;
    char month[MONTH_LENGTH];
    int count = 0;
    int hr, min, sec;
    float x;
    int status;

    // Open the file in read mode
    infile = fopen(FILE_NAME, "r");

    // Check if the file opened successfully
    if (infile == NULL) {
        perror("Error opening file"); // Use perror for better error messages
        return EXIT_FAILURE; // Indicate failure to the OS
    }

    // Read data from the file until EOF is reached
    while (1) {
        // Use fgets to read a line, then sscanf to parse it securely
        //char line[256]; // Assuming max line length is 255 + null terminator
        //if (fgets(line, sizeof(line), infile) == NULL) {
        //    break; // End of file or error
        //}

        //status = sscanf(line, "%9s %i %i %i %f", month, &hr, &min, &sec, &x);  //added max string read length to minimize risks
        status = fscanf(infile, "%9s %i %i %i %f", month, &hr, &min, &sec, &x);  //added max string read length to minimize risks

        // Check for EOF or parsing errors morerobustly
        if (feof(infile)) {
            break; // End of file reached
        }

        if (status != 5) {
            if(ferror(infile)) {
                perror("Error reading from file");
                fclose(infile);
                return EXIT_FAILURE;
            }
            fprintf(stderr, "Error parsing line in file. Skipping.\n");
            // Optionally, read and discard the rest of the line to avoid infinite loop
            int c;
            while ((c = fgetc(infile)) != '\n' && c != EOF);
            continue; // Skip this line and move to the next
        }
          // Validate data (optional but recommended)
        if (strlen(month) >= MONTH_LENGTH) {
             fprintf(stderr, "Month string too long. Skipping.\n");
             int c;
             while ((c = fgetc(infile)) != '\n' && c != EOF);
             continue;
        }

        // Print the extracted data
        printf("%3s %2i %2i %2i %6.2f\n", month, hr, min, sec, x);

        // Increment the line count
        count++;
    }

    // Close the file
    if (fclose(infile) != 0) {
        perror("Error closing file");
        return EXIT_FAILURE; // Indicate failure
    }

    // Print the total number of lines read
    printf("Lines in file: %d\n", count);

    return 0; // Indicate successful execution
}
