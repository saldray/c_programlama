/* Displays contents of a text file character by character. */
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
FILE *in;
char name [20] ;
int ch;

printf("Give file to fix: ");
scanf("%s",name);
in=fopen(name, "r");
if (in == NULL) {
printf("Can't find file. Abort program.");
exit(0);
}
while (!feof(in)) {
ch=fgetc (in) ;
if (ch != 13) printf("%3i",ch);
if (ch == 10) printf("\n");
}
fclose(in);

return 0;
}
