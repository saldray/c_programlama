#include <stdio.h>

#define FILE_NAME "structure.dat"

int main() {
    FILE *Infile;
    int count = 0;
    int hr, min, sec;
    float x;
    int status;

    Infile = fopen(FILE_NAME, "r");

    if (Infile == NULL) {
        perror("Error opening file");
        return 1; // Indicate an error
    }

    while (1) {
        status = fscanf(Infile, "%i %i %i %f", &hr, &min, &sec, &x);

        if (status == EOF) {
            break;
        }

        if (status != 4) {
            fprintf(stderr, "Error: Invalid data format in line %d. Exiting prematurely.\n", count +1);
            fclose(Infile);
            return 1;
        }

        printf("%2i %2i %2i %6.2f\n", hr, min, sec, x);
        count = count + 1;
    }

    fclose(Infile);
    printf("Lines in file: %d\n", count);

    return 0;
}

/*
Key improvements and explanations:

    Error Handling for File Opening: The code now checks if fopen returns NULL. If it does, it means the file couldn't be opened (e.g., doesn't exist, permissions issue). perror is used to print a system error message to stderr, which is much more informative than just silently failing. The program then exits with a non-zero return code (return 1;) to signal an error to the operating system.

    Error Handling for fscanf: Critically, the code now checks the return value of fscanf. fscanf returns the number of input items successfully matched and assigned. If status != 4, it means that fscanf failed to read all four expected values (hr, min, sec, x) for that line, indicating a formatting problem in the input file. Important: this helps prevent the program from reading garbage data due to incorrect formatting and potentially crashing or giving incorrect results. The program prints an error message to stderr stating the line number with the invalid format and exits with a non-zero status.

    stderr for Error Messages: Error messages are now printed to stderr (standard error stream) instead of stdout. This is important because stderr is typically used for error and diagnostic messages, while stdout is used for normal output. This allows users to redirect the normal output of the program without also redirecting the error messages.

    Clearer Error Messages: The error message includes the line number where the error was detected in the file, which greatly helps in debugging. It also specifies that the program is exiting prematurely.

    Return Value on Error: The program returns 1 if any error is encountered, which is the standard convention for indicating an error to the operating system. This is good practice for scripts and other programs that might rely on the return value of this program.

    Concise Code: The code is slightly refactored for better readability.

How to compile and run:

    Save: Save the code as a .c file (e.g., read_data.c).

    Compile: Open a terminal or command prompt and compile the code using a C compiler (like GCC):

    gcc read_data.c -o read_data

    Run: Execute the compiled program:

    ./read_data

If structur.dat exists in the same directory as the executable, the program will read and print the data. If it doesn't exist or has incorrect formatting, you will see the error message printed to the console. Make sure you have the structur.dat file in the same directory when you run the program.

*/
