#include <stdio.h>

#define FILE_NAME "structure.dat"

int main() {
    FILE *Infile;
    int count = 0;
    int hr, min, sec;
    float x;
    int status;

    Infile = fopen(FILE_NAME, "r");
    if (Infile == NULL) {
        perror("Error opening file");
        return 1;
    }

    do {
        status = fscanf(Infile, "%d %d %d %f", &hr, &min, &sec, &x);
        if (status == 4) {
            printf("%2d %2d %2d %6.2f\n", hr, min, sec, x);
            count++;
        }
    } while (status != EOF);

    fclose(Infile);
    printf("Lines in file: %d\n", count);

    return 0;
}
