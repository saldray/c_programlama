#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILE_NAME "stations.dat"
#define LINE_LENGTH 80 // Define maximum length of a line

int main(void) {
    FILE *infile;
    int ID, status, n_reports = 0, n_measurements = 0;
    char *line_ptr;
    char one_line[LINE_LENGTH];
    float measurements[8];

    infile = fopen(FILE_NAME, "r");
    if (infile == NULL) {
        perror("Error opening file");
        return EXIT_FAILURE;
    }

    while ((line_ptr = fgets(one_line, sizeof(one_line), infile)) != NULL) {
        // Initialize measurements array to avoid leftover values
        memset(measurements, 0, sizeof(measurements));

        // Read the line and parse it
        status = sscanf(one_line, "%i %f %f %f %f %f %f %f %f",
                        &ID, &measurements[0], &measurements[1], &measurements[2], &measurements[3],
                        &measurements[4], &measurements[5], &measurements[6], &measurements[7]);

        // Check for valid parsing
        if (status > 1) {
            n_reports++;
            n_measurements += (status - 1); // Subtract 1 to exclude the ID count
        } else {
            fprintf(stderr, "Error parsing line: %s", one_line);
        }
    }

    if (fclose(infile) != 0) {
        perror("Error closing file");
        return EXIT_FAILURE;
    }

    printf("There are %d records and %d measurements.\n", n_reports, n_measurements);

    return EXIT_SUCCESS;
}
