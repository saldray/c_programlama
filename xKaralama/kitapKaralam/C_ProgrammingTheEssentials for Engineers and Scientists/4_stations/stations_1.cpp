#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 80
#define FILENAME "stations.dat"

int main(void) {
    FILE *in = NULL;
    char one_line[MAX_LINE_LENGTH];
    int n_reports = 0;
    int n_measurements = 0;
    int ID;
    float measurement;
    char *token;
    char *rest;


    in = fopen(FILENAME, "r");
    if (in == NULL) {
        perror("Error opening file");
        return EXIT_FAILURE;
    }

    while (fgets(one_line, sizeof(one_line), in) != NULL) {
        n_reports++;

        // Remove trailing newline character, if present
        size_t len = strcspn(one_line, "\n");
        one_line[len] = '\0';

        // Tokenize the line using space as a delimiter
        rest = one_line;
        token = strtok_r(rest, " ", &rest); // Use strtok_r for thread safety

        if (token != NULL) {
            // First token should be the ID
            if (sscanf(token, "%d", &ID) == 1) {
                // Count the ID as the first element in the line so make sure to subtract from total elements in the row
                int count = 0;
                token = strtok_r(rest, " ", &rest);
                while (token != NULL){
                   if(sscanf(token, "%f", &measurement) == 1){
                        count++;
                   }else{
                     fprintf(stderr, "Warning: Invalid float format in line %d\n", n_reports);
                   }
                   token = strtok_r(rest, " ", &rest);
                }
                n_measurements += count;

            } else {
                fprintf(stderr, "Warning: Invalid ID format in line %d\n", n_reports);
            }
        } else {
            fprintf(stderr, "Warning: Empty line encountered.\n");
        }
    }

    fclose(in);

    printf("There are %d records and %d measurements.\n", n_reports, n_measurements);

    return EXIT_SUCCESS;
}
