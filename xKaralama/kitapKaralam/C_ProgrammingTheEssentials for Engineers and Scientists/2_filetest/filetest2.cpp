// #include <stdio.h>
// #include <string.h>
// #define FILE_NAME "structure.dat"
// int main ()
// {
// FILE *Infile;
// char one_line [100] ;
// int count=O;
// int hr,min,sec;
// float x;
// char *line_ptr;
// Infile=fopen(FILE_NAME, "r");
//
// while (1) {
// /* First read the line into a string. */
// line_ptr=fgets (one_line, sizeof (one_line) ,Infile);
// /* Quit if at end-of-file. */
// if (line_ptr == NULL) break;
// /* Replace "new line" with null character (optional) .*/
// one_line(strlen(one_line)-1)='\0';
// /* Print the string just as a test (optional). */
// printf("%s\n" ,one_line) ;
// /* Then scan the line to get numerical data. */
// (void) sscanf(one_line, "%d %d %d %f",&hr,&min,&sec,&x);
// printf("%2i %2i %2i %6.2f\n",hr,min,sec,x);
// /* Keep track of number of lines (optional). */
// count++;
// }
// fclose(Infile) ;
// printf("Lines in file : ");
// return 0;
// }


#include <stdio.h>
#include <string.h>

#define FILE_NAME "structure.dat"

int main() {
    FILE *Infile;
    char one_line[100];
    int count = 0;
    int hr, min, sec;
    float x;
    char *line_ptr;

    Infile = fopen(FILE_NAME, "r");
    if (Infile == NULL) {
        perror("Error opening file");
        return 1;
    }

    while (1) {
        /* First read the line into a string. */
        line_ptr = fgets(one_line, sizeof(one_line), Infile);
        /* Quit if at end-of-file. */
        if (line_ptr == NULL) break;
        /* Replace "new line" with null character (optional) .*/
        one_line[strlen(one_line) - 1] = '\0';
        /* Print the string just as a test (optional). */
        printf("%s\n", one_line);
        /* Then scan the line to get numerical data. */
        if (sscanf(one_line, "%d %d %d %f", &hr, &min, &sec, &x) == 4) {
            printf("%2d %2d %2d %6.2f\n", hr, min, sec, x);
        }
        /* Keep track of number of lines (optional). */
        count++;
    }

    fclose(Infile);
    printf("Lines in file: %d\n", count);
    return 0;
}
