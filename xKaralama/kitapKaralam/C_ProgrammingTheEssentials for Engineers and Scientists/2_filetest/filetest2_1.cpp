#include <stdio.h>
#include <string.h>
#include <stdlib.h>  // Required for exit()

#define FILE_NAME "structure.dat"
#define MAX_LINE_LENGTH 100

int main() {
    FILE *infile;
    char one_line[MAX_LINE_LENGTH];
    int count = 0;
    int hr, min, sec;
    float x;
    char *line_ptr;

    infile = fopen(FILE_NAME, "r");

    // Error handling: Check if the file opened successfully
    if (infile == NULL) {
        fprintf(stderr, "Error: Could not open file '%s'\n", FILE_NAME);
        perror("fopen"); // Print system error message
        return 1; // Indicate an error
    }

    while (1) {
        /* First read the line into a string. */
        line_ptr = fgets(one_line, sizeof(one_line), infile);

        /* Quit if at end-of-file. */
        if (line_ptr == NULL) {
            if (ferror(infile)) { // Check for read errors
                fprintf(stderr, "Error: An error occurred while reading the file.\n");
                perror("fgets");
                fclose(infile); // Close the file before exiting
                return 1;   // Indicate an error
            }
            break; // Exit the loop on EOF
        }

        /* Check for potential buffer overflow before modifying the string. */
        size_t len = strlen(one_line);
        if (len > 0 && one_line[len - 1] == '\n') {
            one_line[len - 1] = '\0'; // Replace newline with null terminator
        } else if (len == sizeof(one_line) - 1 && one_line[len] != '\0') {
            fprintf(stderr, "Warning: Line too long, potential buffer overflow.\n");
            // Consider truncating the line or skipping it based on your needs.
            one_line[len]='\0'; //Force null termination.
        }

        /* Print the string just as a test (optional). */
        printf("%s\n", one_line);

        /* Then scan the line to get numerical data. */
        int num_fields = sscanf(one_line, "%d %d %d %f", &hr, &min, &sec, &x);

        // Error handling: Check if sscanf successfully parsed the expected number of fields
        if (num_fields != 4) {
            fprintf(stderr, "Warning: Invalid data format in line %d: '%s'. Skipping line.\n", count + 1, one_line);
            // Consider adding error recovery logic here, like skipping the line or using default values.
            continue; // Skip to the next line
        }

        printf("%2i %2i %2i %6.2f\n", hr, min, sec, x);

        /* Keep track of number of lines (optional). */
        count++;
    }

    fclose(infile); // Close the file when done

    printf("Number of lines in file: %d\n", count); // Corrected the printf format

    return 0;
}

