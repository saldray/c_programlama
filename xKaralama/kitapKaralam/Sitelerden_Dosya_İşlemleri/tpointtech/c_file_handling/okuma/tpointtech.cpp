#include <stdio.h>

int main(){
    FILE *fp; // file pointer
    char ch;
    fp = fopen("file_handle.cpp", "r");
    while(1){
        ch = fgetc(fp); // Each character of the file is read and stored in the character file.
        if(ch == EOF)
            break;
        printf("%c", ch);
    }
    fclose(fp);
}

// "file_handle.cpp dosyasının mevcut olması lazım
// okuma modunda açar ve çıktıyı ekrana yazdırır.
// https://www.tpointtech.com/file-handling-in-c
