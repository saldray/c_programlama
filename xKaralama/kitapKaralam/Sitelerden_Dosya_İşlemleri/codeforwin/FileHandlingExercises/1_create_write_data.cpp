/*
 * C program to create a file and write data into file. 
 * https://codeforwin.org/c-programming/c-program-create-file-write-contents
 */

#include <stdio.h>
#include <stdlib.h>

#define DATA_SIZE 1000

int main(){
    /* Variable to store user content */
    char data[DATA_SIZE];

    /* File pointer to hold reference to our file */
    FILE * fptr;


    /*
     * Open file in w (write) mode.
     * "data/file.txt" is complete path to create file
     */
    fptr = fopen("file1.txt", "w");

    /* fopen() return NULL if last operation was unsuccessful */
    if(fptr == NULL){
        /* File not created hence exit */
        printf("Unable to create file.\n");
        exit(EXIT_FAILURE);
    }

    /* Input contents from user to store in file */
    printf("Enter contents to store in file : \n");
    fgets(data, DATA_SIZE, stdin);

    /* Write data to file */
    fputs(data,fptr);

    /* Close file to save data */
    fclose(fptr);

    /* Success message */
    printf("File created and saved successfully. :) \n");

    return 0;
}
