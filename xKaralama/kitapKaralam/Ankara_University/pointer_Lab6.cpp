/* 2. Aynı uzunlukta ve aynı harflerden oluşan kelimelere anagram kelimelere
denir. Kullanıcı tarafından girilen iki kelimenin anagram olup olmadığını
kontrol eden programı aşağıdaki gereksinimlere uyarak C programlama
dilinde yazınız.

Gereksinimler

* Anagram kontrolü bool veri tipinde değer döndüren fonksiyon yardımı
  ile yapılmalıdır.
* Program 26 harfli İngilizce Alfabesi dikkate alınarak yazılabilir.
  Türkçe harfler için özel geliştirme yapılmasını gerek yoktur.

Örnek Çıktı

Iki kelime giriniz: alem elma
'alem' ve 'elma' anagram kelimelerdir

Iki kelime giriniz: yasin ahmet
'yasin' ve 'ahmet' anagram kelimeler değildir */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

void countChar(char *str,int *count){
    while(*str){
        count[*str-'A']++;
        str++;
    }
}

bool areAnagram(char *str1, char *str2){
    int count1[60]={0};
    int count2[60]={0};
    countChar(str1,count1);
    countChar(str2,count2);
    for(int i=0;i<60;i++){
        if(count1[i]!=count2[i]){
            return false;
        }
    }
    return true;
}

int main(){
    char word1[100],word2[100];
    printf("İki kelime giriniz: ");
    scanf("%s %s",word1,word2);

    if(strlen(word1)==strlen(word2) && areAnagram(word1,word2)){
        printf("'%s' ve '%s' kelimeleri anagramdır.\n",word1,word2);
    }
    else{
        printf("'%s' ve '%s' kelimeleri anagram değildir.\n",word1,word2);
    }
}
