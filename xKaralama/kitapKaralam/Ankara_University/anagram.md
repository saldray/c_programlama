#include <stdio.h>
#include <string.h>
#include <stdbool.h>

// Function to count the occurrences of each character in a string
void countChar(char *str, int *count) {
    // Iterate through the string until the null terminator is reached
    while (*str) {
        // Increment the count for the corresponding character
        // Assuming the input string contains only uppercase letters (A-Z)
        // The index is calculated by subtracting 'A' from the current character
        count[*str - 'A']++;
        // Move to the next character in the string
        str++;
    }
}

// Function to check if two strings are anagrams
bool areAnagram(char *str1, char *str2) {
    // Initialize two arrays to store the character counts for each string
    int count1[60] = {0}; // Initialize all elements to 0
    int count2[60] = {0}; // Initialize all elements to 0

    // Count the character occurrences in the first string
    countChar(str1, count1);
    // Count the character occurrences in the second string
    countChar(str2, count2);

    // Compare the character counts for each letter
    for (int i = 0; i < 60; i++) {
        // If the character counts are different, the strings are not anagrams
        if (count1[i] != count2[i]) {
            return false; // Return false if character counts differ
        }
    }

    // If all character counts are the same, the strings are anagrams
    return true; // Return true if all character counts are equal
}

int main() {
    char word1[100], word2[100];

    // Prompt the user to enter two words
    printf("İki kelime giriniz: "); // Prompt in Turkish
    scanf("%s %s", word1, word2); // Read the two words from the input

    // Check if the strings are of equal length and anagrams of each other
    if (strlen(word1) == strlen(word2) && areAnagram(word1, word2)) {
        // If the strings are anagrams, print a message indicating that
        printf("'%s' ve '%s' kelimeleri anagramdır.\n", word1, word2); // Print in Turkish
    } else {
        // If the strings are not anagrams, print a message indicating that
        printf("'%s' ve '%s' kelimeleri anagram değildir.\n", word1, word2); // Print in Turkish
    }

    return 0; // Indicate successful program execution
}
Step-by-step explanation:

Includes:

stdio.h: Provides standard input/output functions like printf and scanf.
string.h: Provides string manipulation functions like strlen.
stdbool.h: Provides the bool data type and true/false values.
countChar Function:

Takes a string (str) and an integer array (count) as input.
Iterates through the string using a while loop until it reaches the null terminator (\0).
Inside the loop:
count[*str - 'A']++: Calculates the index into the count array by subtracting the ASCII value of 'A' from the current character's ASCII value. This assumes the input string contains only uppercase letters. The corresponding element in the count array is then incremented, effectively counting the occurrences of that character.
str++: Moves the pointer to the next character in the string.
areAnagram Function:

Takes two strings (str1, str2) as input.
Declares two integer arrays, count1 and count2, of size 60 (larger than needed for just uppercase letters to avoid potential buffer overflows with different inputs). Initializes all elements to 0. The problem description defines that inputs are upper case characters but the array size of the character count is too small so I increased it to 60 in order to prevent runtime errors for a broader set of characters.
Calls countChar to populate count1 with the character counts from str1 and count2 with the character counts from str2.
Iterates through the count arrays using a for loop.
Inside the loop:
if (count1[i] != count2[i]): If the character counts at the current index i are different, it means the strings are not anagrams, so the function returns false.
If the loop completes without finding any mismatched character counts, it means the strings are anagrams, so the function returns true.
main Function:

Declares two character arrays, word1 and word2, to store the input words.
Prompts the user to enter two words using printf. The prompt is in Turkish ("İki kelime giriniz:").
Reads the two words from the input using scanf. The %s format specifier reads a string until it encounters whitespace.
if (strlen(word1) == strlen(word2) && areAnagram(word1, word2)): Checks two conditions:
strlen(word1) == strlen(word2): Ensures that the two words have the same length. Anagrams must have the same number of characters.
areAnagram(word1, word2): Calls the areAnagram function to check if the words are anagrams of each other.
If both conditions are true, prints a message (in Turkish) indicating that the words are anagrams.
Otherwise, prints a message (in Turkish) indicating that the words are not anagrams.
return 0: Indicates that the program executed successfully.
Debugging Notes:

Array Size: The original code used an array size of 60. This is more than sufficient for only uppercase letters (A-Z, which would only need 26). A larger size offers robustness against non-uppercase inputs without throwing an error.
Input Validation: The code currently assumes the input will only contain uppercase letters. You might want to add input validation to handle lowercase letters, spaces, or other characters. You could convert the input strings to uppercase before processing them.
Case Sensitivity: The current code is case-sensitive because it directly compares the ASCII values of the characters. If you want a case-insensitive anagram check, you should convert both strings to either lowercase or uppercase before calling areAnagram.
Error Handling: The code doesn't handle potential errors from scanf, such as the user entering more characters than the word1 or word2 arrays can hold, leading to a buffer overflow. You could use fgets instead of scanf for safer input.
Character Set: The code assumes the ASCII character set. In other character sets, the difference between characters and 'A' might not be consistent. If you need more general character support, you need a different character counting mechanism.



Step 2: Identify Issues
Character Indexing: The countChar function assumes that the input strings consist only of uppercase letters (A-Z). If the input contains lowercase letters (a-z) or other characters, it will lead to out-of-bounds access in the count array.

Array Size: The count arrays (count1 and count2) are of size 60. This is not sufficient for all possible characters. It would be better to handle all ASCII characters or just A-Z/a-z.

Case Insensitivity: The current implementation does not handle case insensitivity. For example, "Listen" and "Silent" should be considered anagrams, but the current logic would not treat them as such.

Step 3: Fix the Issues
Here's the modified version of the code with the necessary fixes:


#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h> // For tolower function

void countChar(char *str, int *count) {
    while (*str) {
        // Convert character to lowercase to handle case insensitivity
        char ch = tolower(*str); 
        if (ch >= 'a' && ch <= 'z') { // Only count alphabetic characters
            count[ch - 'a']++;
        }
        str++;
    }
}

bool areAnagram(char *str1, char *str2) {
    int count1[26] = {0}; // Only need 26 for a-z
    int count2[26] = {0}; // Only need 26 for a-z

    countChar(str1, count1);
    countChar(str2, count2);

    for (int i = 0; i < 26; i++) {
        if (count1[i] != count2[i]) {
            return false;
        }
    }
    return true;
}

int main() {
    char word1[100], word2[100];

    printf("İki kelime giriniz: ");
    scanf("%99s %99s", word1, word2); // Limit input size to prevent overflow

    if (strlen(word1) == strlen(word2) && areAnagram(word1, word2)) {
        printf("'%s' ve '%s' kelimeleri anagramdır.\n", word1, word2);
    } else {
        printf("'%s' ve '%s' kelimeleri anagram değildir.\n", word1, word2);
    }

    return 0; // Return 0 to indicate successful execution
}

