// Example 3: C language stores provide a way to store the string
// as a pointer

#include <stdio.h>

int main(){
    // Storing string as pointer
    char *s = "Geeks";

    printf("%s", s);
    return 0;
}
