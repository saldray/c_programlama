#include <stdio.h>
#include <ctype.h>

int main() {
    char cumle[80];
    int i;
    printf("BUYUK harflerle bir cumle giriniz:\n");
    fgets(cumle, sizeof(cumle), stdin);

    for(i = 0; cumle[i]; i++)
        printf("%c", tolower(cumle[i]));
    printf("\n");

    return 0;
}

// input :PAMUKKALE UNIVERSITESI

