/* Bu programda DİZİ DEĞİŞKENİ kullanılarak
 * büyük harfler küçük harflere çevrilmiştir.
 */

#include <stdio.h>
#include <ctype.h>

int main(){
    char cumle[80];
    int i;
    printf("BUYUK harflerle bir cumle giriniz:\n");
    gets(cumle);

    for(i=0; cumle[i]; i++)
        printf("%c", tolower(cumle[i]));
    printf("\n");
}

/*
#include <stdio.h>
#include <ctype.h>
#include <string.h> // Include for using fgets

int main() {
    char cumle[80];
    int i;
    printf("BUYUK harflerle bir cumle giriniz:\n");

    // Use fgets instead of gets for safer input
    if (fgets(cumle, sizeof(cumle), stdin) == NULL) {
        printf("Hata: Girdi okunamadi.\n");
        return 1; // Exit with an error code
    }

    // Remove trailing newline character if present
    size_t len = strlen(cumle);
    if (len > 0 && cumle[len-1] == '\n') {
        cumle[len-1] = '\0';
    }

    for (i = 0; cumle[i]; i++) {
        printf("%c", tolower(cumle[i]));
    }
    printf("\n");
    return 0; // Exit successfully
}

Key improvements and explanations:

    #include <string.h>: This is crucial. The fgets() function and strlen() are part of the string.h library. Missing this include was a major error.
    fgets() instead of gets(): gets() is extremely dangerous because it doesn't perform bounds checking. If the user enters more than 79 characters, gets() will write past the end of the cumle buffer, leading to undefined behavior (crashes, security vulnerabilities, etc.). fgets() is much safer; it takes the buffer size as an argument and will stop reading input once that size is reached (or when it encounters a newline).
    Error Handling with fgets(): The code now checks the return value of fgets(). If fgets() returns NULL, it means an error occurred during input (e.g., end-of-file or a read error from stdin). The program now prints an error message and exits with a non-zero exit code (1), indicating an error. This is important for robust programs.
    Newline Removal: fgets() includes the newline character (\n) in the buffer if the user presses Enter. This is usually not what you want. The added code checks if the last character of the string is a newline and, if so, replaces it with a null terminator (\0) to remove it.
    Return Value for main(): The code now includes return 0; at the end of main(). This is the standard way to indicate that the program executed successfully. Returning a non-zero value (e.g., return 1; in the error case) indicates that an error occurred. This is good practice.
    Comments: I've added comments to explain the changes and the purpose of different parts of the code.

How to compile and run:

    Save: Save the code as a .c file (e.g., tolower.c).

    Compile: Open a terminal or command prompt and compile the code using a C compiler (like GCC):

    gcc tolower.c -o tolower

    Run: Execute the compiled program:

    ./tolower

Now the program will prompt you to enter a sentence in uppercase letters, and it will convert it to lowercase and print it to the console, handling potential errors and removing the trailing newline. It's much safer and more reliable.

 */


// ########################################
// Bir diğer yaklaşım
// Bir diğer yaklaşım
// ########################################
/*
It looks like the `gets` function is causing an error because it's not declared in this scope. The `gets` function has been deprecated because it poses a risk of buffer overflow. Instead, you can use `fgets` for safer input handling.

Here’s an updated version of your code using `fgets`:

```c
#include <stdio.h>
#include <ctype.h>

int main() {
    char cumle[80];
    int i;
    printf("BUYUK harflerle bir cumle giriniz:\n");
    fgets(cumle, sizeof(cumle), stdin);

    for(i = 0; cumle[i]; i++)
        printf("%c", tolower(cumle[i]));
    printf("\n");

    return 0;
}
```

In this version, `fgets` reads input from the standard input (`stdin`) and stores it in the `cumle` array, ensuring that it does not overflow. This should resolve the error and make your code safer. Happy coding! If you need more help, feel free to ask.
*/



















