// referensing   : belirtilen değişkenin adres noktasını tutma
// dereferensing : tutulan değişken adresinin kaldırılıp değişken
//                 değirinin belirtilmesi (gösterilmesi)
// referensing   : adres gösterme, belirtme
// dereferensing : gösterilen, belirtilen adresin kaldırılıp
//                 adresin gösterdiği, belirtiği değirni gösterilmesi
//                 belirtilmesi


// https://www.geeksforgeeks.org/c-pointers/?ref=lbp
/*
#include <stdio.h>

int main(){

    // an integer variable
    int a = 10;

    // Create a pointer to integer (declaration)
    int *ptr;

    // Store the address of a inside pointer (initialization)
    ptr = &a;

    // Print the content of ptr
    printf("ptr = %p\n", ptr);

    // Get the value pointed by ptr (dereferencing)
    printf("*ptr = %d", *ptr);
}
*/


// ##########################################
//          ALIŞTIRMALAR BURAYA YAZILIR
// ##########################################


//  #include <stdio.h>
//  
//  int main(){
//  
//      int a = 10;
//  
//      int *ptr;
//  
//      ptr = &a;
//  
//      printf("ptr = %p\n", ptr);
//      printf("*ptr = %d\n", *ptr);
//  
//      return 0;
//  }

// #####################################
//  Example 3: C language stores provide a way to store the string
//  as a pointer
// #####################################

// #include <stdio.h>
// 
// int main(){
// 
//     // Storing string as pointer
//     //const char *s = "Geeks";
//     char *s = "Geeks";
//     
//     printf("%s", s);
//     return 0;
// }

// #####################################

// This warning happens because in C++, string literals (like "Geeks") are of type `const char[]`, and a `const char[]` cannot be assigned to a `char*` without a cast. This is because modifying a string literal is undefined behavior.
// 
// To resolve this, you can do one of the following:
// 
// 1. Use a `const char*` instead of a `char*`:
// ```cpp
// #include <stdio.h>                                                    
//          
// int main(){
//          
//     // Storing string as pointer
//     const char *s = "Geeks"; /* Correct usage */
//          
//     printf("%s", s);
//     return 0;
// }
// ```
// 
// 2. If you need a non-const pointer for some reason, you can create a writable copy of the string:
// ```cpp
// #include <stdio.h>
// #include <string.h>
//          
// int main(){
//          
//     // Storing string as a writable copy
//     char s[] = "Geeks"; /* Writable copy */
//          
//     printf("%s", s);
//     return 0;
// }
// ```
// 
// Both of these solutions should compile without warnings in C++. The first solution should be preferred unless you specifically need a modifiable string.
