#include <stdio.h>
int main()
{
char answer;
printf("Would you like to know my name?\n");
printf("Type Y for YES and N for NO: ");
answer = getchar(); /* .... Reading a character...*/
if(answer == 'Y' || answer == 'y')
printf("\n\nMy name is BUSY BEE\n");
else
printf("\n\nYou are good for nothing\n");
}

/*
Would you like to know my name?
Type Y for YES and N for NO: Y
My name is BUSY BEE
Would you like to know my name?
Type Y for YES and N for NO: n
You are good for nothing
*/


// #include <stdio.h>
// #include <ctype.h> // Include for the tolower() function
//
// int main() {
//     char answer;
//
//     printf("Would you like to know my name?\n");
//     printf("Type Y for YES and N for NO: ");
//     answer = getchar(); /* .... Reading a character...*/
//
//     // Convert the input to lowercase using tolower()
//     answer = tolower(answer);
//
//     if (answer == 'y') {
//         printf("\nMy name is BUSY BEE\n");
//     } else {
//         printf("\nYou are good for nothing\n");
//     }
//
//     return 0;
// }
//
// Key improvements and explanations:
//
//     #include <ctype.h>: This is essential. The tolower() function is defined in the ctype.h header file. Without this, the compiler won't know what tolower() is.
//     answer = tolower(answer);: This is the core change. The line reads the character entered by the user and then converts it to lowercase using tolower(). The result is then assigned back to the answer variable. Now answer will hold either 'y' or 'n' regardless of whether the user typed Y or y (or N or n).
//     Simplified if condition: After converting the input to lowercase, the if condition becomes much simpler: if (answer == 'y'). You no longer need the || and the separate check for 'Y'.
//     return 0; in main(): It's good practice to include return 0; at the end of main() to indicate successful program execution. While not strictly required in some compilers, it's best practice.
//     Clarity and Readability: The code is formatted for better readability.
//
// How it works:
//
//     Get Input: getchar() reads a single character from the input.
//     Convert to Lowercase: tolower(answer) takes the character in answer and converts it to its lowercase equivalent if it's an uppercase letter. If it's already lowercase, or if it's not a letter at all, tolower() returns the character unchanged.
//     Check the Lowercase Value: The if condition now only needs to check if answer is equal to 'y'. Because the input was converted to lowercase, this will work correctly whether the user entered y or Y.
//
// This improved version is much more robust and easier to maintain because it handles both uppercase and lowercase 'y' characters correctly. It also reduces the chance of errors in the if condition.

