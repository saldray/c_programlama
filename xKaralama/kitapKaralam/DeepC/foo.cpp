// https://solarex.github.io/downloads/files/DeepC0.pdf

#include <stdio.h>

int main(void){
    int v[] = {0,2,4,6,8};
    int i = 1;
    int n = i + v[++i] + v[++i];
    printf("%d\n", n);
    return 0;
}


/*
#include <stdio.h>

int main(void){
    int v[] = {0, 2, 4, 6, 8};
    int i = 1;
    int n;

    // Calculate n safely by avoiding multiple modifications of i within a single expression.
    i++; // i becomes 2
    n = i; // n becomes 2
    n += v[i]; // n becomes 2 + v[2] = 2 + 4 = 6
    i++; //i becomes 3
    n += v[i]; // n becomes 6 + v[3] = 6 + 6 = 12

    printf("%d\n", n);
    return 0;
}

Explanation of the problem and the fix:

The original code int n = i + v[++i] + v[++i]; invokes undefined behavior in C. The problem is that you are modifying the value of i multiple times within the same expression without intervening sequence points. A sequence point is a point in the execution of a program where all side effects of previous evaluations are complete, and no side effects of subsequent evaluations have yet taken place.

In simpler terms, the compiler is free to evaluate the ++i operations in any order it chooses, and the result of the expression will be unpredictable. Different compilers, different optimization levels, or even small changes to the code could lead to different outputs. That [-Wsequence-point] warning specifically tells you this is happening.

The corrected code avoids this issue by:

    Incrementing i separately: Instead of using ++i within the larger expression, the code now increments i on its own lines (i++;). This guarantees the order of evaluation.
    Breaking down the calculation: The calculation of n is broken into multiple steps, adding each term one at a time. This makes the code easier to understand and eliminates the undefined behavior related to modifying i multiple times within a single expression.

Why this is important:

    Predictability: The corrected code will always produce the same output, regardless of the compiler or optimization settings.
    Portability: Your code will work correctly on different platforms and compilers.
    Maintainability: The corrected code is easier to understand and modify.
    Avoiding Bugs: Undefined behavior can lead to subtle and hard-to-debug errors. It's crucial to avoid it whenever possible.

By incrementing i separately and breaking down the calculation, you ensure that the value of i is well-defined at each step, eliminating the undefined behavior and producing a reliable result.
*/
