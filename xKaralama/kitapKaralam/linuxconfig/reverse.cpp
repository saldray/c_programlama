// https://linuxconfig.org/c-development-on-linux-pointers-and-arrays-vi
// https://toolbaz.com/writer/ai-code-writer

#include <stdio.h>
#include <string.h>

int main()
{
  char stringy[30];
  int i;
  char c;
  printf("Type a string .\n");
  fgets(stringy, 30, stdin);
  printf("\n");

  for(i = 0; i < strlen(stringy); i++)
    printf("%c", stringy[i]);
  printf("\n");
  for(i = strlen(stringy); i >= 0; i--)
    printf("%c", stringy[i]);
  printf("\n");

  return 0;
}

/*
 * #include <stdio.h>
#include <string.h>

int main()
{
  char stringy[30]; // Declares a character array named 'stringy' of size 30. This array will store the string entered by the user.
  int i;             // Declares an integer variable 'i', which will be used as a loop counter.
  char c;            // Declares a character variable 'c'. Even though this variable is declared, the code doesn't use it. This is typically a sign of some previous logic that was removed without also removing the variable.
  printf("Type a string .\n"); // Prompts the user to enter a string.
  fgets(stringy, 30, stdin);   // Reads a line of text from standard input (stdin) and stores it in the 'stringy' array.
                                 //  - fgets(destination, max_length, source)
                                 //  - 'stringy' is the destination array.
                                 //  - 30 is the maximum number of characters to read (including the null terminator). This prevents buffer overflows if the user enters a very long string.
                                 //  - 'stdin' is the standard input stream (usually the keyboard).
  printf("\n");         // Prints a newline character to the console for better formatting.

  for (i = 0; i < strlen(stringy); i++) // This loop iterates through the string from the beginning to the end.
    printf("%c", stringy[i]);           // Prints each character of the string, one at a time, to the console.
  printf("\n");                     // Prints a newline character for formatting.

  for (i = strlen(stringy); i >= 0; i--) // This loop iterates through the string in reverse order.
    printf("%c", stringy[i]);           // Prints each character of the string, one at a time, to the console.  This includes printing the null terminator, and possibly a garbage value at the beginning because `strlen` does not include the null terminator, yet we are accessing it.
  printf("\n");                     // Prints a newline character for formatting.

  return 0; // Indicates that the program executed successfully.
}

Explanation in Detail:

    Include Headers:
        #include <stdio.h>: This line includes the standard input/output library, which provides functions like printf (for printing to the console) and fgets (for reading input from the keyboard).
        #include <string.h>: This line includes the string library, which provides functions for working with strings, such as strlen (for calculating the length of a string).

    main Function:
        int main(): This is the main function where the program execution begins. The int indicates that the function returns an integer value (usually 0 to signal successful execution).

    Variable Declarations:
        char stringy[30];: This declares a character array named stringy. It can hold a string of up to 29 characters plus the null terminator (\0). The null terminator marks the end of the string in C.
        int i;: This declares an integer variable i, which will be used as a loop counter.
        char c;: This declares a character variable c. However, it is not used in the program. This is useless code that should be removed.

    Prompt for Input:
        printf("Type a string .\n");: This line prints a message to the console, prompting the user to enter a string. The \n adds a newline character at the end, moving the cursor to the next line.

    Read Input:
        fgets(stringy, 30, stdin);: This is the most important part for reading the string.
            fgets reads a line of text from the specified input stream and stores it in the specified character array.
            stringy: The character array where the input will be stored.
            30: The maximum number of characters to read, including the null terminator. This prevents buffer overflows (writing beyond the allocated memory). If the user enters more than 29 characters, fgets will read only the first 29 characters and then add the null terminator.
            stdin: The standard input stream, which is usually the keyboard.
            Important: fgets also reads the newline character (\n) if the user presses Enter. This newline will be included in the stringy array.

    Print Newline (Formatting):
        printf("\n");: Prints a newline for formatting.

    Print the String (Forward):
        for(i = 0; i < strlen(stringy); i++): This loop iterates through the string from the first character to the last.
            i = 0: The loop starts with i equal to 0 (the index of the first character).
            i < strlen(stringy): The loop continues as long as i is less than the length of the string. strlen(stringy) returns the number of characters in the string (excluding the null terminator).
            i++: After each iteration, i is incremented (increased by 1).
        printf("%c", stringy[i]);: This line prints the character at index i of the stringy array. %c is a format specifier that tells printf to print a character.
        printf("\n");: Prints a newline for formatting.

    Print the String (Backward):
        for(i = strlen(stringy); i >= 0; i--): This loop iterates through the string in reverse order.
            i = strlen(stringy): The loop starts with i equal to the length of the string. This is one position beyond the last meaningful character because strlen doesn't include the null terminator. This is problematic because we are accessing memory outside the string's bounds in the first iteration.
            i >= 0: The loop continues as long as i is greater than or equal to 0.
            i--: After each iteration, i is decremented (decreased by 1).
        printf("%c", stringy[i]);: This prints the character at index i of the stringy array. Potential Issue: Because the loop starts with i = strlen(stringy), the first character printed will be whatever is stored after the last character of the string (likely the null terminator), which could result in unexpected output or even undefined behavior if the program tries to access memory outside the bounds of stringy.
        printf("\n");: Prints a newline for formatting.

    Return 0:
        return 0;: This indicates that the program executed successfully.

Key Improvements and Considerations:

    Buffer Overflow Protection: The fgets function is used to prevent buffer overflows. Always use fgets or similar safe input functions when reading strings in C to avoid security vulnerabilities.

    Newline Character: The fgets function includes the newline character (\n) in the string if the user presses Enter. In many cases, you'll want to remove this newline. You can do this like this:

    stringy[strcspn(stringy, "\n")] = 0; // Overwrites \n with \0

    This line uses strcspn to find the first occurrence of a newline character in the string and then replaces it with a null terminator, effectively removing the newline. You'd put this line after the fgets call.

    Reverse Printing Bug: The reverse printing loop has a bug. It starts at strlen(stringy), which is the null terminator. This leads to printing the null terminator and then the string in reverse, potentially including garbage before the reversed string. To fix this, the loop needs to start one character before the null terminator:

    for (i = strlen(stringy) - 1; i >= 0; i--)
        printf("%c", stringy[i]);

    Unused Variable: The char c variable is declared but never used. It should be removed to keep the code clean.

Corrected Code (with newline removal and reverse printing fix):

#include <stdio.h>
#include <string.h>

int main()
{
  char stringy[30];
  int i;

  printf("Type a string .\n");
  fgets(stringy, 30, stdin);

  // Remove the trailing newline character, if present
  stringy[strcspn(stringy, "\n")] = 0;

  printf("\n");

  for (i = 0; i < strlen(stringy); i++)
    printf("%c", stringy[i]);
  printf("\n");

  // Corrected reverse printing loop
  for (i = strlen(stringy) - 1; i >= 0; i--)
    printf("%c", stringy[i]);
  printf("\n");

  return 0;
}

This corrected code addresses the newline issue and the reverse printing bug, making it more robust and reliable. Remember to compile and run the code to see it in action.

 */
