#include <stdio.h>

void printAlignedTriangle(int height);

int main(){

    int triangleHeight;
    printf("Bir deger girinis:");
    // scanf("%d ") sorun olan yer burasıymış
    scanf("%d", &triangleHeight); // Define the height of the triangle

    printAlignedTriangle(triangleHeight); // call the function to print the pattern
    return 0;
}

void printAlignedTriangle(int height){
    for(int row=height; row >= 1; row--){
        for(int col = 1; col <= (row - 1); col++)
            printf(" ");
        for(int col = 1; col <= height - (row - 1); col++)
            printf("*");
        printf("\n");
    }
    return;
}









// #include <stdio.h>
//
// void printAlignedTriangle(int height);
//
// int main() {
//
//     int triangleHeight;
//     printf("Bir deger girinis:");
//     scanf("%d", &triangleHeight); // Define the height of the triangle
//
//     printAlignedTriangle(triangleHeight); // call the function to print the pattern
//     return 0;
// }
//
// void printAlignedTriangle(int height) {
//     for (int row = height; row >= 1; row--) {
//         for (int col = 1; col <= (row - 1); col++)
//             printf(" ");
//         for (int col = 1; col <= height - (row - 1); col++)
//             printf("*");
//         printf("\n");
//     }
//     return;
// }

// Explanation of why the code produces the desired output:
//
// The code aims to print a right-aligned triangle pattern of asterisks with a specified height. Let's break down how it achieves this:
//
//     main() function:
//         Prompts the user to enter a value (triangle height) using printf("Bir deger girinis:");.
//         Reads the integer input using scanf("%d", &triangleHeight);. Important: The original code had "%d " with a space after %d. This is problematic because scanf would wait for a non-whitespace character after the number. I've removed the space.
//         Calls the printAlignedTriangle() function, passing the entered height as an argument.
//         Returns 0 to indicate successful execution.
//
//     printAlignedTriangle() function:
//         Outer loop (rows): for (int row = height; row >= 1; row--)
//             Iterates from height down to 1. Each iteration represents a row of the triangle. This is the correct outer loop structure.
//         Inner loop (spaces): for (int col = 1; col <= (row - 1); col++) printf(" ");
//             This loop prints the leading spaces before the asterisks in each row. The number of spaces to print depends on the current row number (row). More specifically, it prints row - 1 spaces. For row height, this is height - 1 spaces, for row height - 1, this is height - 2 spaces, and so on. This ensures the right alignment.
//         Inner loop (asterisks): for (int col = 1; col <= height - (row - 1); col++) printf("*");
//             This loop prints the asterisks for each row. The number of asterisks to print also depends on the current row number, but the calculation is a bit different. It prints height - (row - 1) asterisks. For row height, this is height - (height - 1) = 1 asterisk; For row height - 1, this is height - (height - 2) = 2 asterisks; for row height - 2, this is height - (height - 3) = 3 asterisks, and so on.
//         Newline: printf("\n");
//             After printing the spaces and asterisks for a row, this statement moves the cursor to the next line, ensuring that the triangle is printed vertically.
//
// Example (height = 5):
// Row 	row 	Spaces Printed 	Asterisks Printed
// 1 	5 	4 	1
// 2 	4 	3 	2
// 3 	3 	2 	3
// 4 	2 	1 	4
// 5 	1 	0 	5
//
// This corresponds precisely to the desired aligned triangle output. By controlling the number of leading spaces and asterisks printed in each row, the code generates the pattern correctly.



