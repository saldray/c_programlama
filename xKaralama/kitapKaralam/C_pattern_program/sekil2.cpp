#include <stdio.h>

int main() {
  int rows;

  printf("Enter the number of rows: ");
  scanf("%d", &rows);

  // Outer loop for rows
  for (int i = 1; i <= rows; i++) {
    // Inner loop for spaces (to create the right alignment)
    for (int j = 1; j <= rows - i; j++) {
      printf(" ");
    }

    // Inner loop for stars
    for (int k = 1; k <= i; k++) {
      printf("*");
    }

    // Move to the next line after each row is printed
    printf("\n");
  }

  return 0;
}

// Key improvements and explanations for making the code "more beautiful":
//
//     Clarity and Readability:
//         Meaningful Variable Names: Instead of i, j, k, use rows, spaces, stars (though here i, j, k are fairly standard for loop counters). rows is important to extract into a variable to make the code more adaptible.
//         Comments: Include comments to explain the purpose of each loop. This is crucial for understanding the logic, especially when the pattern gets more complex.
//         Indentation: Consistent indentation makes the code structure immediately apparent.
//
//     Efficiency (minor in this case, but good practice):
//         This version does not pre-calculate anything. For very large patterns, you might consider pre-calculating the necessary number of spaces for each row to potentially optimize. But for typical use cases, the performance difference would be negligible, and the added complexity might reduce readability. The most common situation is that you are not worried about pre-calculating values in this case.
//
//     Adaptability:
//         The code is designed to easily change the number of rows by modifying the rows variable.
//
//     Correctness: The code produces the exact pattern requested.
//
//     Conciseness: The code avoids unnecessary complexity while still maintaining readability. It's a balance between being compact and being understandable.
//
// How to compile and run (example):
//
//     Save: Save the code as a .c file (e.g., pattern.c).
//     Compile: Open a terminal or command prompt and use a C compiler (like GCC):
//
//     gcc pattern.c -o pattern
//
//     Run: Execute the compiled program:
//
//     ./pattern
//
//     The program will prompt you to enter the number of rows.
//
// Example Output (for rows = 5):
//
//     *
//    **
//   ***
//  ****
// *****
//
// Further enhancements (beyond the original prompt but useful to know):
//
//     Error Handling: You could add error checking to ensure the user enters a valid positive integer for the number of rows.
//     Function Abstraction: For more complex patterns, encapsulate the pattern generation logic within a function. This makes the code more modular and reusable. For example:
//
//     void print_right_triangle(int rows) {
//         // pattern printing code from main
//     }
//
//     int main() {
//         int rows;
//         printf("Enter the number of rows: ");
//         scanf("%d", &rows);
//         print_right_triangle(rows);
//         return 0;
//     }
//
//     Different Characters: Allow the user to specify the character used to draw the pattern (e.g., using # instead of *).
//     More Complex Patterns: Explore other patterns (e.g., inverted triangles, pyramids, diamonds). The key is to break down the pattern into rows, spaces, and characters and then use nested loops to control the output.
//

