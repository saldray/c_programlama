#include <stdio.h>

// Function to print a right-aligned triangle pattern
void printRightAlignedTriangle(int height) {
    for (int row = height; row >= 1; row--) {
        // Print leading spaces
        for (int col = 1; col <= height - row; col++) {
            printf(" ");
        }

        // Print asterisks
        for (int col = 1; col <= row; col++) {
            printf("*");
        }

        printf("\n"); // Move to the next line after each row
    }
}

int main() {
    int triangleHeight = 5; // Define the height of the triangle
    printRightAlignedTriangle(triangleHeight); // Call the function to print the triangle

    return 0;
}

// Key improvements and explanations:
//
//     Function for Reusability: The code is now encapsulated within a function printRightAlignedTriangle. This makes the code more modular, reusable, and easier to understand. You can easily change the height by passing a different value to the function.
//
//     Descriptive Variable Names: More descriptive variable names like triangleHeight and row, col improve readability.
//
//     Comments: Comments explain the purpose of each part of the code, making it easier to understand the logic.
//
//     Clearer Logic: The logic for printing spaces and asterisks is separated into distinct loops, making it easier to follow. The comments clarify what each loop is doing.
//
//     height Parameter: Instead of hardcoding the 5, the height of the triangle is now controlled by the triangleHeight variable, making the pattern easily adjustable.
//
//     \n Placement: printf("\n"); is placed outside the inner loop but inside the outer loop, ensuring that a newline character is printed after each row of the pattern, correctly forming the triangle shape.
//
//     Standard Library Usage: Using <stdio.h> is standard for C input/output; it's included in both versions, but this version emphasizes best practices.
//
// How to compile and run:
//
//     Save: Save the code as a .c file (e.g., triangle.c).
//     Compile: Open a terminal or command prompt and use a C compiler (like GCC) to compile the code:
//
//     gcc triangle.c -o triangle
//
//     Run: Execute the compiled program:
//
//     ./triangle
//
// This will print the right-aligned triangle pattern to the console. You can adjust the triangleHeight variable in the code to change the size of the triangle.

