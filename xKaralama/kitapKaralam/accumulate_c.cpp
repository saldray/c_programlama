/*
 * C program to create a file and write data into file.
 * https://codeforwin.org/c-programming/c-program-create-file-write-contents
 */

// #include <stdio.h>
// #include <stdlib.h>
//
// #define DATA_SIZE 1000
//
// int main(){
//     /* Variable to store user content */
//     char data[DATA_SIZE];
//
//     /* File pointer to hold reference to our file */
//     FILE * fptr;
//
//
//     /*
//      * Open file in w (write) mode.
//      * "data/file.txt" is complete path to create file
//      */
//     fptr = fopen("file1.txt", "w");
//
//     /* fopen() return NULL if last operation was unsuccessful */
//     if(fptr == NULL){
//         /* File not created hence exit */
//         printf("Unable to create file.\n");
//         exit(EXIT_FAILURE);
//     }
//
//     /* Input contents from user to store in file */
//     printf("Enter contents to store in file : \n");
//     fgets(data, DATA_SIZE, stdin);
//
//     /* Write data to file */
//     fputs(data,fptr);
//
//     /* Close file to save data */
//     fclose(fptr);
//
//     /* Success message */
//     printf("File created and saved successfully. :) \n");
//
//     return 0;
// }
//
// // ************************************************************
//
// // C program to write data into a file using fprintf() function
// // https://www.codezclub.com/c-write-file-using-fprintf-function/
//
// #include <stdio.h>
//
// int main(){
//     FILE * fp;
//     char file_name[50];
//
//     printf("Enter the file name :: ");
//     scanf("%s", file_name);
//     fp = fopen(file_name, "w");
//
//     fprintf(fp, "%s %s %s", "Welcome", "to", "C_world");
//     printf("\nSuccessfully written to the file %s \n", file_name);
//
//     fclose(fp);
//     return 0;
// }
//
// // ***********************************************************
//
// #include <stdio.h>
//
// int main(){
//     FILE *fp; // file pointer
//     char ch;
//     fp = fopen("file_handle.cpp", "r");
//     while(1){
//         ch = fgetc(fp); // Each character of the file is read and stored in the character file.
//         if(ch == EOF)
//             break;
//         printf("%c", ch);
//     }
//     fclose(fp);
// }
//
// // "file_handle.cpp dosyasının mevcut olması lazım
// // okuma modunda açar ve çıktıyı ekrana yazdırır.
// // https://www.tpointtech.com/file-handling-in-c


// https://www.geeksforgeeks.org/c-file-io/
// https://www.geeksforgeeks.org/basics-file-handling-c/?ref=lbp
// https://www.geeksforgeeks.org/c-file-handling-programs/


























