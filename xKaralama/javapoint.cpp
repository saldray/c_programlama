// https://www.javatpoint.com/c-pointers
// #include <stdio.h>

// int main(){
//   int number = 50;
//   int *p;

//   p = &number;
//   printf("Adress of p variable is %x \n", p);
//   printf("Value of p variable is %d \n",*p);
//   printf("Adres of number variab is %x \n", &number);
//   printf("value of number variab is %d \n", number);
// }


// int (*p)(int (*)[2], int (*)void))


// Bu gösterici, ilk parametreyi iki boyutlu bir tamsayı dizisinin göstericisi olarak ve ikinci parametreyi parametresi void ve geri dönüş türü tamsayı olan bir fonksiyonun göstericisi olarak kabul eden böyle bir fonksiyonun göstericisi olarak okunacaktır.


// Double pointer example

// #include <stdio.h>

// int main(){
//   int number = 50;
//   int *p;
//   int **p2;

//   p = &number;
//   p2 = &p;

//   printf("Address of number variable is %x \n", &number);
//   printf("Address of p variable is %d \n", p);
//   printf("Value of *p variable is %d \n", *p);
//   printf("Address of p2 variable is %x \n", p2);
//   printf("Value ou **p2 variable is %d \n", *p);
//   return 0;
// }


// #include <stdio.h>

// int main(){

//   int number = 50;
//   int *p;
//   int **p2;

//   p = &number;
//   p2 = &p;

//   printf("number degiskeninin adresi %x \n", &number);
//   printf("p degiskeninin adresi %x \n", p);
//   printf("*p degiskeninin degeri %d \n", *p);
//   printf("p2 degiskeninin adresi %x \n", p2);
//   printf("**p2 degiskeninin degeri %d \n", *p);

//   printf("p2 nin adresi %p \n", &p2);
//   printf("p nin adresi %p : \n", &p);
//   return 0;
// }


/*Program 1 */

// #include <stdio.h>

// int main(){

//   int i = 3;

//   printf("\nAddress of i = %u", &i);
//   printf("\nValue of i = %u",i);
// }


// "*" value at address Adresteki değer
// it returns the value stored at a particular address.
// indirection (atıf ile,)
// indirection operator dolaylama işleci


/*Program2*/

// #include <stdio.h>

// int main(){

//   int i = 3;

//   printf("\nAddress of i = %u", &i);
//   printf("\nValue of i = %d", i);
//   printf("\nValue of i = %d", *(&i));
// }



// #include <stdio.h>

// int main(){

//   int i = 3;

//   printf("\nAddress of i = %u", &i);
//   printf("\nValue of i = %d", i);
//   printf("\nValue of i = %d\n", *(&i));
// }


/*Program 3*/

// #include <stdio.h>

// int main(){

//   int i = 3;
//   int *j;

//   j = &i;

//   printf("\nAdress of i = %u", &i);
//   printf("\nAdress of i = %u", j);
//   printf("\nAdress of j = %u", &j);
//   printf("\nValue of j = %d", j);
//   printf("\nValue of i = %d", i); //*(&i)
//   printf("\nValue of i = %d", *(&i));
//   printf("\nValue of i = %d\n", *j);
// }


/*Program 4*/

// #include <stdio.h>

// int main(){

//   int i = 3;
//   int *j;
//   int **k;

//   j = &i;
//   k = &j;

//   printf("\nAddress of i = %u", &i);
//   printf("\nAddress of i = %u", j);
//   printf("\nAddress of i = %u", *k);
//   printf("\nAddress of j = %u", &j);
//   printf("\nAddress of j = %u", k);
//   printf("\nAddress of k = %u", &k);

//   printf("\n\nValue of j = %u", j);
//   printf("\nValue of k = %u", k);
//   printf("\nValue of i = %d", i);
//   printf("\nValue of i = %d", *(&i));
//   printf("\nValue of i = %d", *j);
//   printf("\nValue of i = %d", **k);
// }


// #include <stdio.h>

// int main(){

//   char c,*cc;
//   int i, *ii;
//   float a, *aa;

//   c='A'; //acsii value of A gets stored in c
//   i = 54;
//   a = 3.14;
//   cc = &c;
//   ii = &i;
//   aa = &a;

//   printf("\nAddress contained in cc = %u", cc);
//   printf("\nAddress contained in ii = %u", ii);
//   printf("\nAddress contained in aa = %u", aa);
//   printf("\nValue of c = %c", *cc);
//   printf("\nValue of i = %d", *ii);
//   printf("\nValue of a = %f", *aa);
// }


// #include <stdio.h>

// int main(){

//   char c, *cc;
//   int i, *ii;
//   float a, *aa;

//   c = 'A';
//   i = 54;
//   a = 3.14;

//   cc = &c;
//   ii = &i;
//   aa = &a;

//   printf("\ntuttuğu adres cc = %u", cc);
//   printf("\ntuttuğu adres ii = %u", ii);
//   printf("\ntuttuğu adres aa = %u", aa);

//   printf("\n\nc nin değeri %d", *cc);
//   printf("\n*(&c) %d", *(&c));
//   printf("\ndoğrudan c ile %d", c);
//   printf("\ni nin değeri %d", *ii);
//   printf("\n*(&i), %d", *(&i));
//   printf("\ndoğrudan i, %d", i);
//   printf("\na nın değeri %.2f", *aa);
//   printf("\na nin adresi ile %.3f", *(&a));
//   printf("\na doğrudan %.3f\n", a);
// }


/*Program 8*/

#include <stdio.h>

int main(){

  int a = 10
}
