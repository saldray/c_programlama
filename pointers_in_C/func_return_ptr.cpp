// // https://www.geeksforgeeks.org/how-to-return-a-pointer-from-a-function-in-c/
// #include <stdio.h>

// // Function returning pointer

// int *fun(){
//   int A = 10;
//   return (&A);
// }

// int main(){
//   // Declare a pointer
//   int *p;
//   // Function call
//   p = fun();

//   printf("%p\n", p);
//   printf("d\n", *p);
//   return 0;
// }

// Below is the output of the above program:
// (Nill)
// segmentation fault (core dumped)

// Explanation:

// The main reason behind this scenario is that compiler always
// make a stack for a function call. As soon as the function exits
// the function stack also gets removed which causes the local
// variables of functions goes out of scope.

// Static Variables have a property of preserving their value
// even after they are out of their scope. So to execute the
// concept of returning a pointer from function in C you must
// define the local variable as a static variable.

#include <stdio.h>

// Function that returns pointer
int *fun(){
  //Declare a static integer
  static int A = 10;
  return (&A);
}

int main(){
  // Declare a pointer
  int *p;

  // Function call
  p = fun();

  // Print Address
  printf("%p\n",p);

  // Print value at the above address
  printf("%d\n", *p);
  return 0;
}
