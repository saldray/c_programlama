#include <stdio.h>
#include <string.h>

//int strcmp(const char* pf, const char* p2);

int main(){
    char s1[] = "necati";
    char s2[] = "necati";

    if (!strcmp(s1, s2)){
        print("evet esit\n");
    }
    else {
        printf("hayir esit degil\n");
    }
}

// https://www.geeksforgeeks.org/strcmp-in-c/?ref=header_search
//
// Bu program C dilinde yazılmıştır ve iki karakter dizisini karşılaştıran strcmp() fonksiyonunu kullanmaktadır. İlk olarak stdio.h ve string.h kütüphaneleri dahil edilmiştir.
//
// int main() fonksiyonu programın başlangıç noktasıdır. İki tane karakter dizisi s1 ve s2 tanımlanmış ve her ikisi de "necati" değeri ile başlatılmıştır.
//
// Daha sonra strcmp() fonksiyonu kullanılarak s1 ve s2 karakter dizileri karşılaştırılmıştır. Eğer karakter dizileri eşitse, yani strcmp() fonksiyonu 0 dönerse, "evet esit" ifadesi ekrana yazdırılır. Aksi takdirde, yani karakter dizileri eşit değilse, "hayir esit degil" ifadesi ekrana yazdırılır.
//
// strcmp() fonksiyonu karakter dizilerinin eşit olup olmadığını kontrol eder. Eğer iki karakter dizisi eşitse, 0 değeri döner. Eğer ilk karakter dizisi ikincisinden küçükse, negatif bir değer döner. Eğer ilk karakter dizisi ikincisinden büyükse, pozitif bir değer döner.
//
// Bu program strcmp() fonksiyonunu kullanarak iki karakter dizisini karşılaştırarak ekrana karşılaştırma sonucunu yazdırmaktadır.
//
// https://chatgptfree.ai/
//
// The provided C programming code includes two header files, <stdio.h> and <string.h>, which are used for input/output operations and string manipulation functions, respectively.
// The code defines a main function where two character arrays,
// s1 and s2, are initialized with the strings "necati".
//
// The code then uses the strcmp function from the string.h library to compare the two strings s1 and s2. The strcmp function returns 0 if the strings are equal. In this case,
// if the condition "!strcmp(s1, s2)" is true (meaning the strings are equal), the code will print "evet esit\n". Otherwise, it will print "hayir esit degil\n".
//
// Overall, this code snippet is comparing two strings "necati" using the strcmp function and printing whether they are equal or not based on the comparison result.
//
//
// https://blog.finxter.com/
//
// Certainly! The C program you've posted is a simple comparison program that checks whether two strings are equal or not. Here's a step-by-step explanation of what each part does:
//
// #include <stdio.h>  // This is a preprocessor directive that tells the compiler to include the standard input/output library, which is necessary for using printf().
//
// #include <string.h>  // This directive includes the string library, which provides the strcmp() function used to compare two strings.
//
// Now, let's go through the main() function:
//
// int main(){
//     // ...
// }
//
// This is the entry point of any C program. The main() function is where the execution starts.
//
// Inside the main() function, you have the following:
//
// char s1[] = "necati";  // This declares an array of characters (a string) and initializes it with the value "necati".
// char s2[] = "necati";  // This does the same for another string with the same value "necati".
//
// Next, you perform the comparison:
//
// if (!strcmp(s1, s2)){
//     printf("Yes, they are equal.\n");
// }
// else {
//     printf("No, they are not equal.\n");
// }
//
//     strcmp(s1, s2) is a function call to strcmp() which compares the two strings s1 and s2. The strcmp() function returns 0 if the strings are equal.
//     The ! operator is the logical NOT operator. It inverts the result of the strcmp() function. So, if strcmp() returns 0 (which means the strings are equal), the ! operator will make it True (since !0 is True in C).
//     As a result, if s1 and s2 are equal, the printf("Yes, they are equal.\n"); statement is executed, printing the message to the screen.
//     If s1 and s2 are not equal, the program would instead execute the else block, but in this particular case, since they are initialized with the same value, this block will not be reached.
//
// Finally:
//
// return 0;  // This terminates the main() function and returns 0, signaling that the program executed successfully.
//
// In this specific code snippet, the output will always be "Yes, they are equal." because both s1 and s2 are initialized with the same string literal.


// https://chatgbt.one/
//
// The provided C code snippet includes the necessary header files for standard input/output operations and string manipulation functions. The code defines a main function that compares two strings, "s1" and "s2", using the strcmp function from the string.h library.
//
// In this code, the strings "necati" are assigned to both "s1" and "s2". The strcmp function is used to compare these two strings. If the strings are equal, the code will print "evet esit\n" (yes, equal). Otherwise, it will print "hayir esit degil\n" (no, not equal).
//
// One issue in the code is that the print function should be replaced with printf for correct output. Additionally, the strcmp function returns 0 if the strings are equal, so the condition in the if statement should be modified to check for equality (if(strcmp(s1, s2) == 0)).
//
// Overall, the code snippet aims to demonstrate string comparison in C using the strcmp function.










