// Aşağıdaki kod bloğunun ekran çıktısı nedir?
#include <stdio.h>

//void func(int x)
func(int x){
    ++x;
    printf("%d", x);
}

int main(){
    int x = 10;

    func(++x);
    func(x++);

    printf("%d", x);
}
