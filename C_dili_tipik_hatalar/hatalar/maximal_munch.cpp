// longest match

#include <stdio.h>
int main(void){
    int x = 10;
    int y = 20;

    int a = x+++y;

    printf("%d\n", a);
    printf("%d\n", x);
    printf("%d\n", y);
}


// x ve y değişkenlerinin arasında 3 tane + karakteri birlikte kullanılmış
// Derleyici bunu nasıl tokenize edecek
// maximal munch kuralı diyor ki derleyici en uzun token'ı
// bizim türkçemizde atom u seçecek şekilde atomlarına ayırır
// tokenize eder yani biz kodu bu şekilde yazsak da derleyici
// x i identifier olarak alacak + operatör, + yı ekledi yine bozulmadı
// üçüncü + yı eklerse bozulur
// x ++ + y
// bunu bu şekilde yazmakla bu şekilde yazmakla arasında bir fark yok
// int a = x+++y;
// int a = x++ + y;
// Bu durumda ekran çıktısının ne olduğuna bakalım
// son ek ++ operatörünün ürettiği değer x in değeri 10  y nin değeri
// 20
// a ya verilen ilk değer 30 olmalı yan etkiden dolayı x 1 arttı
// x değişkeninin değeri 11 oldu
// y değişkeninin değeri değişmedi hala 20












