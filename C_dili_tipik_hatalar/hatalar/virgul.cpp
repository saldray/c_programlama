// Aşağıdaki programın ekran çıktısı ne olur?

#include <stdio.h>

int main(void){

    int x = 1;

    if (++x > 2,5)
        printf("%d", ++x);
    else
        printf("%d", x++);
}

// Buradaki virgül (,) comma operater
// Dolayısıyla buradaki , (virgül) operatörünün sol operandı ++x > 2 ifadesi
// sağ operandı ise 5 sabiti
// virgül operatörünü iyi hatırlamamız (anımsamamız gerekiyor)
// virgül operatörü bi c plans point operatörü oluşturuyor
// Yani önce sol operadı olan ifade yapılacak ondan sonra sağ operandı olan
// ifade değerlendirilecek
// if deyiminin parantezi içindeki koşul ifadesinin değeri
// virgül operatörünün ürettiği değer olan
// 5
// Çünkü virgül operatörünün ürettiği değer Sağ operandın değeridir.
// Dolayısıyla 5 sabiti. Logic yorumlamaya tabii tutulduğunda
// True Doğru olarak yorumlanacak ve programın akışı if deyiminin doğru kısmına
// girecek. fakat yan etki sonucu x in değeri artık 2
// Yani bu printf fonksiyonu çağırıldığında x değişkeninin değeri 2 olmuş durumda
// c coins point kavramı
// ++x ürettiği değer nesnenin 1 fazlası olduğu için programın akışı if deyiminin
// doğru kısmına girecek ve standart output a 3 değeri yazdırılacak

// Eğer kodu else girecek şekilde yazarsak

// #inculde <stdio.h>
//
// int main(){
//     int x = 2;
//
//     if (++x > 1,0)
//         printf("%d", ++x);
//     else{
//         printf("%d", x++);
//         printf("\n%d", x);
//     }
// }

// output : 3
//          4

// virgül operandı en son sağ tarafı döndürüyor 0 sabiti False olduğu için
// else giriyor



/*
https://www.geeksforgeeks.org/comma-in-c/
// C Program to Demonstrate
// Comma as operator
#include <stdio.h>

int main()
{
	int x = 10;

	// Using Comma as operator
	int y = (x++, ++x);

	printf("%d", y);
	getchar();
	return 0;
}

Output

12



// C Program to Demonstrate
// comma as separator and
// as operator
#include <stdio.h>

int main()
{
	// Comma separating x=10 and y
	int x = 10, y;

	// Comma acting as operator
	// y= (x+2) and x=(x+3)
	y = (x++, printf("x = %d\n", x), ++x,
		printf("x = %d\n", x), x++);

	// Note that last expression is evaluated
	// but side effect is not updated to y
	printf("y = %d\n", y);
	printf("x = %d\n", x);

	return 0;
}

Output

x = 11
x = 12
y = 12
x = 13

Time Complexity: O(1)
Auxiliary Space: O(1)






// C Program to
// demonstrate Use of
// comma as a separator

#include <stdio.h>

int main()
{

	// i=1 and j=2 are initialized
	// uses comma as separator
	for (int i = 1, j = 2; i < 10 && j < 10; i++) {

		if (i == 5) {
			// using comma as separator
			i = 6, j = 10;
		}

		printf("%d %d\n", i, j);
	}

	return 0;
}


Output

1 2
2 2
3 2
4 2
6 10

Time Complexity: O(1)
Auxiliary Space: O(1)*/









