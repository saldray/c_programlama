// https://www.youtube.com/watch?v=3ZF3pD2BIYo&list=PLL6uEfc2ZSZXTF3OUVwnXpE-jGk-d-EHV&index=3

#include <stdio.h>

int main(){
    int a = 11;

    if (10 < --a < 20)
        printf("%d", --a);
    else
        printf("%d", ++a);
}


// Bu kod ifadesinin çoktısı ne olur.

// Burada üç tane operator var.
// 2 tane küçüktür operatörü, 1 tane ön ek konumundaki -- operatörü
// ön ek konumundaki -- operatörü ön ek konumundaki -- operatörü
// daha yüksek öncelikli ürettiği değer ön ek , prefiks değer olduğu
// için nesnenin değerinin bir eksiği yani 10
// Bu durumda soldan sağa öncelik yönüne sahip olan küçüktür operatörleri
// söz konusu 10 < 10 ifadesinin değeri yanlış yani 0 dır
// 0 < 20 doğru olduğu için if in doğru kısmına girecek fakat
// yan etkiden dolayı a değişkeninin değeri artık 10 oldu
// side effects a nın değerini değiştirdi if in doğru kısmında
// printf çağrısıyla --a ifadesinin değeri yazdırılıyor
// a nın değeri 10 olduğuna göre --a nın değeri 9 oluyor.
