#include <stdio.h>
#include <stdlib.h>

// Kullanıcıdan 4 basamaklı bir sayı alıp
// bu sayının rakamlarını toplayan program

int main(){
  
  int myNumber,bolum,kalan,sum;
  sum=0;

  printf("4 basamaklı bir sayı giriniz:");
  scanf("%d",&myNumber);
  //4. basamağı bul
  bolum=myNumber / 1000; //9
  sum += bolum;//0+9
  kalan=myNumber % 1000;//871
  //3. basamağı bul
  bolum=kalan / 100; //8
  sum += bolum;//0+9+8 = 17
  kalan=kalan % 100;//71
  //2. basamağı bul
  bolum=kalan / 10; //
  sum += bolum;//0+9+8+7 = 24
  kalan=kalan % 10;//1
  //1. basamak
  sum += kalan;
  printf("Girdiginiz sayinin rakamlarin toplami: %d",sum);

  return 0;
}
