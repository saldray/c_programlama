 #include <stdio.h>

int main() {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4 - i - 1; j++) {
            printf(" ");
        }
        for (int j = 0; j < i + 1; j++) {
            printf("*");
        }
        printf("\n");
    }
    return 0;
}

// ilkleme 0
// bitiş, şart i<4
// artış miktarı i = i + 1
//
// içteki döngü
//
// ilkleme, başlangıç 0
// bitiş, şart j<4-i-1
// artış miktarı j = j + 1
//
// ilkleme başlangıç j = 0
// bitiş, şart  j< i + 1
// artış miktarı j= j + 1
//
//
// satır i 0 iken
// 0 ile 4-0-1, 0 ile 3

