// https://codeforwin.org/c-programming/
// c-program-remove-empty-lines-from-file

// Certainly! To remove empty lines from a text file using a C language program, you can follow the logic below. I’ll provide a sample program that demonstrates this process:
//
//     Open the Source File and Create a Temporary File:
//         Open the source file that contains the text.
//         Create a temporary file to store the modified content.
//
//     Read Line by Line from the Source File:
//         Read each line from the source file.
//         Check if the line is empty (contains only whitespace characters).
//
//     Write Non-Empty Lines to the Temporary File:
//         If the line is not empty, write it to the temporary file.
//         Otherwise, skip the empty line.
//
//     Close Both Files and Replace the Source File:
//         Close both the source file and the temporary file.
//         Delete the original source file.
//         Rename the temporary file to have the same name as the original source file.
//
// Here’s a C program that accomplishes this task:


#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE 1000

// Function to check if a string is empty (contains only whitespace)
int isEmpty(const char *str) {
    char ch;
    do {
        ch = *(str++);
        if (ch != ' ' && ch != '\t' && ch != '\n' && ch != '\r' && ch != '\0')
            return 0;
    } while (ch != '\0');
    return 1;
}

// Function to remove empty lines from the source file
void removeEmptyLines(FILE *srcFile, FILE *tempFile) {
    char buffer[BUFFER_SIZE];
    while (fgets(buffer, BUFFER_SIZE, srcFile) != NULL) {
        if (!isEmpty(buffer))
            fputs(buffer, tempFile);
    }
}

// Function to print the contents of a file
void printFile(FILE *fptr) {
    char ch;
    while ((ch = fgetc(fptr)) != EOF)
        putchar(ch);
}

int main() {
    FILE *srcFile;
    FILE *tempFile;
    char path[100];

    printf("Enter file path: ");
    scanf("%s", path);

    srcFile = fopen(path, "r");
    tempFile = fopen("remove-blanks.tmp", "w");

    if (srcFile == NULL || tempFile == NULL) {
        printf("Unable to open file.\n");
        printf("Please check if you have read/write privileges.\n");
        exit(EXIT_FAILURE);
    }

    printf("\nFile contents before removing empty lines:\n\n");
    printFile(srcFile);

    rewind(srcFile); // Move the source file pointer to the beginning
    removeEmptyLines(srcFile, tempFile);

    fclose(srcFile);
    fclose(tempFile);

    remove(path); // Delete the source file
    rename("remove-blanks.tmp", path); // Rename the temporary file

    printf("\n\nFile contents after removing empty lines:\n\n");
    srcFile = fopen(path, "r");
    printFile(srcFile);
    fclose(srcFile);

    return 0;
}

