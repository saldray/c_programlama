/* C code equivalent to the Python code provided */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    FILE *fhand;
    char line[100]; // Assuming a maximum line length of 100 characters

    fhand = fopen("mbox-short.txt", "r");
    if (fhand == NULL) {
        printf("Error opening file!\n");
        return 1;
    }

    while (fgets(line, sizeof(line), fhand) != NULL) {
        line[strcspn(line, "\n")] = 0; // Removing newline character
        if (strncmp(line, "From:", 5) == 0) {
            printf("%s\n", line);
        }
    }

    fclose(fhand);
    return 0;
}

// Explanation
//
// The provided Python code reads a file named 'mbox-short.txt' and prints lines that start with 'From:'. The equivalent C code achieves the same functionality by opening the file, reading it line by line, removing the newline character, and checking if each line starts with 'From:'. If a line matches the criteria, it is printed.
//
// In the C code:
//
// We open the file using fopen and handle potential errors.
// We read the file line by line using fgets.
// We remove the newline character at the end of each line using strcspn.
// We check if the line starts with 'From:' using strncmp.
// If a line matches, we print it using printf.
