#include <stdio.h>
#include <string.h>

int main() {
    FILE *fhand = fopen("mbox-short.txt", "r");
    if (fhand == NULL) {
        printf("Error opening file.\n");
        return 1;
    }

    char line[1024];
    while (fgets(line, sizeof(line), fhand)) {
        // Remove trailing newline character
        line[strcspn(line, "\n")] = '\0';

        // Skip 'uninteresting lines'
        if (strncmp(line, "From:", 5) != 0) {
            continue;
        }

        // Process our 'interesting' line
        printf("%s\n", line);
    }

    fclose(fhand);
    return 0;
}


