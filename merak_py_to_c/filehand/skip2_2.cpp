#include <stdio.h>
#include <string.h>

int main() {
    FILE *fhand;
    char line[1000];

    fhand = fopen("mbox-short.txt", "r");
    if (fhand == NULL) {
        perror("Error opening file");
        return 1;
    }

    while (fgets(line, sizeof(line), fhand)) {
        line[strcspn(line, "\n")] = 0; // Remove trailing newline
        // Skip 'uninteresting lines'
        if (strncmp(line, "From:", 5) != 0) {
            continue;
        }
        // Process our 'interesting' line
        printf("%s\n", line);
    }

    fclose(fhand);
    return 0;
}

