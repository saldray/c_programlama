#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    FILE *fhand;
    char line[255];

    fhand = fopen("mbox-short.txt", "r");
    if (fhand == NULL) {
        printf("Error opening file!");
        exit(1);
    }

    while (fgets(line, sizeof(line), fhand)) {
        line[strcspn(line, "\n")] = 0; // Remove newline character
        // Skip 'uninteresting lines'
        if (strncmp(line, "From:", 5) != 0)
            continue;
        // Process our 'interesting' line
        printf("%s\n", line);
    }

    fclose(fhand);
    return 0;
}

// Explanation
//
// I have converted the Python code to C language. Here's a breakdown of the conversion:
//
// I included the necessary header files for input/output operations and string functions.
// Declared a file pointer fhand and a character array line to store each line read from the file.
// Opened the file "mbox-short.txt" in read mode.
// Checked if the file opening was successful. If not, an error message is displayed, and the program exits.
// Used a while loop to read each line from the file using fgets.
// Removed the newline character at the end of each line.
// Skipped lines that do not start with "From:" using strncmp.
// Printed the 'interesting' lines using printf.
// Closed the file after processing all lines.
