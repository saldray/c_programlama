# message = 'merhaba'
# print(message[i % len(message)], end='')

#txt = "Thank you for the music\nWelcome to the jungle"
#
#x = txt.splitlines()
#print(x)

# newline dan itibaren ayırarak listeler oluşturur

# message = 'wobble'
# for i, bit in enumerate(message):
#     print(message[i % len(message)])
# 


message = 'wobble'
for i, bit in enumerate(message):
    print(message[i % len(message)],end='')
# 
# 
# 
#     message = 'wobble': This line initializes a string 
# variable message with the value 'wobble'.
#     for i, bit in enumerate(message):: This line starts a 
# for loop that iterates over each character in the message string. 
# The enumerate function is used here to get both the index (i) and 
# the character (bit) at that index.
#     print(message[i % len(message)], end=''): Inside the loop, 
# this line prints the character at the index i % len(message). 
# The modulo operator % is used to ensure that the index wraps 
# around to the start of the string once it reaches the end. 
# This creates a repeating pattern of the message string. 
# The end='' argument in the print function prevents it 
# from printing a newline character after each character, 
# so all characters are printed on the same line.
# 
# The result of running this code is that it prints the string 
# 'wobble' repeatedly in a single line, creating a pattern that 
# repeats the original message. This is a simple example of 
# how you can use the enumerate function to loop over an iterable 
# and access both the index and the value of each element, 
# as well as how to use the modulo operator to create repeating patterns.
 
# Cyclical Printing: Inside the loop, message[i % len(message)] is used to print each character in a cyclical manner.
#
# Döngüsel Yazdırma: Döngünün içinde, her karakteri döngüsel bir şekilde yazdırmak için message[i % len(message)] kullanılır.
#
# The modulo operator % is used to wrap the index around the length of the message.
# This means that when i exceeds the length of message, it starts over from the beginning. For example,
# if message has 6 characters, i % len(message) will always be between 0 and 5,
# ensuring that the index wraps around to the start of the string when it reaches the end.
#
# Döngüsel Yazdırma: Döngünün içinde, her karakteri döngüsel bir şekilde yazdırmak
# için message[i % len(message)] kullanılır.  Modulo operatörü %, dizini mesajın
# uzunluğu etrafına sarmak için kullanılır.  Bu, i mesajın uzunluğunu
# aştığında baştan başlayacağı anlamına gelir.  Örneğin, eğer mesaj 6 karakterden oluşuyorsa, i % len(message) her zaman 0 ile 5 arasında olacaktır,
# bu da dizinin sonuna ulaştığında dizinin başlangıcına kadar sarılmasını sağlar.
