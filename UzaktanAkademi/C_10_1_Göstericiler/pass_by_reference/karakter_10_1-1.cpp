#include <stdio.h>

int main(){

  char string[500];
  scanf("%s",string); //neden 'string' &(ampersand) almadı
  printf("%s\n",string);

  return 0;
}

/* Bir karakter dizisi oluşturmuşum bunun adına string demek yerine
   d diyeyim Bu diziye kullanıcıdan bir metin aldım. Daha önceden
   scanf in & ile işlem yaptığını biliyordum. Benim içine vereceğim
   değişkenin referansını vermem gerektiğini biliyordum peki nasıl
   oldu da burada ben d'nin başına &(ampersand,and) koymadan bu kod
   çalıştı? (&d koysam da beklediğim gibi çalışacak) Ben buraya d'nin
   adresini göndermedim?
   char d[500];
   Biz herhangi bir dizi(array) oluşturduğumuzda şu görmüş olduğumuz
   d adı aslında bir pointer oluyor. Gelelim ne demek istiyoruz.

   char d[500];        1000 'u'
      Adres            1001 'z'
      pointer          1002 'a'
                       1003 'k'
                         .  't'
                         .
                       1499
                       ____
                       1000 <--d
   d --> 1000 adresini tutuyor

   Bu pointer nereyi gösteriyor. Dizinin ilk elemanını gösteriyor.
   Yani ben herhangi bir dizi oluşturduğum zaman dizinin adı bir
   pointer oluyor. Bu ornek için d pointer oluyor.
   Bu pointer her zaman dizinin ilk karakterini, elemanını gösteriyor
   Diyelim ki RAM'in farklı bir yerinde tuttu bunun ismini (d)
   Diyelim ki d, en altta olsun şekilde gözüktüğü gibi
   d değişkeni ne tutacak o zaman 1000 Çünkü ilk elemanını gösteriyor.
   d'nin pointer olması Bana bunlara teker teker erişebilmeme olanak
   sağlıyor.
   daha önce d[0] yazdığımda 0. elemana erişiyordum.
             d[1] yazdığımda 1. elemana erişiyordum.

   Bunların pointerlarla arasındaki ilişkiye bakacağız.
   yaptığımız örnekten devam edelim. Diyelim ki karakter pointer'ı
   oluşturdum

   char d[500];
   char* p = d; // p'ye d'yi gösterdim. & kullanmadım d zaten pointer adres
   iki pointer'ı birbirine eşitlemiş oldum. Bu kadar

   1000 'U'\---d
   1001 'z' \---p      char* p = d;
   1002 'a'            *p --> 'U' erişir. Bir sonraki elemana
   1003 'k'               erişmek istersem
     .  't'               p'nin değerini (1000'i) 1 arttırırsam
     .                    bir sonraki elemana erişmiş oluyorum
   1499
   ____                 p+1 --> 1001 içindeki değere erişmek istersem
   1000 <--d           *(p+1)--> 'z'
                       *(p+2)--> 'a'
   !! Burada karakter dizisi tanımladığım için karakterler 1 byte
   yer tutuyor. O yüzden her bir adres bir sonrakinin 1 fazlası
   oluyor.
   Diyelim ki ben integer array tuttum. Tam sayı dizisi tuttum
   Bu tam sayılar sistemden sisteme değişiyor Benim sistemimde
   4 byte diyelim.
   O zaman adresler 1000 den başlıyorsa ondan sonraki adresler
   1000
   1004
   1008
   Bu sefer ne yapmam lazım diye düşünebilirsiniz +4, +4 diye
   arttırmalımıyım. Hayır öyle bir şey yok benim burada p+1
   dediğim şey p'nin sayısal değerine 1 ekle değil
   p'nin gösterdiği elemanın bir sonraki satırına geç demek
   Orada 4 byte var 8 byte var 32 byte var. Önemli değil
   Bir sonraki elemana geç demek oluyor.
   p+1 --> 1000'i 1001 yapmaktansa
           1000'den sonra gelen elemanı(item) gösteriyor.
   integer olsaydı 1000 artık 1004 olacaktı ama ben yine
   p+1 yazacaktım.

   *(p+1) sayısal değere eklemiyor gidip bir sonraki elemanı
   *(p+2) gösteriyor.

   *(p++) --> yaptığımda p=p+1 1 arttırıyorum ama kendi değerinide

   1001 'z' <-- p (p++) artık burayı gösteriyor.

   *(p++) yaptığımda artık pointer'ın gösterdiği yerde değişiyor.
   *(p--) pointer'ı bir yukarı bir öncesine götürüyor.
   *(--q)
   */
