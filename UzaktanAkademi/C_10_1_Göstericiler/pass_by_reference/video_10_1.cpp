#include <stdio.h>

void karesini_al(int* x); // Function prototype(declaration)

int main(){
  int x;
  printf("Lütfen bir sayı giriniz: ");
  scanf("%d",&x); // pass by reference (adres gönderiyoruz)
  karesini_al(&x);// pass by reference
  printf("%d\n",x);
  return 0;
}

void karesini_al(int* x){//Buraya adress göndermişim Pointer
  // Ona point eden bir adres gibi düşünebiliriz *x=&x gibi
  (*x) = (*x) * (*x);
}
