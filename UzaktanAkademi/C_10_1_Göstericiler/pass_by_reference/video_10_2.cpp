// #include <stdio.h>
// //pointer aritmetiği

// int main(){

//   int sayilar[5] = {5,7,4,3,8};
//   for(int i=0; i < 5; i++)
//     printf("%d ",sayilar[i]);
//   puts("");

//   int* dPtr; // pointer tanımlıyorum.
//   // Alttaki 3 ifade de birbirine denk.
//   dPtr = sayilar;
//   printf("Adres: %p\nDeger: %d\n",dPtr,*dPtr);
//   dPtr = &sayilar;
//   printf("Adres: %p\nDeger: %d\n",dPtr,*dPtr);
//   dPtr = &sayilar[0];
//   printf("Adres: %p\nDeger: %d\n",dPtr,*dPtr);
// }


// dizi yarattığım zaman sayilar[5] ismi(sayilar) artık bir pointer
// oluyordu.

// dPtr = sayilar  bir pointer tipindeki değişkene başka bir pointer
// değişkenini eşitleyebiliyordum.

// dPtr = &sayilar aynı zamanda adresine eşitleyebilirim.
// dPtr = &sayilar[0]  adresinde gösterdiği 0'ıncı elemana da
// eşitleyebiliyorum.


#include <stdio.h>
// pointer aritmetiği

int main() {

  int sayilar[5] = {5, 7, 4, 3, 8};
  for (int i = 0; i < 5; i++)
    printf("%d ", sayilar[i]);
  puts("");

  int *dPtr;
  // int(*dPtr)[5]; // pointer tanımlıyorum.
  //  Alttaki 3 ifade de birbirine denk.
  dPtr = sayilar;
  printf("Adres: %p\nDeger: %d\n", dPtr, *dPtr);
  dPtr = &sayilar;
  printf("Adres: %p\nDeger: %d\n", dPtr, *dPtr);
  dPtr = &sayilar[0];
  printf("Adres: %p\nDeger: %d\n", dPtr, *dPtr);
}

// 24:00
