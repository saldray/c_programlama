// #include <stdio.h>

// // Scope
// // scope Kapsam

// void lokal_degisken();
// void lokal_statik_degisken();
// void global_degisken();

// int x = 1; // global değişken tanımlamış olduk.
// // main'in dışında tanımladığımız değişkenler global olur.

// int main(){

//   int x = 10;
//   printf("Main icerisindeki lokal x:\t%d\n", x);

//   {// bir adet scope açmış olduk.
//     int x = 7;
//     printf("Main icerisindeki scope baslangicta lokal x:\t%d\n",x);
//     x++;
//     printf("Main icerisindeki scope cikarken lokal x:\t%d\n",x);
//   }

// printf("Main icerisindeki lokal x:\t%d\n",x);

// lokal_degisken();
// lokal_statik_degisken();
// lokal_degisken();
// global_degisken();
// lokal_statik_degisken();
// global_degisken();

// return 0;
// }

// void lokal_degisken(){
//   int x = 30; // bu fonksiyon her cağırıldığında tekrardan bir x oluşturur.
//   printf("lokal_degisken icerisinde baslangicta lokal x:\t%d\n",x);
//   x *= 20;
//   printf("lokal_degisken icerisinde cikarken lokal x:\t%d\n",x);
// }

// void lokal_statik_degisken(){
//   static int x = 50; // bu fonksiyon ne kadar çağırılırsa çağırılsın 1 adet x oluşturulur.
//   printf("lokal_statik_degisken icerisinde baslangicta statik x:\t%d\n",x);
//   x++;
//   printf("lokal_static_degisken icerisinde cikarken statik x:\t%d\n",x);
// }

// void global_degisken(){
//   printf("global_degisken icerisinde baslangicta global x:\t%d\n",x);
//   x -= 100;
//   printf("global_degisken icerisinde cikarken global x:\t%d\n",x);
// }
