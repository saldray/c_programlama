;; https://www.emacswiki.org/emacs/ElispCookbook
;; (string-prefix-p "bull" "bulldozer")
;; (string-suffix-p "dozer" "bulldozer")

;; (defun string/ends-with (s ending)
;;   "Return non-nil if string S ends with ENDING."
;;   (cond ((>= (length s) (length ending))
;; ;; (length s) >= (length endind) kontrolünü yapar
;;          (string= (substring s (- (length ending))) ending))
;;         (t nil)))

;; (string/ends-with "Hello, world!" "world!")

;; (length "Hello,world")
;; (length "word")


;; (defun string/ends-with (s ending)
;;   (substring s (- (length ending))) ending)

;; (string/ends-with "Hello, world!" "world!")

;; output: world!


;; (defun string/ends-with (s ending)
;;   (length ending))

;; (string/ends-with "Hello, world!" "world!")

;; ;; output: 6

;; (- length ending)

;; ;; -6



;; (defun string/ends-with (s ending)
;;   ;;  (substring s (- (length ending)))
;;   (substring s -6)) ;; üsttekinin aynısı

;; (string/ends-with "Hello, world!" "world!")
