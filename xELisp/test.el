;; (if (eq (what-line) "Line 20")
;;     (princ "Im at line 20")
;;   (princ "Im not at line 20"))


;; (defun add-numbers (num1 num2)
;;   (+ num1 num2))

;; (add-numbers 3 5)

;; (defun add-surname (surname)
;;   (insert "James " surname))

;; (add-surname "Cordon")
(defun add-surname (surname)
  (insert "James " surname))

(add-surname "Cordon")


(defun the-meaning-of-life (answer)
  (message "The answer is %s" answer))



how to run elisp code in emacs (let ((x 0))
  (while (< x 4)
    (print (format "number is %d" x))
    (setq x (1+ x))))


To run the given Emacs Lisp (Elisp) code snippet, follow these steps:

    Interactive Evaluation:
        Type M-: (hold down the Meta key and press :).
        Paste the entire code snippet into the minibuffer.
        Press RET (Enter).
        The result will be displayed in the echo area.

    Batch Mode (from the command line):
        Open your terminal or command prompt.
        Run the following command (replace test.el with the actual filename containing your Elisp code):

        emacs -batch -l test.el







 You can evaluate Emacs Lisp code in an Emacs buffer using the following methods:

    In the scratch buffer:
        Type your code snippet in the scratch buffer.
        Press LFD (or C-j) after the code.
        The result of evaluating the form will be inserted in the buffer.

    Using eval-expression (M-x eval-expression):
        Type M-: or execute M-x eval-expression.
        Enter your Lisp form in the minibuffer.
        Press RET to evaluate it.

    Evaluating a top-level form:
        In emacs-lisp-mode, place your cursor before or around the form.
        Type C-M-x to evaluate it.

    Evaluating the form immediately before point:
        In any buffer, type C-x C-e.
        The result will be printed in the echo area.

    Evaluating all forms in a file:
        Use M-x load-file to evaluate all Lisp forms in a file.




