;; ;; Happy hacking, Saldray - Emacs ♥ you!

;; (type-of 40)

;; (+ 2 4)


;; ;; (defun the-meaning-of-life (answer)
;; ;;   (message "The answer is %s" answer))

;; (stringp "Am I a string?")

;; "Hello world!"

;; ;; (defun add-surname (surname)
;; ;;   (interactive)
;; ;;   (insert "James " surname))
;; ;; ;; (add-surname "Cordon")

;; ;; (add-surname "Cordon")


;; (defun there-and-back ()
;;   (save-excursion
;;     (previous-line)
;;     (insert "ABOVE")))

;; (there-and-back)

;; (defun my-add-function (num)
;;   (+ num 1))

;; (my-add-function 4)

;; (my-add-function 12)


;; https://github.com/chrisdone-archive/elisp-guide?tab=readme-ov-file#programming-in-emacs-lisp

;; Evaluation

;; Use M-: to evaluate any Emacs Lisp expression and print the result. I personally use this constantly.
;; Use C-x C-e to evaluate the previous s-expression in the buffer. I personally never use this. See next binding.
;; Use C-M-x to evaluate the current top-level s-expression. I use this to re-apply defvar and defun declarations.
;; There is a REPL available by M-x ielm. I tend to use M-: rather than the REPL but you might like it.
;; Use M-x eval-buffer to evaluate the whole buffer of Emacs Lisp code.


;; (if t
;;     'it-was-true
;;   'it-was-false)

(find-file "/home/Saldray/.emacs.d/init.el")





















