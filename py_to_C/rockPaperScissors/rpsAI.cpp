#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Function to decide the winner
int game(char you, char computer) {
    if (you == computer)
        return -1; // It's a tie
    else if ((you == 'r' && computer == 'p') || (you == 'p' && computer == 's') || (you == 's' && computer == 'r'))
        return 0; // Computer wins
    else
        return 1; // You win
}

int main() {
    char choices[] = {'r', 'p', 's'};
    char player_choice, computer_choice;
    int result;

    srand(time(NULL));
    computer_choice = choices[rand() % 3];

    printf("Enter 'r' for rock, 'p' for paper, or 's' for scissors: ");
    scanf(" %c", &player_choice); // Note the space before %c to consume any newline character

    result = game(player_choice, computer_choice);

    printf("\nPlayer: %c, Computer: %c\n", player_choice, computer_choice);
    if (result == -1)
        printf("It's a tie!\n");
    else if (result == 0)
        printf("Computer wins!\n");
    else
        printf("You win!\n");

    return 0;
}

