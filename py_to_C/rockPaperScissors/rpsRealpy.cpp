// Code Converted

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* decide_winner(const char* player, const char* computer) {
    if (strcmp(player, computer) == 0) {
        return "It's a tie!";
    } else if ((strcmp(player, "rock") == 0 && strcmp(computer, "scissors") == 0) ||
               (strcmp(player, "scissors") == 0 && strcmp(computer, "paper") == 0) ||
               (strcmp(player, "paper") == 0 && strcmp(computer, "rock") == 0)) {
        return "You win!";
    } else {
        return "You lose!";
    }
}

int main() {
    const char* choices[] = {"rock", "paper", "scissors"};
    char player_choice[20];
    printf("Enter rock, paper, or scissors: ");
    scanf("%s", player_choice);

    int random_index = rand() % 3;
    const char* computer_choice = choices[random_index];

    const char* result = decide_winner(player_choice, computer_choice);

    printf("Player: %s, Computer: %s\n", player_choice, computer_choice);
    printf("%s\n", result);

    return 0;
}


/*
const char* decide_winner(const char* player, const char* computer)

Bu fonksiyon, player ve computer adlı iki parametre alır. Her iki parametre de const char* türünde, yani karakter dizilerini (string) işaret eden sabit işaretçilerdir. Fonksiyon, sonuç olarak bir karakter dizisi döndürür.

Beraberlik Durumu:

if (strcmp(player, computer) == 0) {
    return "It's a tie!";
}

Burada, strcmp fonksiyonu kullanılarak player ve computer dizileri karşılaştırılır. Eğer diziler aynı ise (sonuç 0 döner), bu durumda sonuç "It's a tie!" olarak döndürülür.

Oyuncunun Kazanma Durumları:

else if ((strcmp(player, "rock") == 0 && strcmp(computer, "scissors") == 0) ||
         (strcmp(player, "scissors") == 0 && strcmp(computer, "paper") == 0) ||
         (strcmp(player, "paper") == 0 && strcmp(computer, "rock") == 0)) {
    return "You win!";
}

Bu bölümde, oyuncunun kazandığı üç durum kontrol edilir:

    player "rock" ve computer "scissors" olduğunda

    player "scissors" ve computer "paper" olduğunda

    player "paper" ve computer "rock" olduğunda

Bu üç durumdan herhangi biri gerçekleşirse, sonuç "You win!" olarak döndürülür.

Bilgisayarın Kazanma Durumları:

else {
    return "You lose!";
}

Eğer yukarıdaki durumların hiçbiri gerçekleşmezse, bu durumda bilgisayar kazanır ve sonuç "You lose!" olarak döndürülür.

*/














