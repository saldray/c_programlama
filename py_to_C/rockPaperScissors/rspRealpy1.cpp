#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctime>


// Function to decide the winner
const char* decide_winner(const char* player, const char* computer) {
    if (strcmp(player, computer) == 0) {
        return "It's a tie!";
    } else if ((strcmp(player, "rock") == 0 && strcmp(computer, "scissors") == 0) ||
               (strcmp(player, "scissors") == 0 && strcmp(computer, "paper") == 0) ||
               (strcmp(player, "paper") == 0 && strcmp(computer, "rock") == 0)) {
        return "You win!";
    } else {
        return "You lose!";
    }
}

int main() {
    const char* choices[] = {"rock", "paper", "scissors"};
    char player_choice[20];
    const char* computer_choice;

    printf("Enter rock, paper, or scissors: ");
    scanf("%s", player_choice);

    // Generate a random computer choice
    srand(time(NULL));
    computer_choice = choices[rand() % 3];

    const char* result = decide_winner(player_choice, computer_choice);

    printf("Player: %s, Computer: %s\n", player_choice, computer_choice);
    printf("%s\n", result);

    return 0;
}

