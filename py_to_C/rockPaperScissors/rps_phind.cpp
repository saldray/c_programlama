#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Function to determine the winner
int game(char you, char computer) {
    if (you == computer)
        return -1; // Draw
    if ((you == 's' && computer == 'p') || (you == 'p' && computer == 'z') || (you == 'z' && computer == 's'))
        return 1; // You win
    return 0; // Computer wins
}

int main() {
    srand(time(NULL)); // Seed the random number generator
    int n = rand() % 3; // Generate a random number between 0 and 2
    char computer = (n == 0) ? 's' : (n == 1) ? 'p' : 'z'; // Map the random number to rock, paper, or scissors

    char you;
    printf("\nEnter s for STONE, p for PAPER, and z for SCISSOR\n");
    scanf(" %c", &you); // Note the space before %c to skip any whitespace

    int result = game(you, computer);
    if (result == -1) {
        printf("\nGame Draw!\n");
    } else if (result == 1) {
        printf("\nWow! You have won the game!\n");
    } else {
        printf("\nOh! You have lost the game!\n");
    }
    printf("\tYou choose: %c and Computer choose: %c\n", you, computer);

    return 0;
}

// This code snippet includes the following key components:
//
//     It uses srand(time(NULL)) to seed the random number generator with the current time, ensuring different random numbers are generated each time the program runs.
//     The rand() % 3 generates a random number between 0 and 2, which is then mapped to 's' (stone), 'p' (paper), or 'z' (scissors) to represent the computer's choice.
//     The game function compares the user's choice with the computer's choice and returns -1 for a draw, 1 if the user wins, and 0 if the computer wins.
//     The main function prompts the user for their choice, calls the game function to determine the outcome, and prints the result.
//
// This approach provides a straightforward implementation of the Rock, Paper, Scissors game in C, making it easy to understand and modify.
