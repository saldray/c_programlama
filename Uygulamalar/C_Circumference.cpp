// https://www.knowprogram.com/c-programming/simple-c-programs-for-beginners/

#include<stdio.h>
#define PI 3.14
int main()
{
   float radius, area;

   printf("Enter the radius of the circle: ");
   scanf("%f",&radius);

   area = 2 * PI * radius;
   printf("Area of the circle having radius %.2f = %.2f",
                                         radius, area );

   return 0;
}
