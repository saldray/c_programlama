// #include <stdio.h>

// int topla(int x, int y);

// int x, y, sonuc;

// int main(){

//   printf("topla fonksiyonunu cagırıyorum.\n");
//   printf("x icin deger giriniz: \n");
//   scanf("%d", &x);
//   printf("y icin deger giriniz: \n");
//   scanf("%d", &y);
//   int topla(int x,int y);
//   printf("sonuc : %d", &sonuc);
// }

// int topla(int x, int y){

//   //  int sonuc;
//   sonuc = x + y;
//   return sonuc;
// }


// copilot çıktısı

#include <stdio.h>

int topla(int x, int y);

int main() {
    int x, y, sonuc;

    printf("Enter a value for x: ");
    scanf("%d", &x);
    printf("Enter a value for y: ");
    scanf("%d", &y);

    sonuc = topla(x, y);
    printf("Result: %d\n", sonuc);

    return 0;
}

int topla(int x, int y) {
    return x + y;
}
