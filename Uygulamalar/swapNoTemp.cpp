#include <stdio.h>

int a, b;
int main(){
	
	a = 2;
	b = 3;

	printf(" ilk a harfi: %d\n", a);
	printf(" ilk b harfi: %d\n", b);

	a = a + b;
	b = a - b;
	a = a - b;

	printf("After swap a : %d\n", a);
	printf("After swap b : %d\n", b);
}
