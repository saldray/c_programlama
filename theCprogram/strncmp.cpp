// https://codetofun.com/c/string-functions/strncmp/

#include <stdio.h>
#include <string.h>

int main() {
    const char * str1 = "Hello, World!";
    const char * str2 = "Hello, C!";

    // Compare the first 7 characters of the strings
    int result = strncmp(str1, str2, 7);

    // Output the result
    if (result == 0) {
        printf("The first 7 characters are equal.\n");
    } else {
        printf("The first 7 characters are not equal.\n");
    }

    return 0;
}

// How the Program Works
//
// In this example, the strncmp() function compares the first 7
// characters of the strings "Hello, World!" and "Hello, C!" and
// prints the result.

// https://www.tutorialgateway.org/strncmp-in-c-language/
