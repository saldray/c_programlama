// 1.5.3 Line Counting


// Save it to a file (e.g., line_counter.c).
// Open a terminal or command prompt.
// Compile it using gcc line_counter.c -o line_counter.
// Run the compiled program with ./line_counter.


#include <stdio.h>
/* count lines in input */
int main(){
    int c, nl;

    nl = 0;
    while ((c = getchar()) != EOF)
        if (c == '\n')
            ++nl;
    printf("%d\n", nl);
}
