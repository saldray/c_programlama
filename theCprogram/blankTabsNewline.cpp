// Exercise 1-8. Write a program to count blanks, tabs, and newlines.

#include <stdio.h>

int main() {
    int blank_char, tab_char, new_line;
    // Variables to count blank characters, tab characters, and newlines
    blank_char = 0; // Initialize the count of blank characters
    tab_char = 0;   // Initialize the count of tab characters
    new_line = 0;   // Initialize the count of newlines

    int c; // Variable to hold input characters
    printf("Number of blanks, tabs, and newlines:\n");
    printf("Input some text (press Ctrl+D to end input):\n");

    // Loop to read characters until end-of-file (EOF) is encountered
    while ((c = getchar()) != EOF) {
        if (c == ' ') {
            ++blank_char; // Increment the count of blank characters
        }
        if (c == '\t') {
            ++tab_char; // Increment the count of tab characters
        }
        if (c == '\n') {
            ++new_line; // Increment the count of newlines
        }
    }

    // Print the final counts of blanks, tabs, and newlines
    printf("Blanks: %d, Tabs: %d, Newlines: %d\n", blank_char, tab_char, new_line);
    return 0;
}

