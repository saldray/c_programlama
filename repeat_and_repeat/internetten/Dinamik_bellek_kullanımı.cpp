// https://bilgigunlugum.net/prog/cprog/c_dinamik
// malloc() ve free() fonksiyon kullanılması

#include <stdio.h>
#include <stdlib.h>

int main(){
	int *ip;

	// int veri türü boyutu kadar bellek tahsisi
	ip = (int*) malloc(sizeof(int));
	*ip = 126;

	printf("Tahsis edilen bellek adres başlangıcı: %p\n", ip);
	printf("Tahsis edilen bellekteki değişken değeri: %d", *ip);
	free(ip);

	return 0;
}
