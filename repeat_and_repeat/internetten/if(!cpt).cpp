#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
	char *cpt;

	cpt = (char *) malloc(sizeof(char)*20);

	if(!cpt){
		printf("\n\tBellek tahsi edilemedi\n");
		exit(1);
	}

	printf("\n\tBir metin giriniz : ");
	fgets(cpt, 20, stdin);
	printf("\n\tGirilen metin : %s", cpt);//puts(cpt);

	free(cpt);

	return 0;
}
