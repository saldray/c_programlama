# fhand = open('mbox-short.txt')
# count = 0
# for line in fhand:
#     if line.startswith('From'):
#         print(line)

# fhand = open('mbox-short.txt')
# output_file = open('output.txt', 'w')

# for line in fhand:
#     if line.startswith('From:'):
#         print(line)
#         output_file.write(line)

# output_file.close()
# fhand.close()

# output.txt dosyasının çıktısını satır sayısını saydıralım.

# fhand = open('output.txt')
# count = 0
# for line in fhand:
#     count += 1
# print('Line Count:', count)

# output.txt dosyasının çıktısını yazdıralım.

# fhand = open('output.txt')
# count = 0
# for line in fhand:
#     if line.startswith('From:'):
#         print(line)

# rstrip : boşluk, \n (newline gibi karakterleri kırpıyor.)
# şu an sadece print in newline'ı çalışıyor çıktıda fazladan
# boşluk yok.

# fhand = open('mbox-short.txt')
# for line in fhand:
#     line = line.rstrip()
#     if line.startswith('From:'):
#         print(line)

# print'in newline'ini kaldırırsak bakalım ne olacak
# çıktıyı tek satırda yazdıracak.

# fhand = open('output.txt')
# for line in fhand:
#     line = line.rstrip()
#     if line.startswith('From:'):
#         print(line, end='')
