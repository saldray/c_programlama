// https://www.youtube.com/watch?v=V_T5NuccwRA

// ikili aramada önce (sort) düzgün bir şekilde sıralanması gerekir
// ikili arama sıralama yapıldıktan sonra işe yarar.

indeks   0  1  2  3  4  5  6  7  8  9     n=10
      a  5  9  17 23 25 45 59 63 71 89
         l              mid         r
data = 59

l = left (ilk başlangıç indeksi)
r = right (son bitiş indeksi)

  l   r    mid

  0   9    4    ((l+r)/2)










şu an burada 3 durum söz konusudur (three cases)
first case :

data = 59 ,  mid =25 (indeks 4), data ile mid'in aranan değerle orta değerin
eşit olması

case I : data == a[mid]
case II: data < a[mid]
case III:data > a[mid]  (data =59 > a[4] = 25)

    l   r    mid

    0   9    4    ((l+r)/2)
    5   9    7

indeks   0  1  2  3  4  5  6  7  8  9     n=10
      a  5  9  17 23 25 45 59 63 71 89
                        l     mid   r

yine 3 durum söz konusu


l   r    mid

0   9    4    ((l+r)/2)
5   9    7
5   6    5   left ile mid 5 i gösteriyor bir dahakine left, mid + 1 ilerler
6   6    6


left sağa giderken mid + 1
right sola giderken mid - 1

data (verinin) mid (orta) den büyük ya da küçük olmasına göre
left ya da right sabit kalır.

left > right olduğu durumda aranan değer dizide yok demektir.


int BinarySearch(a, n, data){
    l = 0, r = n-1
    while(l<r){
        mid=(l+r)/2;
        if(data == a[mid])
            return mid;
        else if (data < a[mid]){
            r = mid-1;
        }
        else{
            l = mid + 1
        }
        return -1;
    }
}



int BinarySearch(int a[], int n, int data) {
    int l = 0, r = n - 1;
    while (l <= r) {
        int mid = l + (r - l) / 2;
        if (data == a[mid])
            return mid;
        else if (data < a[mid])
            r = mid - 1;
        else
            l = mid + 1;
    }
    return -1; // Element not found
}



Here are the changes I made:

Added proper data types (int) for variables.
Adjusted the loop condition to l <= r to ensure the search covers the entire array.
Used l + (r - l) / 2 to calculate the middle index to avoid integer overflow.
Updated the else branch to adjust the left boundary (l) when data is greater than a[mid].

Feel free to use this modified code, and let me know if you have any other questions! 😊










