// https://bilgisayarkavramlari.com/2009/12/21/ikili-arama-algoritmasi-binary-search-algorithm/
// https://www.geeksforgeeks.org/binary-search/
#include <stdio.h>

int main(){


    int a[10]={2,5,7,9,12,15,34,76,87,123};
    int aranan = 123;
    int eb = 10;
    int ek = -1;
    int bayrak = 0;
    while(eb-ek >1){
        int bakilan = (eb+ek)/2;
        if(a[bakilan]==aranan){
            bayrak = 1;
            printf("bulunan : %d", bakilan);
            break;
        }
        else if(a[bakilan]< aranan){
            ek = bakilan;
        }
        else{
            eb = bakilan;
        }
    }
    if(bayrak == 0){
        printf("sayi bulunamadi");
    }

}
