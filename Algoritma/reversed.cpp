#include <stdio.h>

// Function to reverse a given array
void reverse_array(int arr[], int start, int end) {
    int temp;
    while (start < end) {
        temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
        start++;
        end--;
    }
}

// Function to print the array
void print_array(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() {
    int arr[] = {1, 2, 3, 4, 5};
    int size = sizeof(arr) / sizeof(arr[0]);

    printf("Original array: \n");
    print_array(arr, size);

    reverse_array(arr, 0, size - 1);

    printf("Reversed array: \n");
    print_array(arr, size);

    return 0;
}
