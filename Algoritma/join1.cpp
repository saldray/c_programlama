#include <stdio.h>
#include <string.h>
#include <cstdlib> // Include the <cstdlib> header for the 'free' function

// Function to concatenate two strings
char *concat_strings(const char *str1, const char *str2) {
    size_t len1 = strlen(str1);
    size_t len2 = strlen(str2);
    char *result = (char *)malloc(len1 + len2 + 1);

    if (result == NULL) {
        fprintf(stderr, "Memory allocation failed\n");
        return NULL;
    }

    strcpy(result, str1);
    strcat(result, str2);

    // Don't forget to free the memory when done
    free(result);

    return NULL;
}

int main() {
    const char *str1 = "Hello, ";
    const char *str2 = "world!";
    char *joined_string = concat_strings(str1, str2);

    if (joined_string != NULL) {
        printf("%s\n", joined_string);
        // free(joined_string); // Uncomment this line when using the corrected function
    }

    return 0;
}
