#include <stdio.h> /* header file */

// 3.3 Example 1

void do_little();

int main () /* Trivial program */
{
    /* This little line has no effect */
    /* This little line has none */
    /* This little line went all the way down
     t *o the next line,
     And so on...
     And so on...
     And so on... */
    do_little();
    printf ("Function ’main’ completing.\n");
}
/**********************************************/
/* A bar like the one above can be used to */
/* separate functions visibly in a program */
void do_little ()
{
    /* This function does little. */
    printf ("Function ’do_little’ completing.\n");
}
